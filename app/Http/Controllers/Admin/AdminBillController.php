<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Models\Admin\Bills;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Customers;


class AdminBillController extends Controller
{
    protected $categories;
    protected $role;


    public function __construct(Bills $bills)
    {
        $this->middleware('auth');
        $this->bills = $bills;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin');
        $this->data['title'] = 'Quản lý hóa đơn';
        $customers = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->select('customers.*', 'bills.status as bill_status')
                    ->orderBy('id', 'desc')
                    ->get();
        $this->data['customers'] = $customers;

        $this->data['bills'] = Bills::all();
        // $this->data['customerInfos'] = DB::table('customers')
                        
        //                 ->get();
         // dd($this->data['customers'])       ;        
        return view('admin.bill.index', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('admin');
        $customerInfo = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->select('customers.*', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->where('customers.id', '=', $id)
                        ->first();

        $billInfo = DB::table('bills')
                    ->join('bill_details', 'bills.id', '=', 'bill_details.bill_id')
                    ->leftjoin('products', 'bill_details.product_id', '=', 'products.id')
                    ->where('bills.customer_id', '=', $id)
                    ->select('bills.*', 'bill_details.*', 'products.name as product_name')
                    ->get();
                    
        $this->data['customerInfo'] = $customerInfo;
        $this->data['billInfo'] = $billInfo;

        return view('admin.bill.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('admin');
        $bill = Bills::find($id);
        $bill->status = $request->input('status');
        $bill->save();
        // dd($bill);
        
        flash('Đã thay đổi trạng thái!!!')->success();

        return redirect()->route('bill_admin.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('admin');
        $bill = Customers::find($id);
        $bill->forceDelete();
        flash('Đã xóa đơn hàng!!!')->success();

        return redirect()->route('bill_admin.index');
    }
}