<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Bills;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminShopController extends Controller
{

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $this->authorize('admin');
        $users = DB::table('users')
                ->whereNotNull('shop')
                ->paginate(15);

        return view('admin.shop.shop', compact('users', 'user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $this->authorize('admin');
       
        $users = User::find($id);
        $products = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->paginate(3);

        $products_rating = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->avg('rating');

        $customers = DB::table('customers')
                    ->orderBy('id', 'desc')
                    ->get();
        $bills = Bills::all();
        $customerInfos = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where('products.shopper', '=', $id)
                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();
        $count = count($customerInfos);               

        $customerInfosXL = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '<>', 'Chưa xử lý'] ])
                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();

        $TotalMoney =   DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '=', '3'] ])
                        ->sum('total');


        $countXL = count($customerInfosXL);   



        $customerInfosCXL = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '=', 'Chưa xử lý'] ])

                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();
        $countCXL = count($customerInfosCXL);   



        return view('admin.shop.show', compact('users', 'products', 'products_rating', 'count', 'TotalMoney', 'countXL', 'countCXL'));
    }

    
    public function pay()
    {
        $listPays = DB::table('users')
                    ->where('pay', '=', 2)
                    ->get();

        return view('admin.shop.pay', compact('listPays'));
    }

     public function registerShop()
    {
        $users = DB::table('users')
                    ->where('status_shop', '=', 2)
                    ->get();
        return view('admin.shop.register_shop', compact('users'));
    } 

     public function register_shop_details($id)
    {
        $users = DB::table('users')
                    ->where('id', '=', $id)
                    ->get();
        return view('admin.shop.register_shop_details', compact('users'));
    }

     public function register_shop_update($id)
    {
        $users = User::find($id);
        $users->status_shop = 1;
        $users->update();
        return redirect()->route('register_shop');
    }

     public function register_shop_destroy($id)
    {
        $users = User::find($id);
        $users->shop = '';
        $users->addressshop = '';
        $users->phoneshop = '';
        $users->linkshop = '';
        $users->front_img = '';
        $users->backside_img = '';
        $users->status_shop = '3';
        $users->update();
        return redirect()->route('register_shop');
    }


    public function search(Request $request)
    {
        $search =  $request->input('search');

        $products = DB::table('users')
                ->where('name', 'like', '%'.$search.'%')
                ->whereNotNull('shop')
                ->paginate(10);
        $count_search = count($products);
        $user = Auth::id();
        $users = User::all();
        $users = DB::table('users')->where('name', 'like', '%'.$search.'%')->whereNotNull('shop')->orderBy('name')->paginate(20);

        $count = DB::table('users')->where('name', 'like', '%'.$search.'%')->whereNotNull('shop')->orderBy('name')->count();
        return view('admin.shop.shop', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page', 'search', 'count_search' ));
    }

}
