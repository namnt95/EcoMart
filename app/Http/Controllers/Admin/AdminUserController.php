<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class AdminUserController extends Controller
{
    protected $user;
    protected $role;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();


        $this->authorize('admin');
        $users = $this->user->getList();
        return view('admin.user.user', compact('users', 'user'));

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        $this->authorize('admin');
        return view('admin.user.user_edit', compact('user'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        $this->authorize('admin');
        $user->update($request->all());
        return redirect()->route('home.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $this->authorize('admin');
        $user->delete();
        return redirect()->route('user.index');
    }


    public function block($id)
    {
        $this->authorize('admin');
        $user = User::find($id);
        if ( $user->status_id == 1) {
            $user->status_id = 2;
        } else {
            $user->status_id = 1;
        }
        $user->save();
        if ( $user->status_id == 1) {
            flash('Bỏ chặn thành công!!!')->success();
            
        } else {
            flash('Chặn thành công!!!')->success();
        }
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $search =  $request->input('search');

        $products = DB::table('users')
                ->where([ ['status_id', 1], ['name', 'like', '%'.$search.'%'] ])
                ->paginate(10);
        $count_search = count($products);
        $user = Auth::id();
        $users = User::all();
        $users = DB::table('users')->where('name', 'like', '%'.$search.'%')->orderBy('name')->paginate(20);

        $count = DB::table('users')->where('name', 'like', '%'.$search.'%')->orderBy('name')->count();
        return view('admin.user.user', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page', 'search', 'count_search' ));
    }
}
