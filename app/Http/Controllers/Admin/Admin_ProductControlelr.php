<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Categories;
use App\Models\Admin\Price;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class Admin_ProductControlelr extends Controller
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin');
        $products = Product::paginate(3);
        $count = DB::table('products')->count();
        return view ('admin.product', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function search(Request $request)
    {
        $search =  $request->input('search');

        $products = DB::table('products')
                ->where([ ['status_id', 1], ['name', 'like', '%'.$search.'%'] ])
                ->paginate(10);
        $count_search = count($products);
        $user = Auth::id();
        $users = User::all();
        $brands = Brand::all();
        $page = DB::table('products')->where('name', 'like', '%'.$search.'%')->orderBy('name')->paginate(20);

        $count = DB::table('products')->where('name', 'like', '%'.$search.'%')->orderBy('name')->count();
        $categories = DB::table('categories')->get();
        $prices = Price::all();
        return view('admin.product', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page', 'search', 'count_search' ));
    }

    public function news()
    {
        return view('news');
    }

}
