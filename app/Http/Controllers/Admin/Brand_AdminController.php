<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class Brand_AdminController extends Controller
{

    protected $brand;


    public function __construct(Brand $brand)
    {
        $this->middleware('auth');
        $this->brand = $brand;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin');
        $status = Status::all();
        $brands = $this->brand->getBrand();
        return view('admin.brand.brand', compact('status', 'brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin');

        $status = Status::all();
        return view('admin.brand.brand_create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $brand = new Brand();
        $brand->name = $request->name;
        $brand->status_id = '1';
        $status = Status::all();
        
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $fileName = time().$file->getClientOriginalName();
            $file->storeAs('images/brands', $fileName);
            $brand->img = 'images/brands/'.$fileName;
        }
        
        $brand->save();
        flash('Thêm mới thành công!!!')->success();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('admin');

        $status = Status::all();
        $brand = Brand::find($id);
        return view('admin.brand.brand_edit', compact('brand', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);           
        $brand->name = $request->name;
        $brand->status_id = $request->status_id;

        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $fileName = time().$file->getClientOriginalName();
            $file->storeAs('images/brands', $fileName);
            $brand->img = 'images/brands/'.$fileName;
        }
            
        $brand->save();
        flash('Sửa thành công!!!')->success();

        return redirect()->route('brand_admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $brand = Brand::find($id);
        $brand->forceDelete();
        flash('Xóa thành công!!!')->success();

        return redirect()->route('brand_admin.index');
    }
}
