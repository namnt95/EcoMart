<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\Categories;
use App\Models\Admin\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class Categories_AdminController extends Controller
{

    protected $categories;
    protected $role;


    public function __construct(Categories $categories)
    {
        $this->middleware('auth');
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('admin');
        $status = Status::all();
        $categoriess = $this->categories->getCategories();
        return view('admin.categories.categories', compact('status', 'categoriess'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin');
        $status = Status::all();
        return view('admin.categories.categories_create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('admin');
        $categories = $this->categories->createCategories($request->all());
        return redirect()->route('categories_admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('admin');
        $status = Status::all();
        $categories = Categories::find($id);
        return view('admin.categories.categories_edit', compact('categories', 'status'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('admin');
        $categories = Categories::find($id);
        $categories->name = $request->name;
        $categories->status_id = $request->status_id;
        // dd($request->status_id);
        $categories->save();
        return redirect()->route('categories_admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('admin');
        $categories = Categories::find($id);
        $categories->forceDelete();
        return redirect()->route('categories_admin.index');
    }
}
