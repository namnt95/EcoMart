<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Categories;
use App\Models\Admin\Bills;
use App\Models\Admin\Customers;
use App\Models\Admin\Comment;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Star;
use App\Models\Admin\Information;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class DefaultController extends Controller
{


    public function __construct(User $user)
    {
        $this->user = $user;
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        $this->authorize('admin');
        $count_user = DB::table('users')
                    ->count();
        $count_admin = DB::table('users')
                    ->where('role', '=', 1)
                    ->count();
        $customerInfosCXL = DB::table('bills')
            ->where([ ['status', '=', 'Chưa xử lý'] ])
            ->count();
        $count_brand = Brand::count();
        $count_product = Product::count();
        $count_categories = Categories::count();
        $count_customer = Customers::count();
        $count_information = Information::count();

// dd($count_brand);
        return view ('admin.inc.index', compact('user', 'count_user', 'count_admin', 'count_brand', 'count_product', 'count_customer', 'count_categories', 'customerInfosCXL', 'count_information'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
