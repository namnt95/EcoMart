<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\Information;
use App\Models\Admin\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $information;
    protected $role;


    public function __construct(Information $information)
    {
        $this->middleware('auth');
        $this->information = $information;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('admin');
        $status = Status::all();
        $informations = $this->information->getInformation();
        return view('admin.information.information', compact('status', 'informations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin');
        // $New = Information::News;
        // $Introduce = Information::Introduce;
        // $Contact = Information::Contact;
        // $Job = Information::Job;
        // dd($New);
        $status = Status::all();
        // dd($status->sta);
        return view('admin.information.information_create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('admin');
        $information = $this->information->createInformation($request->all());
        return redirect()->route('information.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $information = Information::find($id);
        return view('admin.information.information_details', compact('information'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('admin');
        $status = Status::all();
        $information = Information::find($id);
        return view('admin.information.information_edit', compact('information', 'status'));
        // //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('admin');
        $information = Information::find($id);
        $information->title = $request->title;
        $information->news = $request->news;
        $information->information_id = $request->information_id;
        $information->status = $request->status;
        // dd($request->status_id);
        $information->save();
        return redirect()->route('information.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('admin');
        $information = Information::find($id);
        $information->forceDelete();
        return redirect()->route('information.index');
    }

    public function showNews($id)
    {
        // dd($id);
        $user = Auth::User(); 
        $news = Information::get();
        $news = DB::table('information')
            ->where([ ['information_id', '=', $id], ['status', '=', 1] ])
            ->get();
        $information_id = DB::table('information')
            ->where([ ['information_id', '=', $id], ['status', '=', 1] ])
            ->select('information_id')
            ->first();
            $information_id = json_encode($information_id);
        return view('news', compact('news', 'user', 'information_id'));
    }

    public function showNewsDetails($id)
    {
        $user = Auth::User(); 
        $news_details = DB::table('information')
            ->where([ ['id', '=', $id], ['status', '=', 1] ])
            ->get();
        return view('news', compact('news_details', 'user'));
    }




}