<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Admin\Comment;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Price;
use App\Models\Admin\Categories;
use App\Models\Admin\Bills;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Customers;
use App\Models\Admin\Star;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::count();
        $product_het = Product::where('quantity', '=', 0)->count();
        $product_con = Product::where('quantity', '>', 0)->count();
        $product_rating = Product::where('rating', '<', 3)->count();
        $product_status_id = Product::where('status_id', '=', 2)->count();

        $total = DB::table('Bills')
                ->sum('total');
        $count = Bills::count();
        $count = $count*15000;
        $total_percent = $total*0.03;
        $total_shop = $total - $total_percent;
    // dd($total_shop);
        return view('admin.profit', compact('product', 'product_het', 'product_con', 'product_rating', 'product_status_id', 'total', 'total_percent', 'total_shop', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function invoice()
    {
        $product = Product::count();
        $product_het = Product::where('quantity', '=', 0)->count();
        $product_con = Product::where('quantity', '>', 0)->count();
        $product_rating = Product::where('rating', '<', 3)->count();
        $product_status_id = Product::where('status_id', '=', 2)->count();

        $total = DB::table('Bills')
                ->sum('total');
        $count = Bills::count();
        $count = $count*15000;
        $total_percent = $total*0.03;
        $total_shop = $total - $total_percent;
        return view('admin.invoice-print', compact('product', 'product_het', 'product_con', 'product_rating', 'product_status_id', 'total', 'total_percent', 'total_shop', 'count'));

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
