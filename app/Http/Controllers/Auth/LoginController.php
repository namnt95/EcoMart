<?php

namespace App\Http\Controllers\Auth;

use App\Models\Admin\Product;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Cart;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    // public function username()
    // {
    //     return 'email';
    // }

    // public function authenticate(Request $request)
    // {
    //     $credentials = $request->only('email', 'password');

    //     if (Auth::attempt(['email' => $email, 'password' => $password, 'status_id' => 1])) {
    //     return redirect('/home');
    //     } else {
    //         asdsa;
    //     }
    // }

        protected function credentials(Request $request)
        {
            $credentials = $request->only($this->username(), 'password');
            return array_add($credentials, 'status_id', '1');
        }

    public function logout(Request $request) {
        Auth::logout();
        foreach (Cart::content() as $key => $value)  {
            $product_id = Cart::get($key)->id;
            $product = Product::find($product_id);
            $product->quantity = $product->quantity + Cart::get($key)->qty;
            $product->update();
        }
        Cart::destroy();
        session()->forget('key');
      return redirect('/home');
    }


    /**
     * Get the maximum number of attempts to allow.
     *
     * @return int
     */
    public function maxAttempts()
    {
        return 5;
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */
    public function decayMinutes()
    {
        return 60;
    }

    
}
