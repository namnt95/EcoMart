<?php

namespace App\Http\Controllers;

use DB; 
use App\Models\Admin\Categories;
use App\Models\Admin\Price;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class BrandController extends Controller
{


    protected $brand;


    public function __construct(Brand $brand)
    {

        $this->brand = $brand;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $brands = $this->brand->getList();
        return view ('brand.brand', compact('brands', 'user'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Status::all();
        return view('brand.brand_create', compact('status'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $brand = new Brand();
        $brand->name = $request->name;
        $brand->img = $request->img;
        $brand->link = $request->link;
        $brand->status_id = $request->status_id;
        
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $fileName = time().$file->getClientOriginalName();
            $file->storeAs('images/brands', $fileName);
            $brand->img = 'images/brands/'.$fileName;
        }
        $brand->save();
        return redirect()->route('brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        
        $categories = Categories::all();
        $prices = Price::all();
        $brands = Brand::all();
        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where([ ['products.brand_id', '=', $id], ['products.status_id', '=', 1] ])
                ->select('products.*','users.shop as shop')
                ->orderBy('name')
                ->paginate(10);

                    
                    
        $count = DB::table('products')
                ->where([ ['products.brand_id', '=', $id], ['products.status_id', '=', 1] ])
                ->count();
                 
        $brand_id = $id;
        return view('brand.brand', compact('products', 'brands', 'categories', 'prices', 'brand_id', 'count', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('brand.brand_edit', compact('brand'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, brand $brand)
    {

        $brand->update($request->all());
        
        
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $fileName = time().$file->getClientOriginalName();
            $file->storeAs('images/brands', $fileName);
            $brand->img = 'images/brands/'.$fileName;
        }
        $brand->save();
        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(brand $brand)
    {
        $brand->delete();
        return redirect()->route('brand.index');
    }

// GET PRICE
    public function getBrand_Price($brand_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where([ ['products.brand_id', '=', $brand_id], ['price', '>=', $price1],['price', '<=', $price2],['products.status_id', '=', 1] ])
                    ->select('products.*','users.shop as shop')
                    ->orderBy('name')
                    ->paginate(10);
       
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->count();

        $brand_id = $brand_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'categories', 'brand_id', 'prices', 'price1', 'price2', 'count', 'user'));
    }

// GET: CATEGORIES
    public function getBrand_Categories($brand_id,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['brand_id', '=', $brand_id], ['categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id], ['categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('brand.brand', compact('products', 'brands', 'categories', 'categories_id', 'prices', 'brand_id', 'count', 'user'));
    }


// GET: PRICE, CATEGORIES
    public function getBrand_Price_Categories($brand_id,$price1,$price2,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'categories', 'categories_id', 'prices', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

// GET: GENERAL 
    public function getBrand_RateHigh($brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->select('products.*','users.shop as shop')
                ->orderBy('price', 'desc')
                ->paginate(10);

                
        $count = DB::table('products')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->orderBy('price', 'desc')
                ->count();

        $brand_id = $brand_id;  
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'count', 'user'));
    }

    public function getBrand_RateLow($brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->select('products.*','users.shop as shop')
                ->orderBy('price')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->orderBy('price')
                ->count();

        $brand_id = $brand_id;  
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'count', 'user'));
    }

    public function getBrand_RateName($brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->select('products.*','users.shop as shop')
                ->orderBy('name')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['products.brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                ->orderBy('name')
                ->count();

        $brand_id = $brand_id;  
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'count', 'user'));
    }

// GET: PRICE, NO categories

    public function getBrand_PriceRateHigh($brand_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->select('products.*','users.shop as shop')
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $brand_id = $brand_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'price1', 'price2', 'count', 'user'));
    }

    public function getBrand_PriceRateLow($brand_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->select('products.*','users.shop as shop')
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $brand_id = $brand_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'price1', 'price2', 'count', 'user'));
    }

    public function getBrand_PriceRateName($brand_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->select('products.*','users.shop as shop')
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['brand_id', '=', $brand_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $brand_id = $brand_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'brand_id', 'price1', 'price2', 'count', 'user'));
    }

// GET: NO PRICE, Categories
    public function getBrand_CategoriesRateHigh($brand_id,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

    public function getBrand_CategoriesRateLow($brand_id,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

    public function getBrand_CategoriesRateName($brand_id,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

// GET: PRICE, Categories_id
    public function getBrand_Price_CategoriesRateHigh($brand_id,$price1,$price2,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

    public function getBrand_Price_CategoriesRateLow($brand_id,$price1,$price2,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

    public function getBrand_Price_CategoriesRateName($brand_id,$price1,$price2,$categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('brand.brand', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

}
