<?php

namespace App\Http\Controllers;

use Session;
use DB;
use Cart;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Categories;
use App\Models\Admin\Price;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Bills;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class CartController extends Controller
{
    public function create(Request $request, $product)
    {
        $product_buy = DB::table('products')
                    ->where([ ['id', $product], ['quantity', '!=', 0] ])
                    ->first();
        $count = DB::table('products')
                    ->where([ ['id', $product], ['quantity', '!=', 0] ])
                    ->count();
        if ( $count > 0  ) {
                    if ( $request->qty <= $product_buy->quantity ) {
                        $product_buy = DB::table('products')
                                ->where('id', $product)
                                ->first();
                        Cart::add(array('id' => $product, 'name' => $product_buy->name, 'qty' => $request->qty, 'price' => $product_buy->price, 'options' => array('img' => $product_buy->img1, 'shop' => $product_buy->shopper)));
                        $product = Product::find($product);
                        $product->quantity = $product->quantity - $request->qty;
                        $product->save();
                        $content = Cart::content();
                        flash('Đã thêm sản phẩm vào giỏ hàng。')->success();
                        return redirect()->back();
                    } else {
                        flash('Bạn đã chọn quá số lượng hiện có。')->error();
                        return redirect()->back();
                    }
                   
        } else {
            flash('Sản phẩm đã hết hàng。 Vui lòng chọn sản phẩm khác ')->error();
            return redirect()->back();
        }
    	
    }

    public function createIndex($product)
    {
        $product_buy = DB::table('products')
                    ->where([ ['id', $product], ['quantity', '>', 0] ])
                    ->count();
        if ( $product_buy > 0 ) {
            $user = Auth::user();
            $product_buy = DB::table('products')
                        ->where('id', $product)
                        ->first();
            Cart::add(array('id' => $product, 'name' => $product_buy->name, 'qty' => 1, 'price' => $product_buy->price, 'options' => array('img' => $product_buy->img1, 'shop' => $product_buy->shopper)));

            $product = Product::find($product);
            $product->quantity = $product->quantity - 1;
            $product->save();
            $users = User::all();
            $brands = Brand::all();
            $products = DB::table('products')->paginate(20);
            $count = DB::table('products')->count();
            $categories = Categories::all();
            $prices = Price::all();
            flash('Đã thêm sản phẩm vào giỏ hàng。')->success();
            return redirect()->back();
        } else {
            flash('Sản phẩm đã hết hàng。 Vui lòng chọn sản phẩm khác ')->error();
            return redirect()->back();
        }
        
    	
    }

    public function cart()
    {
        $id = Auth::id();
        $codes = DB::table('users')
                ->where([ ['id', '=', $id], ['code', '!=', 'NULL'] ])
                ->get();
        $count = count($codes);
        $user = Auth::user();
    	$content = Cart::content();
        
    	$total = Cart::subtotal('0', ',' , '.');
    	return view('cart', compact('content', 'total', 'user', 'codes', 'count'));
    }

    public function delete($id)
    {
        $product_id = Cart::get($id)->id;

        $product = Product::find($product_id);
        $product->quantity = $product->quantity + Cart::get($id)->qty;
        $product->update();
    	Cart::remove($id);
    	return redirect()->route('cart');
    }

    public function deleteall()
    {
        foreach (Cart::content() as $key => $value)  {
            $product_id = Cart::get($key)->id;
            $product = Product::find($product_id);
            $product->quantity = $product->quantity + Cart::get($key)->qty;
            $product->update();
        }
    	Cart::destroy();
        flash('Hủy giỏ hàng thành công')->success();
        if ( session()->has('key') ) {
          session()->forget('key');  
        }
    	return redirect()->route('home.index');
    }


    public function update($id, Request $request)
    {   
        

        $product_id = Cart::get($id)->id;

        $product = DB::table('products')
                    ->where([ ['id', $product_id], ['quantity', '>=', 0] ])
                    ->first();

        $count = DB::table('products')
                    ->where([ ['id', $product_id], ['quantity', '>=', 0] ])
                    ->count();
                    // dd(Cart::get($id)->qty);
        if ( $count > 0 ) {
            if ( $request->qty > Cart::get($id)->qty  ) {
                if ( $request->qty < $product->quantity ) {
                    $qty = $request->qty - Cart::get($id)->qty;
                    $product_id = Cart::get($id)->id;
                    $product = Product::find($product_id);
                    $product->quantity = $product->quantity - $qty;
                    $product->update();

                    $update = $request->qty;
                    Cart::update($id, $update);
                    return redirect()->route('cart');
                } else {
                    flash('Bạn đã chọn quá số lượng hiện có。')->error();
                    return redirect()->route('cart');
                }
                    
            } else {
                $qty = Cart::get($id)->qty - $request->qty ;
                $product_id = Cart::get($id)->id;
                $product = Product::find($product_id);
                $product->quantity = $product->quantity + $qty;
                $product->update();

                $update = $request->qty;
                Cart::update($id, $update);
                return redirect()->route('cart');
            }
        } else {
            flash('Bạn đã chọn số lượng nhỏ hơn mặc định, vui lòng chọn lại。')->error();
            return redirect()->route('cart');
        }
    }

    public function getCheckOut($id) 
    {
        $this->data['user'] = Auth::user();
        $this->data['title'] = 'Check out';
        $this->data['content'] = Cart::content();
       
        $this->data['total'] = Cart::total();
        $products = Product::all();
        $this->data['products'] = $products;
        return view('checkout', $this->data);
    }

    public function postCheckOut(Request $request) {
        
        $user = Auth::user();
        
        $content = Cart::content();
        $total = Cart::total();
        
        $customer = new Customers();
        $customer->user_id = 0;
        $customer->name = $request->fullName;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phoneNumber;
        $customer->note = $request->note;
        $customer->senddate = $request->senddate;
        $customer->save();
        

        $bill= new Bills();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $bill->customer_id = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = str_replace(',', '', Cart::subtotal());
        $bill->note = $request->note;
        $bill->status = 'Chưa xử lý';

        $bill->save();

        foreach($content as $item){
        $bill_detail = new Bill_Details();
        $bill_detail->bill_id = $bill->id;
        $bill_detail->product_id = $item->id;
        $bill_detail->quantity = $item->qty;
        $bill_detail->price = $item->price;
        $bill_detail->save();


    }
    Cart::destroy();
    session()->forget('key');
    return view('notification', 'user');
    }

    public function postCheckOut_id(Request $request,$id) {
        
        $user = Auth::user();
        
        $content = Cart::content();
        $total = Cart::total();
        
        $customer = new Customers();
        $customer->user_id = $id;
        $customer->name = $request->fullName;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phoneNumber;
        $customer->note = $request->note;
        $customer->senddate = $request->senddate;
        $customer->save();
        

        $bill= new Bills();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $bill->customer_id = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = str_replace(',', '', Cart::subtotal());
        $bill->note = $request->note;
        $bill->status = 'Chưa xử lý';

        $bill->save();

        foreach($content as $item){
        $bill_detail = new Bill_Details();
        $bill_detail->bill_id = $bill->id;
        $bill_detail->product_id = $item->id;
        $bill_detail->quantity = $item->qty;
        $bill_detail->price = $item->price;
        $bill_detail->save();


        }
    Cart::destroy();
    session()->forget('key');
    return view('notification', compact('user'));
    }

    public function postCheckOut_code_id(Request $request,$id, $number) {
        $user = Auth::user();
        
        $content = Cart::content();
        $total = Cart::total();
        
        $customer = new Customers();
        $customer->user_id = $id;
        $customer->name = $request->fullName;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phoneNumber;
        $customer->note = $request->note;
        $customer->senddate = $request->senddate;
        $customer->save();
        

        $bill= new Bills();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $bill->customer_id = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = $number;
        $bill->note = $request->note;
        $bill->status = 'Chưa xử lý';


        $bill->save();

        foreach($content as $item){
        $bill_detail = new Bill_Details();
        $bill_detail->bill_id = $bill->id;
        $bill_detail->product_id = $item->id;
        $bill_detail->quantity = $item->qty;
        $bill_detail->price = $item->price;
        $bill_detail->save();


        }
    Cart::destroy();
    session()->forget('key');
    return view('notification', compact('user'));
    }

    public function code(Request $request, $id) {
        $code = DB::table('users')
                ->where([ ['code', '=', $request->code], ['id', '=', $id] ])
                ->count();
                // dd($code);
        if($code == 0) {
            flash('Mã giảm giá không đúng hoặc đã hết hạn。')->error();
            return redirect()->back();
        } else {
            $code = User::find($id);
            $code->code = 'NULL';
            $code->update();
            flash('Nhập mã thành công。')->success();
            Session::put('key', '10');
            return redirect()->back();
        }
    }
}

