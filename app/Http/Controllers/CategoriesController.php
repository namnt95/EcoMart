<?php

namespace App\Http\Controllers;

use DB; 
use App\Models\Admin\Price;
use App\Models\Admin\Categories;
use App\Models\Admin\Product;
use App\Models\Admin\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{


    protected $categories;


    public function __construct(Categories $categories)
    {

        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categories = Categories::all();
        // $brands = Brand::all();
        // $products = Product::all();
        // $count = Product::all()->count();
        // return view('categories', compact('categories', 'brands', 'products', 'count'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriess = $this->categories->getStatus();
        return view('categories_create', compact('categoriess'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $categories = $this->categories->createCategories($request->all());
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['products.categories_id', '=', $id], ['products.status_id', '=', 1] ])
                ->paginate(10);

        $count = DB::table('products')
        ->where([ ['products.categories_id', '=', $id], ['products.status_id', '=', 1] ])
        ->count();

        $categories_id = $id;  

        return view('categories', compact('prices', 'products', 'brands', 'categories', 'categories_id', 'count', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit(Categories $categories)
    {
        $user = Auth::user();
        return view('categories_edit', compact('categories', 'user'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categories $categories)
    {
        $categories->update($request->all());
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $categories)
    {
        $categories->delete();
        return redirect()->route('categories.index');
    }

// GET PRICE
    public function getCategories_Price($categories_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->count();

        $categories_id = $categories_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'categories', 'categories_id', 'prices', 'price1', 'price2', 'count', 'user'));
    }

// GET: BRAND
    public function getCategorie_Brand($categories_id,$brand_id)
    {
        $user = Auth::user();
        
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('categories', compact('products', 'brands', 'categories', 'prices', 'categories_id', 'brand_id', 'count', 'user'));
    }


// GET: PRICE, BRAND
    public function getCategories_Price_Brand($categories_id,$price1,$price2,$brand_id)
    {
        $user = Auth::user();
        
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'categories', 'categories_id', 'prices', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

// GET: GENERAL 
    public function getCategories_RateHigh($categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('price', 'desc')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('price', 'desc')
                ->count();

        $categories_id = $categories_id;  
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user'));
    }

    public function getCategories_RateLow($categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('price')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('price')
                ->count();

        $categories_id = $categories_id;  
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user'));
    }

    public function getCategories_RateName($categories_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('name')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['products.categories_id', '=', $categories_id], ['products.status_id', '=', 1] ])
                ->orderBy('name')
                ->count();

        $categories_id = $categories_id;  
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user'));
    }

// GET: PRICE, NO BRAND_ID

    public function getCategories_PriceRateHigh($categories_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $categories_id = $categories_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'count', 'user'));
    }

    public function getCategories_PriceRateLow($categories_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $categories_id = $categories_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'count', 'user'));
    }

    public function getCategories_PriceRateName($categories_id,$price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $categories_id = $categories_id;
        $price1 = $price1;
        // dd($price1);
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'count', 'user'));
    }

// GET: NO PRICE, BRAND_ID
    public function getCategories_BrandRateHigh($categories_id,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

    public function getCategories_BrandRateLow($categories_id,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

    public function getCategories_BrandRateName($categories_id,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $categories_id = $categories_id;
        $brand_id = $brand_id;
        
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'brand_id', 'count', 'user'));
    }

// GET: PRICE, BRAND_ID
    public function getCategories_Price_BrandRateHigh($categories_id,$price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price', 'desc')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

    public function getCategorie_Price_BrandRateLow($categories_id,$price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();

        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('price')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }

    public function getCategories_Price_BrandRateName($categories_id,$price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->paginate(10);
        $count = DB::table('products')
                    ->where([['categories_id', '=', $categories_id],['price', '>=', $price1],['price', '<=', $price2], ['brand_id', '=', $brand_id], ['products.status_id', '=', 1] ])
                    ->orderBy('name')
                    ->count();

        $brand_id = $brand_id;       
        $categories_id = $categories_id;
        $price1 = $price1;
        $price2 = $price2;
        return view('categories', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'price1', 'price2', 'brand_id', 'count', 'user'));
    }
}
