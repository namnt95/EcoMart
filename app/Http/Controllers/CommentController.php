<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Admin\User;
use App\Models\Admin\Comment;
use App\Models\Admin\Comment_2;
use App\Models\Admin\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = Auth::id();
        $count = DB::table('comments')
                ->where([ ['product_id', '=', $id],['user_id', '=', $user_id] ])
                ->count();
                // dd($count);
        // if ( $count == 0 ) {
            $comment = new Comment();
            $comment->comments_1 = $request->comments_1;
            $comment->user_id = $user_id;
            $comment->product_id = $id;
            $comment->save();
        // } else {
        //     $comment = DB::table('comments')
        //             ->where([ ['product_id', '=', $id],['user_id', '=', $user_id] ])
        //             ->select('id')
        //             ->first();
        //     $comment = $comment->id;
        //             // dd($comment);

        //     $comment = Comment::find($comment);
        //     $comment->comments_1 = $request->comments_1;
        //     $comment->save();
        // }
        return redirect()->back();

        
    }

    public function update_2(Request $request, $cmt_id,$product_id)
    {
        $user_id = Auth::id();
        // $count = DB::table('comments')
        //         ->where([ ['product_id', '=', $id],['user_id', '=', $user_id] ])
        //         ->count();
        //         // dd($count);
        // if ( $count == 0 ) {
            $comments_2 = new Comment_2();
            $comments_2->comments_2 = $request->comments_2;
            $comments_2->comments_1_id = $cmt_id;
            $comments_2->product_id = $product_id;
            $comments_2->user_id = $user_id;
            $comments_2->save();
        // } else {
        //     $comment = DB::table('comments')
        //             ->where([ ['product_id', '=', $id],['user_id', '=', $user_id] ])
        //             ->select('id')
        //             ->first();
        //     $comment = $comment->id;
        //             // dd($comment);

        //     $comment = Comment::find($comment);
        //     $comment->comments_1 = $request->comments_1;
        //     $comment->save();
        // }
        return redirect()->back();

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
