<?php

namespace App\Http\Controllers;


use DB;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Categories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HeaderController extends Controller
{
    public function header(){
        $categories = Categories::all();
        
        return view('inc-index.index', compact('categories'));
    }
}
