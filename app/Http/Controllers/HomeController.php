<?php

namespace App\Http\Controllers;

use Session;
use DB;
use App\Models\Admin\User;
use App\Models\Admin\Price;
use App\Models\Admin\Brand;
use App\Models\Admin\Categories;
use App\Models\Admin\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{


    protected $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::id();
        $users = User::all();
        $brands = Brand::all();

        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where('products.status_id', 1)
                    ->select('products.*','users.shop as shop')
                    ->orderBy('name')
                    ->paginate(20);
                    // dd($products);
        $count = Product::where('status_id', 1)->count();
        $categories = DB::table('categories')->get();
        $prices = Price::all();
        return view('inc-index.index', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page' ));
    }


    public function find(Request $request) {
        $customer = Product::where('name', 'like', '%' . $request->get('q') . '%')->get();
        return response()->json($customer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getRate_High()
    {
        $user = Auth::id();
        $users = User::all();
        $page = DB::table('products')->where('status_id', 1)->orderBy('name')->paginate(20);
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->where('products.status_id', 1)
                    ->select('products.*','users.shop as shop')
                    ->orderBy('price', 'desc')
                    ->paginate(10);
                
        $count = DB::table('products')
                ->orderBy('price', 'desc')
                ->count();

        return view('inc-index.index', compact('products', 'page', 'brands', 'prices', 'categories', 'count', 'user', 'users'));
    }

    public function getRate_Low()
    {
        $user = Auth::id();
        $users = User::all();
        $page = DB::table('products')->where('status_id', 1)->orderBy('name')->paginate(20);
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where('products.status_id', 1)
                ->select('products.*','users.shop as shop')
                ->orderBy('price')
                ->paginate(10);
                
        $count = DB::table('products')
                ->orderBy('price')
                ->count();

        return view('inc-index.index', compact('products', 'brands', 'prices', 'categories', 'count', 'user', 'users', 'page'));
    }

    public function getRate_Name()
    {
        $user = Auth::id();
        $users = User::all();
        $page = DB::table('products')->where('status_id', 1)->orderBy('name')->paginate(20);
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->where('products.status_id', 1)
                ->select('products.*','users.shop as shop')
                ->orderBy('name')
                ->paginate(10);
                
        $count = DB::table('products')
                ->orderBy('name')
                ->count();

        return view('inc-index.index', compact('products', 'brands', 'prices', 'categories', 'count', 'user', 'users', 'page'));
    }

    public function autocomplete(Request $request)
    {
        if ($request->ajax()){
            $find = User::where("name", "like", "%{$request->input('query')}%")->get();
            return response()->json($find);
        }
    }


    public function product_shop($shop_id)
    {

        $user = Auth::id();
        $users = User::all();
        $brands = Brand::all();
        
        $shop = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                    ->first();

        $products = DB::table('products')
                    ->where([ ['shopper', '=', $shop_id], ['status_id', '=', 1] ])
                    ->paginate(20);
       
        $categories = DB::table('categories')->get();
        $prices = Price::all();
        
        return view('product.product_shop', compact('users', 'brands','shop', 'products', 'categories', 'prices', 'user', 'page' ));
    }


    public function search(Request $request)
    {
        $search =  $request->input('search');

        $products = DB::table('products')
                ->where([ ['status_id', 1], ['name', 'like', '%'.$search.'%'] ])
                ->get();

        $user = Auth::id();
        $users = User::all();
        $brands = Brand::all();
        $page = DB::table('products')->where([ ['status_id', 1], ['name', 'like', '%'.$search.'%'] ])->orderBy('name')->paginate(20);

        $count = DB::table('products')->where([ ['status_id', 1], ['name', 'like', '%'.$search.'%'] ])->orderBy('name')->count();
        $categories = DB::table('categories')->get();
        $prices = Price::all();
        return view('search', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page', 'search' ));
    }

    public function news()
    {
        return view('news');
    }


}
