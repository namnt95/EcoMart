<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Admin\User;
use App\Models\Admin\Price;
use App\Models\Admin\Brand;
use App\Models\Admin\Categories;
use App\Models\Admin\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PriceController extends Controller
{
    
    public function showprice($price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([ ['products.price', '<=', $price2],['products.price', '>=', $price1], ['products.status_id', '=', 1] ])
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                ->paginate(10);

        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['status_id', '=', 1] ])
                ->count();
        $price1 = $price1;
        $price2 = $price2;

        return view('price.price', compact('prices', 'products', 'brands', 'categories', 'price1', 'price2', 'count', 'user'));
    }
    public function price_brand($price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->where([ ['products.price', '<=', $price2],['products.price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                    ->paginate(10);

        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->count();
        $price1 = $price1;
        $price2 = $price2;
        $brand_id = $brand_id;
        return view('price.price', compact('brand_id','prices', 'products', 'brands', 'categories', 'price1', 'price2', 'count', 'user'));
    }


    
    public function price_rate_high($price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                ->orderBy('price', 'desc')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->orderBy('price', 'desc')
                ->count();

        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

    public function price_rate_low($price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                ->orderBy('price')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->orderBy('price')
                ->count();
        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

    public function price_rate_name($price1,$price2)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                ->orderBy('name')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1] ])
                ->orderBy('name')
                ->count();

        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

    public function price_brand_rate_high($price1,$price2,$brand_id)
    {
        // dd($brand_id);
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                    ->join('users', 'products.shopper', '=', 'users.id')
                    ->select('products.*','users.shop as shop')
                    ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                    ->orderBy('price', 'desc')
                    ->paginate(10);
        // dd($products);        
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->orderBy('price', 'desc')
                ->count();

        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

    public function price_brand_rate_low($price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->select('products.*','users.shop as shop')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->orderBy('price')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->orderBy('price')
                ->count();
        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

    public function price_brand_rate_name($price1,$price2,$brand_id)
    {
        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $prices = Price::all();
        $products = DB::table('products')
                ->join('users', 'products.shopper', '=', 'users.id')
                ->select('products.*','users.shop as shop')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->orderBy('name')
                ->paginate(10);
                
        $count = DB::table('products')
                ->where([ ['price', '<=', $price2],['price', '>=', $price1], ['products.status_id', '=', 1], ['products.brand_id', '=', $brand_id] ])
                ->orderBy('name')
                ->count();

        $price1 = $price1;
        $price2 = $price2;
        return view('price.price', compact('products', 'brands', 'prices', 'categories', 'categories_id', 'count', 'user', 'price1', 'price2'));
    }

}
