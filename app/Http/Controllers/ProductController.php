<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Admin\Comment;
use App\Models\Admin\User;
use App\Models\Admin\Product;
use App\Models\Admin\Brand;
use App\Models\Admin\Status;
use App\Models\Admin\Price;
use App\Models\Admin\Categories;
use App\Models\Admin\Bills;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Customers;
use App\Models\Admin\Star;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\EditProductRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function star(Request $request,$stars, $id)
    {
        $user = Auth::user();
        $user_id = Auth::id();
        $categories = Categories::all();
        $product = Product::find($id);
        $star = DB::table('star')
                ->where('product_id', '=', $id)
                ->avg('vote_id');
        $count_star = DB::table('star')
                ->where('product_id', '=', $id)
                ->count();

            $count = DB::table('star')
                    ->where([ ['user_id', '=', $user_id],['product_id', '=', $id] ])
                    ->count();
            $star_rating = new Star();
            if ( $count == 0 ) 
            {
                $star_rating->vote_id = $stars;
                $star_rating->product_id = $request->id;
                $star_rating->user_id = $user_id;
                $star_rating->save();   
            } else {
                $star_rating = DB::table('star')
                    ->where([ ['user_id', '=', $user_id],['product_id', '=', $id] ])
                    ->select('id')
                    ->first();
                $star_rating = $star_rating->id;
                $star_rating = Star::find($star_rating);
                $star_rating->vote_id = $stars;
                $star_rating->save();
            }

        $shopper = DB::table('products')
                ->select('shopper')
                ->where('products.id', '=', $id)
                ->get();

        $shops = DB::table('users')
        ->join('products', 'users.id', '=', 'products.shopper')
        ->where('products.id', '=', $id)
        ->select('users.shop')
        ->get();



        return redirect()->back();
    }





    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $id = Auth::id();
        $user = Auth::user();
        $count = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->count();
        $products = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->paginate(10);


        $count_quantity = Product::where([ ['products.shopper', '=', $id], ['products.quantity', '=', 0] ])->count();
        $products_quantity = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.quantity', '=', 0] ])
                    ->paginate(10);

        $count_rating = Product::where([ ['products.shopper', '=', $id], ['products.rating', '<', 3] ])->count();
        $products_rating = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.rating', '<', 3] ])
                    ->paginate(10);

        $count_status_id = Product::where([ ['products.shopper', '=', $id], ['products.status_id', '=', 2] ])->count();
        $products_status_id = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.status_id', '=', 2] ])
                    ->paginate(10);
                    


        return view ('product.product', compact('products', 'user', 'count', 'products_quantity', 'count_quantity', 'products_status_id', 'count_status_id', 'count_rating', 'products_rating', 'id'));
        //
    }

    public function find(Request $request) {
        $products = Product::where('name', 'like', '%' . $request->get('q') . '%')->get();
        return response()->json($products);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $check =  DB::table('users')
                ->where([ ['id', '=', Auth::id()], ['post', '>', 0] ])
                ->get();
        $count =  DB::table('users')
                ->where([ ['id', '=', Auth::id()], ['post', '>', 0] ])
                ->first();
                // $check = $count->post;
             $check = count($check);
        if ( $check > 0 ) {
            $user = Auth::user();
            $brand = Brand::all();
            $categories = Categories::all();
            $post = User::find(Auth::id());
            $post->post = $post->post - 1;
            $post->update();
            return view('product.product_create', compact('brand', 'categories', 'user', 'count', 'check'));
        } else {
            flash('Bạn đã hết lượt đăng, vui lòng mua thêm lượt!!!')->success();
            $user = Auth::user();
            $brand = Brand::all();
            $categories = Categories::all();
            return view('product.product_create', compact('brand', 'categories', 'user', 'count', 'check'));
        }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->display = $request->display;
        $product->os = $request->os;
        $product->memory = $request->memory;
        $product->camera = $request->camera;
        $product->cpu = $request->cpu;
        $product->ram = $request->ram;
        $product->sim = $request->sim;
        $product->pin = $request->pin;
        $product->promotion = $request->promotion;
        $product->review = $request->review;
        $product->shopper = Auth::id();
        $product->brand_id = $request->brand_id;
        $product->categories_id = $request->categories_id;
        $product->status_id = '1';
        $product->view = '0';
        $product->rating = '5';
    
        if ($request->hasFile('img1')) {
            $file1 = $request->file('img1');
            $fileName1 = time().$file1->getClientOriginalName();
            $file1->storeAs('images/products', $fileName1);
            $product->img1 = 'images/products/'.$fileName1;
        }

        if ($request->hasFile('img2')) {
            $file2 = $request->file('img2');
            $fileName2 = time().$file2->getClientOriginalName();
            $file2->storeAs('images/products', $fileName2);
            $product->img2 = 'images/products/'.$fileName2;
        }

        if ($request->hasFile('img3')) {
            $file3 = $request->file('img3');
            $fileName3 = time().$file3->getClientOriginalName();
            $file3->storeAs('images/products', $fileName3);
            $product->img3 = 'images/products/'.$fileName3;
        }
        
        $product->save();
        flash('Đăng sản phẩm thành công!!!')->success();

        // $product = $this->product->createProduct($request->all());
        return redirect()->route('product.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Auth::user();
        $categories = Categories::all();
        $brands = Brand::all();
        $products = DB::table('products')
                    ->orderBy('view', 'desc')   
                    ->limit(5)
                    ->get();
                    // dd($products);
        $product = Product::find($id);
        $star = DB::table('star')
                ->where('product_id', '=', $id)
                ->avg('vote_id');
        $count_star = DB::table('star')
                ->where('product_id', '=', $id)
                ->count();
                // dd($star);

        $count_view = DB::table('products')
                ->where([ ['id', '=', $id], ['view', '=', 0] ])
                ->count();
        if ( $count_view == 0 ) {
            $product->view = 1;
            $product->save();
        } else {
            $product->view = $product->view + 1;
            $product->save();
        }
        
        
        $shopper = DB::table('products')
                ->select('shopper')
                ->where('products.id', '=', $id)
                ->get();

        $shops = DB::table('users')
        ->join('products', 'users.id', '=', 'products.shopper')
        ->where('products.id', '=', $id)
        ->select('users.*')
        ->get();

        $cmts = DB::table('comments')
            ->join('products', 'comments.product_id', '=', 'products.id')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->select('comments.*', 'users.name as name')
            ->orderBy('created_at')
            ->paginate(1);
            // dd($cmts);

        $cmts_2 = DB::table('comments')
            ->join('comment_2s', 'comments.id', '=', 'comment_2s.comments_1_id')
            ->join('users', 'users.id', '=', 'comment_2s.user_id')
            ->where('comment_2s.product_id', '=', $id)
            ->get();


        return view('product.product_signle', compact('products', 'product', 'brands', 'shops', 'categories', 'user', 'star', 'count_star', 'cmts', 'cmts_2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $product = Product::find($id);
        $categoriess = Categories::all();
        $brands = Brand::all();
        $status = Status::all();
        return view('product.product_edit', compact('product', 'categoriess', 'brands', 'status', 'user'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditProductRequest $request, product $product)
    {
        $product->update($request->all());

        if ($request->hasFile('img1')) {
            $file1 = $request->file('img1');
            $fileName1 = time().$file1->getClientOriginalName();
            $file1->storeAs('images/products', $fileName1);
            $product->img1 = 'images/products/'.$fileName1;
        }

        if ($request->hasFile('img2')) {
            $file2 = $request->file('img2');
            $fileName2 = time().$file2->getClientOriginalName();
            $file2->storeAs('images/products', $fileName2);
            $product->img2 = 'images/products/'.$fileName2;
        }

        if ($request->hasFile('img3')) {
            $file3 = $request->file('img3');
            $fileName3 = time().$file3->getClientOriginalName();
            $file3->storeAs('images/products', $fileName3);
            $product->img3 = 'images/products/'.$fileName3;
        }

        $product->save();
        flash('Thay đổi thành công!!!')->success();

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = Product::find($id);
        $product->forceDelete();
        flash('Xóa thành công!!!')->success();

        return redirect()->route('product.index');
    }

    public function status($id,$status)
    {
        $product = Product::find($id);
        $product->status_id = $status;
        $product->save();
        if( $status == 1 ) {
            flash('Hủy ẩn thành công!!!')->success();
        } else {
            flash('Ẩn thành công!!!')->success();
        }
        return redirect()->route('product.index');
    }


    public function exchange()
    {

        $id = Auth::id();

        $post= User::find($id);
        $this->data['post'] = $post->post;
        $this->data['pay'] = $post->pay;
        $this->data['user'] = Auth::user();
        $customers = DB::table('customers')
                    ->orderBy('id', 'desc')
                    ->get();
        $this->data['bills'] = Bills::all();
        $this->data['customerInfos'] = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where('products.shopper', '=', $id)
                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();
                        // dd();
        $this->data['count'] = count($this->data['customerInfos']);               


        $this->data['customerInfosXL'] = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '<>', 'Chưa xử lý'] ])
                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();

        $TotalMoney =   DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '=', '3'] ])
                        ->sum('total');


        $this->data['TotalMoney'] = $TotalMoney;   
        // dd($this->data['TotalMoney']);
        $this->data['countXL'] = count($this->data['customerInfosXL']);   



        $this->data['customerInfosCXL'] = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->join('bill_details', 'bill_details.bill_id', '=', 'bills.id')
                        ->join('products', 'bill_details.product_id', '=', 'products.id')
                        ->where([ ['products.shopper', '=', $id], ['bills.status', '=', 'Chưa xử lý'] ])

                        ->select('customers.name as customers', 'products.name as name', 'bill_details.quantity as quantity', 'bills.created_at as created', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->get();
        $this->data['countCXL'] = count($this->data['customerInfosCXL']);   

        $this->data['products_ratings'] = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->avg('rating');

        return view('product.product_exchange', $this->data);
        //
    }

    public function month($id) {

    }

    public function search(Request $request, $id)
    {
        $search =  $request->input('search');

        
        $user = Auth::id();
        $users = User::all();
        $brands = Brand::all();
        $products = DB::table('products')
                ->where([ ['status_id', 1], ['products.shopper', '=', $id], ['name', 'like', '%'.$search.'%'] ])
                ->paginate(20);
        

        $count_search = DB::table('products')->where([ ['status_id', 1], ['products.shopper', '=', $id], ['name', 'like', '%'.$search.'%'] ])->orderBy('name')->count();

        $count = DB::table('products')
                    ->where('products.shopper', '=', $id)
                    ->count();
        $categories = DB::table('categories')->get();
        $prices = Price::all();

        // count
        $count_quantity = Product::where([ ['products.shopper', '=', $id], ['products.quantity', '=', 0] ])->count();
        $products_quantity = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.quantity', '=', 0] ])
                    ->paginate(10);

        $count_rating = Product::where([ ['products.shopper', '=', $id], ['products.rating', '<', 3] ])->count();
        $products_rating = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.rating', '<', 3] ])
                    ->paginate(10);

        $count_status_id = Product::where([ ['products.shopper', '=', $id], ['products.status_id', '=', 2] ])->count();
        $products_status_id = DB::table('products')
                    ->where([ ['products.shopper', '=', $id], ['products.status_id', '=', 2] ])
                    ->paginate(10);
        return view('product.product', compact('users', 'brands', 'products', 'categories', 'count', 'prices', 'user', 'page', 'search', 'products_quantity', 'count_quantity', 'products_status_id', 'count_status_id', 'count_rating', 'products_rating', 'count_search' ));
    }
}
