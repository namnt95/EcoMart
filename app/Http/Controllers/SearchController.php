<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Customers;

class SearchController extends Controller
{
	public function view()
	{
		return view('view');
	}

    // public function find(Request $request) {
    // 	return Customers::where('name', 'like', '%' . $request->get('q') . '%')->get();
    // }


    public function find(Request $request) {
    	$customer = Customers::where('name', 'like', '%' . $request->get('q') . '%')->get();
    	return response()->json($customer);
    }

    public function autocomplete()
    {
    	$queries = Customers::where(function($query)
		{
			$tern = Input::get('$tern');
			$query->where('name', 'like', '%'.$tern.'%');
		})->take(6)->get();

		foreach ($queries as $query) {
			$results[] = ['id' => $query->id, 'value' => $query->name];
		}
		return Response::json($results);
    }
}	