<?php

namespace App\Http\Controllers;

use App\Models\Admin\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Models\Admin\Bills;
use App\Models\Admin\Bill_Details;
use App\Models\Admin\Customers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateShopRequest;


class UserController extends Controller
{

    protected $user;
    protected $role;



    public function __construct(User $user)
    {
        $this->user = $user;
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $users = DB::table('users')
                ->where('id', '=', Auth::id() )
                ->first();
        
        return view('user.user', compact('users', 'user'));

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        flash('Bạn phải đăng kí thành viên trước khi bán hàng!!!')->success();
        return redirect()->route('register');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $this->user->createUser($request->all());
        return redirect()->route('home.index', 'user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        $user = Auth::user();
        return view('user.user', compact('user', 'user'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        $check = DB::table('users')
                ->where([ ['id', '=', Auth::id()], ['shop', '!=', ''], ['status_shop', '=', '1'] ])
                ->get();
        $count = count($check);
        if ( $count == 0 ) {
            $user = Auth::user();
            return view('user.user_edit', compact('user'));
        } else {
            return redirect()->route('user.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateShopRequest $request, $id)
    {

        $user = User::find($id);     
        $user->password = Hash::make('$request->password');
        $user->name     = $request->name;
        $user->address  = $request->address;
        $user->phone    = $request->phone;
        $user->shop     = $request->shop;
        $user->addressshop = $request->addressshop;
        $user->phoneshop = $request->phoneshop;
        $user->linkshop = '';
        $user->status_shop = '2';
        $user->post = '3';

        $file1 = $request->file('front_img');
        $fileName1 = time().$file1->getClientOriginalName();
        $file1->storeAs('images/user', $fileName1);
        $user->front_img = 'images/user/'.$fileName1;

        $file2 = $request->file('backside_img');
        $fileName2 = time().$file2->getClientOriginalName();
        $file2->storeAs('images/user', $fileName2);
        $user->backside_img = 'images/user/'.$fileName2;

        $user->update();
        flash('Yêu cầu đăng ký đã được gửi, sau khi xác minh bạn được tặng 3 lần đăng hàng miễn phí, xin cảm ơn 。')->success();
        return redirect()->route('user.index');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        $user->delete();
        return redirect()->route('user.index');
    }
    

    public function order()
    {
        $this->data['user'] = Auth::user();
        $this->data['title'] = 'Quản lý hóa đơn';
        $customers = DB::table('customers')
                    ->where('name', Auth::user()->name)
                    ->join('bills', 'customers.id', '=', 'bills.customer_id')
                    ->join('bill_details', 'bills.id', '=', 'bill_details.bill_id')
                    ->select('customers.*', 'bills.status as bill_status' )
                    ->orderBy('id', 'desc')
                    ->get();
        $this->data['customers'] = $customers;
        $this->data['bills'] = Bills::all();
        $this->data['customerInfos'] = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->select('customers.*', 'bills.status as bill_status')
                        ->first();
        return view('user.user_order', $this->data);
    }


    public function details($id)
    {
        $this->data['user'] = Auth::user();
        $customerInfo = DB::table('customers')
                        ->join('bills', 'customers.id', '=', 'bills.customer_id')
                        ->select('customers.*', 'bills.id as bill_id', 'bills.total as bill_total', 'bills.note as bill_note', 'bills.status as bill_status')
                        ->where('customers.id', '=', $id)
                        ->first();

        $billInfo = DB::table('bills')
                    ->join('bill_details', 'bills.id', '=', 'bill_details.bill_id')
                    ->leftjoin('products', 'bill_details.product_id', '=', 'products.id')
                    ->where('bills.customer_id', '=', $id)
                    ->select('bills.*', 'bill_details.*', 'products.name as product_name')
                    ->get();
                    
        $this->data['customerInfo'] = $customerInfo;
        $this->data['billInfo'] = $billInfo;

        return view('user.user_orderdetails', $this->data);
    }


    public function user_personalinformation(user $user)
    {
        $user = Auth::user();
        $img = DB::table('users')
                ->where('id', '=', Auth::id())
                ->select('img')
                ->first();
        return view('user.user_personalinformation', compact('user', 'img'));
        
    }

    public function user_personalinformation_edit(User $user)
    {
        
        $user = Auth::user();

        return view('user.user_personalinformation', compact('user'));
        
    }

    

    public function user_update(Request $request, $id)
    {
        // dd($id);
        $user = User::find($id);           
        $user->name     = $request->name;
        $user->password = Hash::make('$request->password');
        $user->address  = $request->address;
        $user->phone    = $request->phone;
        // dd($request->img);
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $fileName = time().$file->getClientOriginalName();
            $file->storeAs('images/user', $fileName);
            $user->img = 'images/user/'.$fileName;
        }
        // dd($file);

        // dd($fileName);
        $user->update();
        flash('Thay đổi thành công.')->success();
        return redirect()->route('user.index');
    }
    
    public function shop($id)
    {
        $user = Auth::user();

        $users = DB::table('users')
                ->where('id', '=', $id)
                ->get();
        return view('user.user_shop', compact('users', 'user'));

        //
    }

    public function shop_edit(user $user)
    {
        $user = Auth::user();
        return view('user.user_shoppersonalinformation', compact('user'));
        
    }

    public function shop_update(Request $request, $id)
    {
        $user = User::find($id);      
        // dd($user);     
        $user->shop     = $request->shop;
        $user->addressshop = $request->addressshop ;
        $user->phoneshop = $request->phoneshop ;
        $user->linkshop =  $request->linkshop;
        $user->update();
        flash('Thay đổi thông tin thành công.')->success();
        return redirect()->route('shop', $user->id );
    }

    public function pay($id)
    {
        $user = User::find($id);   
        if ( $user->pay == 1 ) {
            $user->pay = 2;
            $user->update();
            flash('Gửi yêu cầu thanh toán thành công.')->success();
            return redirect()->route('product_exchange');
        } else {
            $user->pay = 1;
            $user->update();
            flash('Huỷ yêu cầu thanh toán thành công.')->success();
            return redirect()->route('product_exchange');
        }

       
    }

    public function buypost()
    {
        $money =   DB::table('users')
                        ->where('id', '=', Auth::id())
                        ->select('money')
                        ->first();
        $money = $money->money;

        $user = Auth::user();
        return view('user.buy_post', compact('user', 'money'));
    }

    public function buy(Request $request)
    {
        $user = User::find(Auth::id());
        if ( $user->money == 0 ) {
            flash('Số dư tài khoản không đủ, vui lòng nạp thêm tiền。')->error();
            return redirect()->route('buy_post');
        } elseif ( $user->money < ($request->post)*10000 ) {
            flash('Số dư tài khoản không đủ, vui lòng nạp thêm tiền。')->error();
            return redirect()->route('buy_post');
        } else {
            $user->post = $request->post;
        $user->money = $user->money - ($request->post)*10000 ;
        $user->update();
        
        $money =   DB::table('users')
                        ->where('id', '=', Auth::id())
                        ->select('money')
                        ->first();
        $money = $money->money;

        $user = Auth::user();
        return view('user.buy_post', compact('user', 'money')); 
        }
    }

    public function click($email) {
        $code = uniqid();
        $user = DB::table('users')
                ->where('email', '=', $email)
                ->first();
                $id = $user->id;
        $user = User::find($id);
        $user->status_id = 1;
        $user->code = str_shuffle($code);
        $user->update();
        flash('Kích hoạt tài khoản thành công. Bạn hãy đăng nhập và tận hưởng.')->success();
        return redirect()->route('home.index');
    }

}
