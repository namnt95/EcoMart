<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'address' => 'bail|required|max:255',
            'phone' => 'bail|required|regex:/[0-9]/|max:15',
            'shop' => 'bail|required|max:255',
            'addressshop' => 'bail|required|max:255',
            'phoneshop' => 'bail|required|regex:/[0-9]/|max:15',
            'front_img' => 'bail|required',
            'backside_img' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'                   => 'Tên không được để trống',
            'address.required'                => 'Địac chỉ không được để trống',
            'phone.required'                  => 'Số Điện Thoại không được để trống',
            'shop.required'                   => 'Tên Cửa Hàng không được để trống',
            'addressshop.required'            => 'Địa chỉ cửa hàng không được để trống',
            'phoneshop.required'              => 'SĐT cửa hàng không được để trống',
            'front_img.required'              => 'Ảnh không được để trống',
            'backside_img.required'           => 'Ảnh không được để trống',
        ];   
    }
    
}
