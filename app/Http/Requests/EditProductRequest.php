<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:100',
            'price' => 'bail|required|max:15',
            'quantity' => 'bail|required|max:15',
            'review' => 'bail|required|max:1000',
            'display' => 'bail|required|max:255',
            'os' => 'bail|required|max:255',
            'memory' => 'bail|required|max:255',
            'camera' => 'bail|required|max:255',
            'cpu' => 'bail|required|max:255',
            'ram' => 'bail|required|max:255',
            'sim' => 'bail|required|max:255',
            'pin' => 'bail|required|max:255',
        ];

    }
}
