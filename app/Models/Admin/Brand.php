<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Brand extends Authenticatable
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'brands';
    protected $fillable = ['id', 'name', 'img', 'status_id'];
    
	public function product()
    {
            return $this->hasMany('App\Models\Admin\Product');
    }

	public function status(){
        return $this->belongsTo('App\Models\Admin\Status');
    }

    public function getList()
    {   
        $builder = Brand::orderBy('name', 'ASC');
        return $builder->paginate(20);
    }

    public function getBrand()
    {
        $builder = Brand::orderBy('name', 'ASC');
        return $builder->paginate(20);
    }


    public function createBrand(array $data)
    {
        return Categories::create($data);
    }


}
