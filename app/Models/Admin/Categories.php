<?php

namespace App\Models\Admin;

use App\Models\Admin\Status;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Categories extends Authenticatable
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'categories';
    protected $fillable = ['id', 'name', 'status_id'];



    public function product()
    {
            return $this->hasMany('App\Models\Admin\Product');
    }


    public function status()
    {
        return $this->belongsTo('App\Models\Admin\Status');
    }
 

    public function getCategories()
    {
        $builder = Categories::orderBy('name', 'ASC');
        return $builder->paginate(20);
    }
    
    
    public function getStatus()
    {
        $this->belongsTo('App\Models\Admin\Status');
        return $builder = Status::get();
    }

    public function createCategories(array $data)
    {
        return Categories::create($data);
    }

    public function updateCategories(array $data)
    {
        return Categories::edit($data);
    }
}
