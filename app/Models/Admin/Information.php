<?php
namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Information extends Model
{
    // const News = 1;
    // const Introduce = 2;
    // const Contact = 3;
    // const Job = 4;

    // public static $information_id = [
    //     self::News => 'Tin Tức',
    //     self::Introduce => 'Giới Thiệu',
    //     self::Contact => 'Liên Hệ',
    //     self::Job => 'Tuyển dụng',
    // ];

    public function status(){
        return $this->belongsTo('App\Models\Admin\Status');
    }

    protected $dates = ['deleted_at'];
    protected $table = 'information';
    protected $fillable = ['id', 'title', 'news', 'information_id', 'status'];

    public function createInformation(array $data)
    {
        return Information::create($data);
    }

    public function getInformation()
    {
        $builder = Information::orderBy('id', 'ASC');
        return $builder->paginate(20);


    }

}
