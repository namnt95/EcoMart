<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Price extends Authenticatable
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'prices';
    protected $fillable = ['id', 'price1', 'price2'];

    public function categories(){
        return $this->hasMany('App\Models\Admin\Categories');
    }

    public function brand(){
        return $this->hasMany('App\Models\Admin\Brand');
    }

    public function product(){
        return $this->hasMany('App\Models\Admin\Product');
    }
    
     
}
