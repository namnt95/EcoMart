<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Product extends Authenticatable
{
    use SoftDeletes;
    

    protected $dates = ['deleted_at'];
    protected $table = 'products';
    protected $fillable = ['name', 'price', 'review', 'display', 'os', 'memory', 'camera', 'cpu', 'ram', 'sim', 'pin', 'promotion', 'img1', 'img2', 'img3', 'shopper', 'quantity' ,  'brand_id', 'categories_id', 'status_id', 'view'];


    public function status()
    {
        return $this->belongsTo('App\Models\Admin\Status');
    }

    public function categories()
    {
        return $this->belongsTo('App\Models\Admin\Categories');
    }

    public function brand()
    {
        return $this->belongsTo('App\Models\Admin\Brand');
    }


    public function getList()
    {
        $builder = Product::orderBy('name', 'ASC');

        return $builder->paginate(1);
    }

    public function createProduct(array $data)
    {
        return Product::create($data);
    }

    public function Show($data)
    {
        return $product =Product::find($data);
    }





}
