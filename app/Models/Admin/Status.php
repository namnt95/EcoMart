<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Status extends Authenticatable
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];
    protected $table = 'status';
    protected $fillable = ['id', 'status'];

    public function categories(){
        return $this->hasMany('App\Models\Admin\Categories');
    }

    public function brand(){
        return $this->hasMany('App\Models\Admin\Brand');
    }

    public function product(){
        return $this->hasMany('App\Models\Admin\Product');
    }

    public function information(){
        return $this->hasMany('App\Models\Admin\News');
    }
    
    public function getStatus()
    {
       return $status = DB::table('status')->get();

    }


     public function createStatus(array $data)
    {
        return Status::create($data);
    }
}
