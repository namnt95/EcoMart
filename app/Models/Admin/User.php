<?php

namespace App\Models\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use SoftDeletes;

    const ROLE_ADMIN = 1;
    const ROLE_USER = 2;

    public static $role = [
        self::ROLE_ADMIN => 'Quản trị viên',
        self::ROLE_USER => 'Nhân viên',
    ];

    public function findForPassport($identifier) {
        return User::orWhere('email', $identifier)->where('status', 1)->first();
    }

    protected $dates = ['deleted_at'];
    protected $table = 'users';
    protected $fillable = ['id', 'email', 'password', 'name', 'address', 'phone', 'role', 'shop', 'addressshop', 'phoneshop', 'linkshop', 'status_id', 'front_img', 'backside_img', 'status_shop'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function status(){
        return $this->belongsTo('App\Models\Admin\Status');
    }

    public function getList()
    {
        $builder = User::orderBy('email', 'ASC');

        return $builder->paginate(20);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUser(array $data)
    {
        $data['password'] = bcrypt($data['password']);
        // dd($data);
        return User::create($data);
    }



}
