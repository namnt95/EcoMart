<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 100)->unique();
            $table->string('password');
            $table->string('name', 255)->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable()->default(null);
            $table->string('img')->nullable();
            $table->string('code')->nullable();
            $table->rememberToken();
            $table->integer('role')->nullable();
            $table->string('shop')->nullable();
            $table->string('post')->nullable();
            $table->string('pay')->nullable();
            $table->string('money')->nullable();
            $table->text('addressshop')->nullable();
            $table->string('phoneshop')->nullable();
            $table->string('linkshop')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
