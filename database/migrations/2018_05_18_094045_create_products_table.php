<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->text('review')->nullable();
            $table->string('display')->nullable();
            $table->string('os')->nullable();
            $table->string('memory')->nullable();
            $table->string('camera')->nullable();
            $table->string('cpu')->nullable();
            $table->string('ram')->nullable();
            $table->string('sim')->nullable();
            $table->string('pin')->nullable();
            $table->string('promotion')->nullable();
            $table->string('img1');
            $table->string('img2')->nullable();
            $table->string('img3')->nullable();
            $table->string('view');
            $table->string('shopper');
            $table->string('quantity');
            $table->string('rating')->nullable();
            $table->integer('categories_id');
            $table->integer('brand_id');
            $table->integer('status_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
