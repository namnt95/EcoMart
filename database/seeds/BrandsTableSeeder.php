<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('brands');
        // $faker = Faker\Factory::create('vi_VN');
        // $limit = 100;
        // for ($i = 0; $i < $limit; $i++) {
        //     App\Models\Admin\Brand::create([
        //         'name' => $faker->email,
        //         'img' => $faker->email,
        //         'link' => $faker->email,
        //         'status_id' => 1
        //     ]);
        // }

         $brands = [
            ['name' => 'APPLE', 'img' => 'images/brands/1526891870brand4.png', 'status_id' => 1],
            ['name' => 'SAMSUNG', 'img' => 'images/brands/1526891911brand3.png', 'status_id' => 1],
            ['name' => 'NOKIA', 'img' => 'images/brands/1526891930brand1.png', 'status_id' => 1],
            ['name' => 'HTC', 'img' => 'images/brands/1526891939brand5.png', 'status_id' => 1],
            ['name' => 'SONY', 'img' => 'images/brands/1526891951brand6.png', 'status_id' => 1],
            ['name' => 'Asus', 'img' => 'images/brands/1526891951brand6.png', 'status_id' => 1],
            ['name' => 'Xiaomi', 'img' => 'images/brands/1526891951brand6.png', 'status_id' => 1],
            
            
            
            

        ];
        DB::table('brands')->insert($brands);
    }
}
