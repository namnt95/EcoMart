<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Điện Thoại', 'status_id'=> '1'],
            // ['name' => 'Laptop', 'status_id'=> '1'],
            // ['name' => 'Phụ Kiện Điện Thoại', 'status_id'=> '1'],
            // ['name' => 'Phụ Kiện Laptop', 'status_id'=> '1'],
            // ['name' => 'Âm Thanh', 'status_id'=> '1'],
        ];
        DB::table('categories')->insert($categories);

        
    }
}
