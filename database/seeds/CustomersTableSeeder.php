<?php

use Illuminate\Database\Seeder;


class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create('vi_VN');
        $limit = 100;
        for ($i = 0; $i < $limit; $i++) {
            App\Models\Admin\Customers::create([
                'email' => $faker->email,
                'name' => $faker->name,
                'address' => $faker->address,
                'phone_number' => $faker->e164PhoneNumber,
               	'note' => $faker->name,
        		'senddate' => $faker->phoneNumber,
            ]);                   

        }
    }
}
