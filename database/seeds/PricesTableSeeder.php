<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $price = [
            ['price1'=> '1', 'price2'=> '250000'],
            ['price1'=> '250000', 'price2'=> '500000'],
            ['price1'=> '500000', 'price2'=> '1000000'],
            ['price1'=> '1000000', 'price2'=> '2000000'],
            ['price1'=> '2000000', 'price2'=> '5000000'],
            ['price1'=> '5000000', 'price2'=> '10000000'],
            ['price1'=> '10000000', 'price2'=> '15000000'],
            ['price1'=> '15000000', 'price2'=> '1000000000'],
        ];
        DB::table('prices')->insert($price);
    }
}
