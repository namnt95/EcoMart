<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $products = [
            ['name'=> 'Samsung Galaxy J6','price'=> '700','review'=> 'SAMSUNG', 'display'=> 'Super AMOLED, 5.6", HD+' ,
             'os'=> 'IOS 11', 'memory'=> '32G', 'camera'=> '13 MP
                ' , 'cpu'=> 'Exynos 7870 8 nhân 64-bit', 'ram'=> '  3 GB',
                 'sim'=> ' Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34790000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],
                   
            ['name'=> 'iPhone X 256GB','price'=> '34760000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34750000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34700000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '4', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34690000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '5', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34799900','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '5', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34699000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '6', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34790000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '7', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34780000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '8', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '350000000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '9', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'iPhone X 256GB','price'=> '34990000','review'=> 'iPhone X 256GB', 'display'=> 'OLED, 5.8", Super Retina' ,
             'os'=> 'IOS 11', 'memory'=> '256G', 'camera'=> '7 MP
                ' , 'cpu'=> 'Apple A11 Bionic 6 nhân', 'ram'=> '  3 GB',
                 'sim'=> '1 Nano SIM, Hỗ trợ 4G', 'pin'=> '   2716 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '10', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '1','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24990000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24999000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24900000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24790000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '23990000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '4', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '22990000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '5', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '25190000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '6', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24590000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '7', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24990000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '8', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '24800000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '9', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Samsung Galaxy S9+ 128GB','price'=> '23990000','review'=> 'Samsung Galaxy S9+ 128GB', 'display'=> 'Super AMOLED, 6.2", Quad HD+ (2K+)' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '128G', 'camera'=> '8 MP
                ' , 'cpu'=> '   Exynos 9810 8 nhân 64 bit', 'ram'=> '  6 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3500 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '10', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '2','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8990000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8900000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8500000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8990000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '4', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8790000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '5', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '7990000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '6', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '9190000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '7', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8490000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '8', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8999000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '9', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'Nokia 7 plus','price'=> '8990000','review'=> 'Nokia 7 plus', 'display'=> 'IPS LCD, 6", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 660 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   3800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '10', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '3','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8990000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8790000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '9000000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> '   Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8590000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '4', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8190000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '5', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8900000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '6', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8990000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '7', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8490000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '8', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '8999000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '9', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'HTC U Ultra','price'=> '7990000','review'=> 'HTC U Ultra', 'display'=> 'Super LCD, Chính: 5.7", phụ: 2.05", Quad HD (2K)' ,
             'os'=> 'Android 7.0 (Nougat)', 'memory'=> '64G', 'camera'=> '16 MP
                ' , 'cpu'=> 'Qualcomm Snapdragon 821 4 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3000 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '10', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '4','status_id'=> '1', ],

            ['name'=> 'Sony Xperia XZ2','price'=> '19990000','review'=> 'Sony Xperia XZ2', 'display'=> 'IPS HDR LCD, 5.7", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'Snapdragon 845 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3180 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '5','status_id'=> '1', ],

            ['name'=> 'Sony Xperia XZ2','price'=> '18990000','review'=> 'Sony Xperia XZ2', 'display'=> 'IPS HDR LCD, 5.7", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'Snapdragon 845 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3180 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '5','status_id'=> '1', ],

            ['name'=> 'Sony Xperia XZ2','price'=> '19590000','review'=> 'Sony Xperia XZ2', 'display'=> 'IPS HDR LCD, 5.7", Full HD+' ,
             'os'=> 'Android 8.0 (Oreo)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'Snapdragon 845 8 nhân', 'ram'=> '  4 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   3180 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '5','status_id'=> '1', ],

            ['name'=> 'ASUS Zenfone Max Plus M1 - ZB570TL','price'=> '4490000','review'=> 'ASUS Zenfone Max Plus M1 - ZB570TL', 'display'=> 'IPS LCD, 5.7", Full HD+' ,
             'os'=> '   Android 7.0 (Nougat)', 'memory'=> '32G', 'camera'=> '8 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  3 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   4130 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '6','status_id'=> '1', ],

            ['name'=> 'ASUS Zenfone Max Plus M1 - ZB570TL','price'=> '4790000','review'=> 'ASUS Zenfone Max Plus M1 - ZB570TL', 'display'=> 'IPS LCD, 5.7", Full HD+' ,
             'os'=> '   Android 7.0 (Nougat)', 'memory'=> '32G', 'camera'=> '8 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  3 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   4130 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '6','status_id'=> '1', ],

            ['name'=> 'ASUS Zenfone Max Plus M1 - ZB570TL','price'=> '4990000','review'=> 'ASUS Zenfone Max Plus M1 - ZB570TL', 'display'=> 'IPS LCD, 5.7", Full HD+' ,
             'os'=> '   Android 7.0 (Nougat)', 'memory'=> '32G', 'camera'=> '8 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  3 GB',
                 'sim'=> '2 Nano SIM, Hỗ trợ 4G', 'pin'=> '   4130 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '6','status_id'=> '1', ],

            ['name'=> 'Xiaomi Mi A1 64GB','price'=> '5490000','review'=> 'Xiaomi Mi A1 64GB', 'display'=> ' LTPS LCD, 5.5", Full HD' ,
             'os'=> '   Android 7.1 (Nougat)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   30800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '1', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '7','status_id'=> '1', ],

            ['name'=> 'Xiaomi Mi A1 64GB','price'=> '5990000','review'=> 'Xiaomi Mi A1 64GB', 'display'=> ' LTPS LCD, 5.5", Full HD' ,
             'os'=> '   Android 7.1 (Nougat)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   30800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '2', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '7','status_id'=> '1', ],

            ['name'=> 'Xiaomi Mi A1 64GB','price'=> '5190000','review'=> 'Xiaomi Mi A1 64GB', 'display'=> ' LTPS LCD, 5.5", Full HD' ,
             'os'=> '   Android 7.1 (Nougat)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   30800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '3', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '7','status_id'=> '1', ],

            ['name'=> 'Xiaomi Mi A1 64GB','price'=> '5890000','review'=> 'Xiaomi Mi A1 64GB', 'display'=> ' LTPS LCD, 5.5", Full HD' ,
             'os'=> '   Android 7.1 (Nougat)', 'memory'=> '64G', 'camera'=> '5 MP
                ' , 'cpu'=> 'MT6750T 8 nhân 64-bit', 'ram'=> '  4 GB',
                 'sim'=> '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', 'pin'=> '   30800 mAh',
                  'promotion'=> 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ' ,
                  'img1'=> 'images/products/Smartphone/.png','img2'=> 'images/products/Smartphone/.png',
                  'img3'=> 'images/products/Smartphone/.png','view'=> '1','shopper'=> '4', 'quantity'=> '3' ,
                   'rating'=> '5','categories_id'=> '1','brand_id'=> '7','status_id'=> '1', ],








             ];







        DB::table('products')->insert($products);


    }
}
