<?php

use Illuminate\Database\Seeder;

class StarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $star = [
            ['product_id'=> '1', 'vote_id'=> '3', 'user_id'=>'1'],
            ['product_id'=> '1', 'vote_id'=> '1', 'user_id'=>'2'],
            ['product_id'=> '1', 'vote_id'=> '2', 'user_id'=>'3'],
            ['product_id'=> '1', 'vote_id'=> '5', 'user_id'=>'4'],
            ['product_id'=> '1', 'vote_id'=> '4', 'user_id'=>'5'],
           
        ];
        DB::table('star')->insert($star);
    }
}
