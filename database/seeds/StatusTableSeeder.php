<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('status');
        // $faker = Faker\Factory::create('vi_VN');
        // $limit = 100;
        // for ($i = 0; $i < $limit; $i++) {
        //     App\Models\Admin\Status::create([
        //         'status' => 1
        //     ]);
        // }

        $status = [
            ['status'=> 'Hiển Thị'],
            ['status'=> 'Không Hiển Thị'],
        ];
        DB::table('status')->insert($status);
    }
}
