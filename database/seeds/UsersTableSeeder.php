<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = uniqid();
        $users = [
            ['email'=> 'nguyentrongnam2307@gmail.com','password'=> bcrypt('nguyentrongnam2307@gmail.com'),'name'=> 'nguyentrongnam2307@gmail.com',
             'address'=> 'Hà nội', 'phone'=>'01678396404','role'=> '1', 'shop' => 'Trần Anh', 'post'=> 3, 'addressshop'=> 'Hà Nội','phoneshop'=>'01678397404',
             'linkshop'=> 'google.com','status_id' => 1, 'code'=> str_shuffle($code)],
            
            ['email'=> 'nguyentrongnam230701@gmail.com','password'=> bcrypt('nguyentrongnam230701@gmail.com'),'name'=> 'nguyentrongnam230701@gmail.com',
             'address'=> 'Hà nội', 'phone'=>'01678350230','role'=> '1', 'shop' => 'smatphone24h', 'post'=> 3, 'addressshop'=>'24 Nguyễn Chí Thanh, Đống Đa, Hà Nội','phoneshop'=>'01678350230',
             'linkshop'=> 'smartphone24h.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'hoangmaianh261295@gmail.com','password'=> bcrypt('hoangmaianh261295@gmail.com'),'name'=> 'hoangmaianh',
             'address'=> 'Hà nội', 'phone'=>'01674176287','role'=> '1', 'shop' => 'Mai Anh', 'post'=> 3, 'addressshop'=>'242, đường Láng, Đống Đa, Hà Nội','phoneshop'=>'01674176287',
             'linkshop'=> 'didongmoi.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'lavandarttr09@gmail.com','password'=> bcrypt('lavandarttr09@gmail.com'),'name'=> 'lavandarttr09',
             'address'=> 'Hà nội', 'phone'=>'01676566404','role'=> '1', 'shop' => 'Lavandar shop', 'post'=> 3, 'addressshop'=>'278 Giải Phóng, Hoàng Mai, Hà Nội','phoneshop'=>'01676566404',
             'linkshop'=> 'lavandarttr09.com','status_id' => 1, 'code'=> str_shuffle($code)],
            
            ['email'=> 'hoangthanhthuong@gmail.com','password'=> bcrypt('hoangthanhthuong@gmail.com'),'name'=> 'hoangthanhthuong',
             'address'=> 'Hà nội', 'phone'=>'01678290404','role'=> '1', 'shop' => 'Sún Shop', 'post'=> 3, 'addressshop'=>'4 Hạ Đình, Thanh Xuân, Hà Nội','phoneshop'=>'01678290404',
             'linkshop'=> 'didongnhasun.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'didong999@gmail.com','password'=> bcrypt('didong999@gmail.com'),'name'=> 'didong999',
             'address'=> 'Hà nội', 'phone'=>'01654390404','role'=> '1', 'shop' => 'Di Động 999', 'post'=> 3, 'addressshop'=>'256 Phạm Văn Đồng, Từ Liêm, Hà Nội','phoneshop'=>'01654390404',
             'linkshop'=> 'didong999.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'smartphoneTNT@gmail.com','password'=> bcrypt('smartphoneTNT@gmail.com'),'name'=> 'smartphoneTNT',
             'address'=> 'Hà nội', 'phone'=>'01692453704','role'=> '1', 'shop' => 'Smartphone TNT', 'post'=> 3, 'addressshop'=>'1 Vũ Trọng Phụng, Thanh Xuân, Hà Nội','phoneshop'=>'01692453704',
             'linkshop'=> 'smartphoneTNT.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'dienthoai365@gmail.com','password'=> bcrypt('dienthoai365@gmail.com'),'name'=> 'dienthoai365',
             'address'=> 'Hà nội', 'phone'=>'01692090404','role'=> '1', 'shop' => 'Điện Thoại 365', 'post'=> 3, 'addressshop'=>'88 đường Thanh Niên, Hoàn Kiếm, Hà Nội','phoneshop'=>'01692090404',
             'linkshop'=> 'dienthoai365.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'castana0909@gmail.com','password'=> bcrypt('castana0909@gmail.com'),'name'=> 'castana',
             'address'=> 'Hà nội', 'phone'=>'01609290404','role'=> '1', 'shop' => 'Castana Shop', 'post'=> 3, 'addressshop'=>'123 Đào Tấn, Ba Đình, Hà Nội','phoneshop'=>'01609290404',
             'linkshop'=> 'castanashop.com','status_id' => 1, 'code'=> str_shuffle($code)],

            ['email'=> 'Korento333@gmail.com','password'=> bcrypt('Korento333@gmail.com'),'name'=> 'korento333',
             'address'=> 'Hà nội', 'phone'=>'01678555404','role'=> '1', 'shop' => 'Korento333', 'post'=> 3, 'addressshop'=>'82 Ô Chợ Dừa, Đống Đa, Hà Nội','phoneshop'=>'01678555404',
             'linkshop'=> 'korento333.com','status_id' => 1, 'code'=> str_shuffle($code)],


        ];
        DB::table('users')->insert($users);


    }

}
