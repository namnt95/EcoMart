-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2018 at 09:57 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomarttttt`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date_order` datetime NOT NULL,
  `total` double NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `customer_id`, `date_order`, `total`, `note`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-06-10 00:00:00', 8990000, 'giao hàng chấp tối', 'Chưa xử lý', '2018-06-10 03:31:12', '2018-06-10 03:31:12'),
(2, 2, '2018-06-10 00:00:00', 33880000, 'Gửi đến sớm', '2', '2018-06-10 15:47:31', '2018-06-10 12:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `bill_details`
--

CREATE TABLE `bill_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `bill_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bill_details`
--

INSERT INTO `bill_details` (`id`, `bill_id`, `product_id`, `quantity`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 24, 1, 8990000, '2018-06-10 03:31:12', '2018-06-10 03:31:12'),
(2, 2, 15, 1, 24900000, '2018-06-10 15:47:31', '2018-06-10 15:47:31'),
(3, 2, 47, 2, 4490000, '2018-06-10 15:47:31', '2018-06-10 15:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `img`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'APPLE', 'images/brands/1528601087brands_5.jpg', 1, NULL, '2018-06-09 20:24:47', NULL),
(2, 'SAMSUNG', 'images/brands/1528601021brands_4.jpg', 1, NULL, '2018-06-09 20:23:41', NULL),
(3, 'NOKIA', 'images/brands/1528601140brands_1.jpg', 1, NULL, '2018-06-09 20:25:40', NULL),
(4, 'HTC', 'images/brands/1528601069brands_7.jpg', 2, NULL, '2018-06-09 22:12:15', NULL),
(5, 'SONY', 'images/brands/1528601040brands_6.jpg', 1, NULL, '2018-06-09 20:24:00', NULL),
(6, 'Asus', 'images/brands/1528601099brands_8.jpg', 1, NULL, '2018-06-09 20:24:59', NULL),
(7, 'Xiaomi', 'images/brands/1528601058brands_3.jpg', 1, NULL, '2018-06-09 20:24:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Điện Thoại', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comments_1` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comments_1`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Hàng đẹp chất lượng', 24, 1, '2018-06-09 20:12:24', '2018-06-09 20:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `comment_2s`
--

CREATE TABLE `comment_2s` (
  `id` int(10) UNSIGNED NOT NULL,
  `comments_2` text COLLATE utf8mb4_unicode_ci,
  `comments_1_id` text COLLATE utf8mb4_unicode_ci,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment_2s`
--

INSERT INTO `comment_2s` (`id`, `comments_2`, `comments_1_id`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Pin tốt không bạn', '1', 24, 2, '2018-06-09 20:14:12', '2018-06-09 20:14:12');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `senddate` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `name`, `email`, `address`, `phone_number`, `note`, `senddate`, `created_at`, `updated_at`) VALUES
(1, 2, 'Hoàng Anh', 'nguyentrongnam230701@gmail.com', '84, Tân Nhuệ Hà nội', '01678350230', 'giao hàng chấp tối', '2018-06-24 00:00:00', '2018-06-09 20:31:12', '2018-06-09 20:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(133, '2018_05_18_093922_create_status_table', 1),
(134, '2018_05_18_093952_create_brands_table', 1),
(135, '2018_05_18_094023_create_categories_table', 1),
(136, '2018_05_18_094037_create_users_table', 1),
(137, '2018_05_18_094045_create_products_table', 1),
(138, '2018_05_25_073425_create_price_table', 1),
(139, '2018_05_27_103528_create_customers_table', 1),
(140, '2018_05_27_122259_create_bills_table', 1),
(141, '2018_05_27_122403_create_bill_details_table', 1),
(142, '2018_06_06_072202_create_star_table', 1),
(143, '2018_06_07_063006_create_comments_table', 1),
(144, '2018_06_07_091115_create_comment_2s_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `prices`
--

CREATE TABLE `prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `price1` int(11) NOT NULL,
  `price2` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prices`
--

INSERT INTO `prices` (`id`, `price1`, `price2`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 250000, NULL, NULL, NULL),
(2, 250000, 500000, NULL, NULL, NULL),
(3, 500000, 1000000, NULL, NULL, NULL),
(4, 1000000, 2000000, NULL, NULL, NULL),
(5, 2000000, 5000000, NULL, NULL, NULL),
(6, 5000000, 10000000, NULL, NULL, NULL),
(7, 10000000, 15000000, NULL, NULL, NULL),
(8, 15000000, 1000000000, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `display` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memory` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `camera` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shopper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `review`, `display`, `os`, `memory`, `camera`, `cpu`, `ram`, `sim`, `pin`, `promotion`, `img1`, `img2`, `img3`, `view`, `shopper`, `quantity`, `rating`, `categories_id`, `brand_id`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'iPhone X 256GB', 34760000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574342ip2.jpg', 'images/products/1528574342ip.jpg', 'images/products/1528574342ip6.jpg', '1', '2', '3', '2', 1, 1, 1, NULL, '2018-06-09 12:59:02', NULL),
(4, 'iPhone X 256GB', 34750000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574608ip.jpg', 'images/products/1528574608ip2.jpg', 'images/products/1528574608ip6.jpg', '1', '3', '3', '2', 1, 1, 1, NULL, '2018-06-09 13:03:28', NULL),
(5, 'iPhone X 256GB', 34700000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575534ip2.jpg', 'images/products/1528575534ip.jpg', 'images/products/1528575534ip5.jpg', '1', '4', '1', '2', 1, 1, 1, NULL, '2018-06-10 07:36:21', NULL),
(6, 'iPhone X 256GB', 34690000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575695ip.jpg', 'images/products/1528575695ip2.jpg', 'images/products/1528575695ip6.jpg', '1', '5', '3', '2', 1, 1, 1, NULL, '2018-06-09 13:21:35', NULL),
(8, 'iPhone X 256GB', 34699000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575826ip2.jpg', 'images/products/1528575826ip5.jpg', 'images/products/1528575826ip4.png', '1', '6', '3', '2', 1, 1, 1, NULL, '2018-06-09 13:23:46', NULL),
(9, 'iPhone X 256GB', 34790000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576008ip.jpg', 'images/products/1528576008ip6.jpg', 'images/products/1528576008ip5.jpg', '1', '7', '3', '2', 1, 1, 1, NULL, '2018-06-09 13:26:48', NULL),
(10, 'iPhone X 256GB', 34780000, '<p>iPhone X 256GB</p>', 'OLED, 5.8\", Super Retina', 'IOS 11', '256G', '7 MP', 'Apple A11 Bionic 6 nhân', '3 GB', '1 Nano SIM, Hỗ trợ 4G', '2716 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576175ip.jpg', 'images/products/1528576176ip6.jpg', 'images/products/1528576176ip5.jpg', '1', '8', '3', '2', 1, 1, 1, NULL, '2018-06-09 13:29:36', NULL),
(13, 'Samsung Galaxy S9+ 128GB', 24990000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528573988s93.jpg', 'images/products/1528573988s92.jpg', 'images/products/1528573988s96.png', '1', '1', '3', '2', 1, 2, 1, NULL, '2018-06-09 12:53:08', NULL),
(14, 'Samsung Galaxy S9+ 128GB', 24999000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574376s91.jpg', 'images/products/1528574376s92.jpg', 'images/products/1528574376s96.png', '1', '2', '3', '2', 1, 2, 1, NULL, '2018-06-09 12:59:36', NULL),
(15, 'Samsung Galaxy S9+ 128GB', 24900000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574990s9.jpg', 'images/products/1528574990s91.jpg', 'images/products/1528574990s97.jpg', '1', '3', '2', '2', 1, 2, 1, NULL, '2018-06-10 08:29:46', NULL),
(16, 'Samsung Galaxy S9+ 128GB', 24790000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575360s91.jpg', 'images/products/1528575360s93.jpg', 'images/products/1528575360s97.jpg', '1', '3', '1', '2', 1, 2, 1, NULL, '2018-06-10 07:34:46', NULL),
(17, 'Samsung Galaxy S9+ 128GB', 23990000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575569s93.jpg', 'images/products/1528575569s91.jpg', 'images/products/1528575569s92.jpg', '1', '4', '3', '2', 1, 2, 1, NULL, '2018-06-09 13:19:29', NULL),
(18, 'Samsung Galaxy S9+ 128GB', 22990000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575731s9.jpg', 'images/products/1528575731s9.png', 'images/products/1528575731s91.jpg', '1', '5', '3', '2', 1, 2, 1, NULL, '2018-06-09 13:22:11', NULL),
(19, 'Samsung Galaxy S9+ 128GB', 25190000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575853s91.jpg', 'images/products/1528575853s92.jpg', 'images/products/1528575853s97.jpg', '1', '6', '3', '2', 1, 2, 1, NULL, '2018-06-09 13:24:13', NULL),
(20, 'Samsung Galaxy S9+ 128GB', 24590000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576037s91.jpg', 'images/products/1528576037s92.jpg', 'images/products/1528576037s93.jpg', '1', '7', '3', '2', 1, 2, 1, NULL, '2018-06-09 13:27:17', NULL),
(21, 'Samsung Galaxy S9+ 128GB', 24990000, '<p>Samsung Galaxy S9+ 128GB</p>', 'Super AMOLED, 6.2\", Quad HD+ (2K+)', 'Android 8.0 (Oreo)', '128G', '8 MP', 'Exynos 9810 8 nhân 64 bit', '6 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3500 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576199s9.jpg', 'images/products/1528576199s95.png', 'images/products/1528576199s97.jpg', '1', '8', '3', '2', 1, 2, 1, NULL, '2018-06-09 13:29:59', NULL),
(24, 'Nokia 7 plus', 8990000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574018nokia.jpg', 'images/products/1528574018nokia1.jpg', 'images/products/1528574018nokia4.jpg', '1', '1', '3', '2', 1, 3, 1, NULL, '2018-06-09 12:53:38', NULL),
(25, 'Nokia 7 plus', 8900000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574418nokia.jpg', 'images/products/1528574418nokia6.jpg', 'images/products/1528574418nokia4.jpg', '1', '2', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:00:18', NULL),
(26, 'Nokia 7 plus', 8500000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575388nokia1.jpg', 'images/products/1528575388nokia2.png', 'images/products/1528575388nokia.jpg', '1', '3', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:16:28', NULL),
(27, 'Nokia 7 plus', 8990000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575613nokia1.jpg', 'images/products/1528575613nokia1.jpg', 'images/products/1528575613nokia2.png', '1', '4', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:20:13', NULL),
(28, 'Nokia 7 plus', 8790000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575760nokia.jpg', 'images/products/1528575760nokia1.jpg', 'images/products/1528575760nokia6.jpg', '1', '5', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:22:40', NULL),
(29, 'Nokia 7 plus', 7990000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575878nokia.jpg', 'images/products/1528575878nokia5.jpg', 'images/products/1528575878nokia3.png', '1', '6', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:24:38', NULL),
(30, 'Nokia 7 plus', 9190000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576061nokia.jpg', 'images/products/1528576061nokia1.jpg', 'images/products/1528576061nokia6.jpg', '1', '7', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:27:41', NULL),
(31, 'Nokia 7 plus', 8490000, '<p>Nokia 7 plus</p>', 'IPS LCD, 6\", Full HD+', 'Android 8.0 (Oreo)', '64G', '16 MP', 'Qualcomm Snapdragon 660 8 nhân', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '3800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576225nokia.jpg', 'images/products/1528576225nokia4.jpg', 'images/products/1528576225nokia6.jpg', '1', '8', '3', '2', 1, 3, 1, NULL, '2018-06-09 13:30:25', NULL),
(44, 'Sony Xperia XZ2', 19990000, '<p>Sony Xperia XZ2</p>', 'IPS HDR LCD, 5.7\", Full HD+', 'Android 8.0 (Oreo)', '64G', '5 MP', 'Snapdragon 845 8 nhân', '4 GB', '2 Nano SIM, Hỗ trợ 4G', '3180 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574054sony.jpg', 'images/products/1528574054sony3.jpg', 'images/products/1528574054sony4.png', '1', '1', '3', '2', 1, 5, 1, NULL, '2018-06-09 12:54:14', NULL),
(45, 'Sony Xperia XZ2', 18990000, '<p>Sony Xperia XZ2</p>', 'IPS HDR LCD, 5.7\", Full HD+', 'Android 8.0 (Oreo)', '64G', '5 MP', 'Snapdragon 845 8 nhân', '4 GB', '2 Nano SIM, Hỗ trợ 4G', '3180 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574486sony2.jpg', 'images/products/1528574486sony.jpg', 'images/products/1528574486sony3.jpg', '1', '2', '3', '2', 1, 5, 1, NULL, '2018-06-09 13:01:26', NULL),
(46, 'Sony Xperia XZ2', 19590000, '<p>Sony Xperia XZ2</p>', 'IPS HDR LCD, 5.7\", Full HD+', 'Android 8.0 (Oreo)', '64G', '5 MP', 'Snapdragon 845 8 nhân', '4 GB', '2 Nano SIM, Hỗ trợ 4G', '3180 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575444sony.jpg', 'images/products/1528575444sony3.jpg', 'images/products/1528575444sony2.jpg', '1', '3', '3', '2', 1, 5, 1, NULL, '2018-06-09 13:17:24', NULL),
(47, 'ASUS Zenfone Max Plus M1 - ZB570TL', 4490000, '<p>ASUS Zenfone Max Plus M1 - ZB570TL</p>', 'IPS LCD, 5.7\", Full HD+', 'Android 7.0 (Nougat)', '32G', '8 MP', 'MT6750T 8 nhân 64-bit', '3 GB', '2 Nano SIM, Hỗ trợ 4G', '4130 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574078asus1.jpg', 'images/products/1528574078asus.jpg', 'images/products/1528574078asus10.jpg', '1', '1', '-5', '2', 1, 6, 1, NULL, '2018-06-10 11:25:21', NULL),
(48, 'ASUS Zenfone Max Plus M1 - ZB570TL', 4790000, '<p>ASUS Zenfone Max Plus M1 - ZB570TL</p>', 'IPS LCD, 5.7\", Full HD+', 'Android 7.0 (Nougat)', '32G', '8 MP', 'MT6750T 8 nhân 64-bit', '3 GB', '2 Nano SIM, Hỗ trợ 4G', '4130 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574509asus1.jpg', 'images/products/1528574509asus.jpg', 'images/products/1528574509asus10.jpg', '1', '2', '-1', '2', 1, 6, 1, NULL, '2018-06-10 11:24:42', NULL),
(49, 'ASUS Zenfone Max Plus M1 - ZB570TL', 4990000, '<p>ASUS Zenfone Max Plus M1 - ZB570TL</p>', 'IPS LCD, 5.7\", Full HD+', 'Android 7.0 (Nougat)', '32G', '8 MP', 'MT6750T 8 nhân 64-bit', '3 GB', '2 Nano SIM, Hỗ trợ 4G', '4130 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575471asus1.jpg', 'images/products/1528575471asus10.jpg', 'images/products/1528575471asus.jpg', '1', '3', '3', '2', 1, 6, 1, NULL, '2018-06-09 13:17:51', NULL),
(50, 'Xiaomi Mi A1 64GB', 5490000, '<p>Xiaomi Mi A1 64GB</p>', 'LTPS LCD, 5.5\", Full HD', 'Android 7.1 (Nougat)', '64G', '5 MP', 'MT6750T 8 nhân 64-bit', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '30800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574109Xiaomi.jpg', 'images/products/1528574109Xiaomi3jpg.jpg', 'images/products/1528574109Xiaomi2.jpg', '1', '1', '3', '2', 1, 7, 1, NULL, '2018-06-09 12:55:09', NULL),
(51, 'Xiaomi Mi A1 64GB', 5990000, '<p>Xiaomi Mi A1 64GB</p>', 'LTPS LCD, 5.5\", Full HD', 'Android 7.1 (Nougat)', '64G', '5 MP', 'MT6750T 8 nhân 64-bit', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '30800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528574542Xiaomi.jpg', 'images/products/1528574542Xiaomi3jpg.jpg', 'images/products/1528574542Xiaomi2.jpg', '1', '2', '3', '2', 1, 7, 1, NULL, '2018-06-09 13:02:22', NULL),
(52, 'Xiaomi Mi A1 64GB', 5190000, '<p>Xiaomi Mi A1 64GB</p>', 'LTPS LCD, 5.5\", Full HD', 'Android 7.1 (Nougat)', '64G', '5 MP', 'MT6750T 8 nhân 64-bit', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '30800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528575417Xiaomi.jpg', 'images/products/1528575417Xiaomi3jpg.jpg', 'images/products/1528575417Xiaomi2.jpg', '1', '3', '3', '2', 1, 7, 1, NULL, '2018-06-09 13:16:57', NULL),
(53, 'Xiaomi Mi A1 64GB', 5890000, '<p>Xiaomi Mi A1 64GB</p>', 'LTPS LCD, 5.5\", Full HD', 'Android 7.1 (Nougat)', '64G', '5 MP', 'MT6750T 8 nhân 64-bit', '4 GB', '2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G', '30800 mAh', 'Khuyến mại: Hỗ trợ 4G HOT Mua sim Mobi Big 60 (3GB data/ tháng). Giá từ 80.000đ', 'images/products/1528576510Xiaomi.jpg', 'images/products/1528575641Xiaomi3jpg.jpg', 'images/products/1528575641Xiaomi.jpg', '1', '4', '3', '2', 1, 7, 1, NULL, '2018-06-09 13:35:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `star`
--

CREATE TABLE `star` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `vote_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `star`
--

INSERT INTO `star` (`id`, `product_id`, `vote_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, NULL, NULL),
(2, 1, 1, 2, NULL, NULL),
(3, 1, 2, 3, NULL, NULL),
(4, 1, 5, 4, NULL, NULL),
(5, 1, 4, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hiển Thị', NULL, NULL, NULL),
(2, 'Không Hiển Thị', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `shop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressshop` text COLLATE utf8mb4_unicode_ci,
  `phoneshop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkshop` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `address`, `phone`, `remember_token`, `role`, `shop`, `addressshop`, `phoneshop`, `linkshop`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'nguyentrongnam2307@gmail.com', '$2y$10$J1PBQYC2yUxPtCkcE0oFie./ufPbDlIJv/EYfoZ8IjEoa3EhYyeAe', 'Nam Nguyễn', 'Hà Nội', '01678396404', 'I3eF5obiy0XdniizXGzRriXhOlHGq1cWX7h4vD9PNDukL6yuXEtCA5xArt0d', 1, 'Trần Anh', 'Hà Nội', '016783974123', 'google.com', 2, NULL, '2018-06-10 12:54:05', NULL),
(2, 'nguyentrongnam230701@gmail.com', '$2y$10$hLo7am6PFd1vUNfHv2n/tev11ACwRtTlRUhzhcARxgkwrde.AG8eO', 'Hoàng Anh', 'Hà nội', '01678350230', 'fyli2X6fyOE56kzTxQ41KYaktKTB8dad4RNa8SnAKYazyUqDvCagNyaju1hP', 1, 'smatphone24h', '24 Nguyễn Chí Thanh, Đống Đa, Hà Nội', '01678350230', 'smartphone24h.com', 1, NULL, '2018-06-09 20:13:42', NULL),
(3, 'hoangmaianh261295@gmail.com', '$2y$10$47XI/k6ebD0s4idBNlOZU.13wrZzgRVf./KzXQCTM1IiF7nvJbkwC', 'hoangmaianh', 'Hà nội', '01674176287', 'MXhB8bKlMstM54cUXeczoToH0eyIbGB5dQGd2dEYxEkZv18fqg1XtRO8hoCc', 1, 'Mai Anh', '242, đường Láng, Đống Đa, Hà Nội', '01674176287', 'didongmoi.com', 1, NULL, NULL, NULL),
(4, 'lavandarttr09@gmail.com', '$2y$10$mhqFkU26tLxqW5pJGiV90.FcQcpwgdQz/Li7pElyqCRI2HnCyl3Qe', 'lavandarttr09', 'Hà nội', '01676566404', 'VPipe3s3UetdoWnfgaaWARVmaHPKXE7Cd1LxQQKBtn5F7XWjf9oREswzY0dd', 1, 'Lavandar shop', '278 Giải Phóng, Hoàng Mai, Hà Nội', '01676566404', 'lavandarttr09.com', 1, NULL, NULL, NULL),
(5, 'hoangthanhthuong@gmail.com', '$2y$10$cYaIQytMATOH/FjC8GdkCOR9BgEAhldR8nU2iYaQS5UT63HgLiE8y', 'hoangthanhthuong', 'Hà nội', '01678290404', '5dVPuQ8pETnoPfL4VdLQ9tuqwjSeDu9I0RP6qT7V1uMUKZTFQVnM2Dd3HnlC', 1, 'Sún Shop', '4 Hạ Đình, Thanh Xuân, Hà Nội', '01678290404', 'didongnhasun.com', 1, NULL, NULL, NULL),
(6, 'didong999@gmail.com', '$2y$10$gvf0W9dZNn.oFJtEhR50bO56enF7nKk8ghjQzCDk.TMrcgyl50572', 'didong999', 'Hà nội', '01654390404', 'BVjUvoIVevJ02kKVDQSIurtPXSASOCzITxv4RpNm4CeuJiSE3arTVBYCXBs6', 1, 'Di Động 999', '256 Phạm Văn Đồng, Từ Liêm, Hà Nội', '01654390404', 'didong999.com', 1, NULL, NULL, NULL),
(7, 'smartphoneTNT@gmail.com', '$2y$10$9ZtU8LCQwzvv5Xghd186zeVxQYmhO/NDmwM7sOR/OjDgI1oj2gYDi', 'smartphoneTNT', 'Hà nội', '01692453704', 'UbGKXaA8NNykEz2HbWkHyizsczjyLracgKdQ1JqzZhRg6HGarqlseHgl6WD9', 1, 'Smartphone TNT', '1 Vũ Trọng Phụng, Thanh Xuân, Hà Nội', '01692453704', 'smartphoneTNT.com', 1, NULL, NULL, NULL),
(8, 'dienthoai365@gmail.com', '$2y$10$QNTFnRTXu5IW.xzvpjoQ7.0fWme7nVkzHVVlRxqWN3tEjV07JB4yu', 'dienthoai365', 'Hà nội', '01692090404', 'QGn0q7htY2T54Nqm1T38nPa804GFBeDao0rAMztIQ9qFZfAyruLNTSoIyTpK', 1, 'Điện Thoại 365', '88 đường Thanh Niên, Hoàn Kiếm, Hà Nội', '01692090404', 'dienthoai365.com', 1, NULL, NULL, NULL),
(9, 'castana0909@gmail.com', '$2y$10$aMWIbfrpvwsJzOBJbANgduzBQpcsf0O2v.HOvz4nfe.LRS8sC5tW2', 'castana', 'Hà nội', '01609290404', '743hv6v8TRGKVt0TWoV3GzHHY2yl58RTrKCyGxqGYyLnkVKsm9gAFEf2Ngs7', 1, 'Castana Shop', '123 Đào Tấn, Ba Đình, Hà Nội', '01609290404', 'castanashop.com', 1, NULL, NULL, NULL),
(10, 'Korento333@gmail.com', '$2y$10$LP7EX6uMheB7b9fqgavxtuieROiqLwIPcVyZn7AqWvsmardttW5SS', 'korento333', 'Hà nội', '01678555404', 'zGWbvRLTXrwv4DeTtzvOw59NSMrSvBPLFuIQ4UA484pSznXJaJiNBNu4n4rr', 1, 'Korento333', '82 Ô Chợ Dừa, Đống Đa, Hà Nội', '01678555404', 'korento333.com', 1, NULL, NULL, NULL),
(11, 'asdasdas@gmail.com', '$2y$10$iUhQLHhZabs6b0DCIsecUeLfxQ8qij6pzdnMX.kb0mOxpIkz/gft2', 'asdasdas@gmail.com', NULL, NULL, 'mQY7FmK2S1ekNOqdzIfBcxLbeLx5Jz9F8z7hA192eMgrd6AeRjrwpiLi2arT', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:02:06', '2018-06-10 11:02:06', NULL),
(12, '123123@gmail.com', '$2y$10$4p9LPfeHBrWCBh661SnnQeu5IxAGtTqgvlkzcmXTcT5DDMdj3spju', '123123@gmail.com', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, '2018-06-10 11:05:40', '2018-06-10 12:48:05', NULL),
(13, 'nguyentrongnam23071@gmail.com', '$2y$10$hXIwnkViZjNDj/aoxS8u1eEEIuwH8A7lt39LzvbPqBqsqAvXfJOQ.', 'nguyentrongnam23071@gmail.com', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, '2018-06-10 11:08:23', '2018-06-10 11:08:23', NULL),
(14, 'nguyentrongnam230712@gmail.com', '$2y$10$iWInwKCofZqWCUlGXxH4jO50VRCjRWk045v9IIKT1Oxa4fPgDoe9a', 'nguyentrongnam23071@gmail.com', NULL, NULL, 'zsuTkSbzc3E39TA9JYv47TLVfDTbeB9xh6nzfpQGaYtRHB9SbtRi7uPe4190', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:09:03', '2018-06-10 11:09:03', NULL),
(15, 'asdasdasd@ga.com', '$2y$10$7Zx/KB5EuS3XTdcjLJh01eOXBePyoKy2WgZDd.2dbDS.sLrpw75w2', 'asdasdasd@ga.com', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:12:05', '2018-06-10 11:12:05', NULL),
(16, 'register@gasd.com', '$2y$10$OaPu5UbOnp4x431GCtGEH.0l7QUgdZKqgBBX7ptyRIjI4BKRUKjNK', 'register@gasd.com', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:14:25', '2018-06-10 11:14:25', NULL),
(17, 'registeraa@gasd.com', '$2y$10$SZB8m0m2G.qY7uznzBG1UOahsuGLNSQtG3FImQp2P3SdPZ8gNTbCS', 'register@gasd.com', NULL, NULL, 'PwICkaUqXToBsjUOUqa1CEwQ1wvkrqqWbzuMPfWZZLbqjAOOMxCWE71gsmHa', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:14:49', '2018-06-10 11:14:49', NULL),
(18, 'nguyentrongnasdasdam2307@gmail.com', '$2y$10$BUknKLz6pg48hyrZEs6KPe6YfufO.JUxO5zn46idMEAUuoc5ivZ7.', 'nguyentrongnasdasdam2307@gmail.com', NULL, NULL, 'BKUee95QOxuAQW8RSQnZaRvGanFIaPppgqy2iGmk2y4gxOfcYo4mgFnpyTEn', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:15:18', '2018-06-10 11:15:18', NULL),
(19, 'asdasasdasdsadasd@ga.com', '$2y$10$46gQUsIL7vDvwModgXrKK.Nu/gbYuzDGmCCYMTF7R/FJ.rd7c/lwa', 'asdasasdasdsadasd@ga.com', NULL, NULL, 'zk7wwC26sdvh7SQNbVCdS9fvop0g6TWFrHCaz25edagGNVz16tNvnbpDkxS6', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:17:34', '2018-06-10 11:17:34', NULL),
(20, 'dadasdasdasdas@gmai.cn', '$2y$10$wyjdCs3OFuG4vXZ8h0Em0.YVOxcUb3IeK9uaVV4ehWrP2pNhn8LdK', 'dadasdasdasdas@gmai.cn', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:19:23', '2018-06-10 11:19:23', NULL),
(21, 'nguyentrongnaaam2307@gmail.com', '$2y$10$WJvRBDwz4y32Vs2VpUZnCelzYhVG.7mghzxLaP6hTq0rBHpPzmKQW', 'nguyentrongnaaam2307@gmail.com', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:23:23', '2018-06-10 11:23:23', NULL),
(22, 'nguyentrongnaaam2a307@gmail.com', '$2y$10$.RODaL.wChDb.AhMUkeHoea6O3CKrlzi7n70fixcP3fVMhWzhfZaq', 'nguyentrongnaaam2307@gmail.com', 'ádasd', '0123265', 'hzsg4HP03Zo4qQd39TthVCxiEFXvPDz40XAZ9tDRKtQQ8WgSvWHbNlLLEC4k', 2, 'Hàng Nhật', 'cua ong', '08.0888 8888', 'google.com', NULL, '2018-06-10 11:24:25', '2018-06-10 11:39:27', NULL),
(23, 'nguyentrongnasdadasam2307@gmail.com', '$2y$10$OtZBYF09zUKXfrOVNDBMJ.r7BcnxfsWMfR1RyynYH3Q/rh42e.ag.', 'nguyentrongnasdadasam2307@gmail.com', 'nguyentrongnasdadasam2307@gmail.com', '132', NULL, NULL, 'nguyentrongnasdadasam2307@gmail.com', 'nguyentrongnasdadasam2307@gmail.com', '12312', 'google.comasdasdadasd', NULL, '2018-06-10 11:42:49', '2018-06-10 11:42:49', NULL),
(25, 'hungoilahung@gmail.com', '$2y$10$ERodaXhUqx8h4eyhdZ1p0O8g3OuE9wZQqlPaVNW09ghPBCAaADs6S', 'hungoilahung@gmail.com', 'hungoilahung@gmail.com', '123', NULL, NULL, 'hungoilahung@gmail.com', 'hungoilahung@gmail.com', '123', 'hungoilahung@gmail.com', NULL, '2018-06-10 11:44:55', '2018-06-10 11:44:55', NULL),
(28, 'hoangasdasds@gmail.com', '$2y$10$jBQmZ7ZoB33i/K7k9KbuwOgprt2me4ZuQSNpxffMK856rLZT8sCU.', 'hoangasdasds@gmail.com', 'hoangasdasds@gmail.com', '123123', 'L251KKaYCzCZlmPP2pIGNl9CrsxqlYQftke2JDLuILndfYokx58MGkWNTH2B', 2, 'hoangasdasds@gmail.com', 'hoangasdasds@gmail.com', '13123', 'hoangasdasds@gmail.com', NULL, '2018-06-10 11:51:24', '2018-06-10 11:51:41', NULL),
(29, 'qweqweqwe@as.c', '$2y$10$9dAMYLy3vTm3EVaSwugbiepGzNrUdnpg1BHCJMN.jNpzG50mdOmbu', 'qweqweqwe@as', NULL, NULL, 'atBT45JzeAFj7YN8B2HVsqsCiXqRIdsqQnS0te51PIFKCDIVaeuwnbnoHQju', 2, NULL, NULL, NULL, NULL, NULL, '2018-06-10 11:53:45', '2018-06-10 11:53:45', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_details`
--
ALTER TABLE `bill_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_2s`
--
ALTER TABLE `comment_2s`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prices`
--
ALTER TABLE `prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `star`
--
ALTER TABLE `star`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bill_details`
--
ALTER TABLE `bill_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comment_2s`
--
ALTER TABLE `comment_2s`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `prices`
--
ALTER TABLE `prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `star`
--
ALTER TABLE `star`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
