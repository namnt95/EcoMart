@extends('admin.inc.layouts')
@section('content')
    <section class="content-header">
        <div>
                    @include('flash::message')
                    <script>
                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                    </script>
                </div>
        <h1>
            Danh sách đơn hàng
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    @if (Session::has('message'))
        <div class="alert alert-info"> {{ Session::get('message') }}</div>
    @endif
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
            <div class="col-md-12">
                <table id="myTable" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                    <thead>
                    <tr role="row">
                        <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="" >ID</th>
                        <th class="sorting_asc col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="">Tên người order</th>
                        <th class="sorting col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Địa chỉ</th>
                        <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Ngày đặt hàng</th>
                        <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Ngày nhận hàng</th>
                        <th>Email</th>
                        <th>Trạng thái</th>
                        <th class="sorting col-md-1" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Action</th>
                        <th class="sorting col-md-2" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="">Xóa</th></tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($customers as $customer)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->address }}</td>
                                <td>{{ $customer->created_at }}</td>
                                <td>{{ $customer->senddate }}</td>
                                <td>{{ $customer->email }}</td>
                                        <td>
                                            @if ( $customer->bill_status == 1 || $customer->bill_status == 2 || $customer->bill_status == 3)
                                                @if ( $customer->bill_status == 1 )
                                                    Chưa giao
                                                @endif 
                                                @if ( $customer->bill_status == 2 )
                                                    Đang giao
                                                @endif 
                                                @if ( $customer->bill_status == 3 )
                                                    Đã giao
                                                @endif 
                                            @else
                                                {{ $customer->bill_status }}
                                            @endif
                                        </td>
                                        <td><a href="{{ url('bill_admin')}}/{{ $customer->id }}/edit">Chi Tiết</a></td>
                                        <td>
                                           
                                    {!! Form::open(array('route'=> ['bill_admin.destroy', $customer->id], 'method' => 'DELETE')) !!}
                                    
                                
                                    {!! Form::button('Xóa', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                                {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </section>
@endsection
