@extends('admin.inc.layouts')
@section('content')

    <!-- Main content -->
    <section class="content">
    <?php $i = ($brands->currentpage()-1)* $brands->perpage() + 1;?>
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh mục hãng</h3>
                <div>
                    @include('flash::message')
                    <script>
                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                    </script>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Img</th>
                        <th class="text-center">Trạng thái</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($brands as $brand)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $brand->name}}</td>
                        <td><img src=" {{ $brand->img }} "></td>
                        <td>
                                @if ( $brand->status_id  == '1')
                                    Hiển thị
                                @else
                                    Không hiển thị
                                @endif
                        <td>
                            {!! Form::open(array('route'=> ['brand_admin.destroy', $brand->id], 'method' => 'DELETE')) !!}
                                {{ link_to_route('brand_admin.edit', 'Edit', [$brand->id], ['class' => 'btn btn-primary btn-xs btn-block']) }}
                            
                                {!! Form::button('Delete', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="text-center">{!! $brands->links() !!}</div>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->

@endsection
