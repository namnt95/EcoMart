@extends('admin.inc.layouts')
@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Thêm mới hãng</h3>
            </div>
            <div>
                @include('flash::message')
                <script>
                $('div.alert').not('.alert-important').delay(1000).fadeOut(150);
                </script>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">

            {!!  Form::open(array('route' => ['brand_admin.store'], 'method' => 'POST', 'files' => true, 'enctype' =>"multipart/form-data" )) !!}
            
            <tr>
				<div class="form-group">
					<td>{!! Form::label('name','Hãng')  !!}</td>
					<td>{!! Form::text('name', null, ['class' => 'form-control' ])  !!}</td>
				</div>
			</tr>

            <tr>
				<div class="form-group">
					<td>{!! Form::label('img','Ảnh')  !!}</td>
					<td>{!! Form::file('img', null, ['class' => 'form-control' ])  !!}</td>
				</div>
			</tr>


			<tr>
				<div class="form-group">
					<td>{!! Form::label('status','Trạng Thái')  !!}</td>
					<td>
						<select name="status_id">
						@foreach($status as $status )
                              <option value=" {{ $status->id }}"> {{ $status->status }}</option>
                        @endforeach
                 		</select>
                 	</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td align="right"><a class="btn btn-link " href="{{ route('brand_admin.index') }}">Quay Lại</i></a></td>
					<td>{!! Form::button('Thêm', ['type' => 'submit', 'class' => 'btn btn-success'])  !!}</td>
				</div>
			</tr>

			{!! Form::close() !!}

            </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection

