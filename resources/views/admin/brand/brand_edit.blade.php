@extends('admin.inc.layouts')

@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục sản phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
			
            {!!  Form::model($brand,array('route' => ['brand_admin.update', $brand->id], 'method' => 'PUT', 'files' => true, 'enctype' =>"multipart/form-data" ))  !!}
                {{ csrf_field() }}

               <tr>
                <div class="form-group">
                    <td>{!! Form::label('name','Hãng')  !!}</td>
                    <td>{!! Form::text('name', null, ['class' => 'form-control' ])  !!}</td>
                </div>
            </tr>

            <tr>
                <div class="form-group">
                    <td> 
                        {!! Form::label('img','Ảnh')  !!}
                    </td>
                    <td>
                        <div><img src="{{ asset($brand->img) }}">   </div>
                        <br>
                        <div>{!! Form::file('img', null, ['class' => 'form-control' ])  !!}</div>
                    </td>
                </div>
            </tr>


            <tr>
                <div class="form-group">
                    <td>{!! Form::label('status_id','Trạng Thái')  !!}</td>
                    <td>
                        <select name="status_id">
                        @foreach($status as $status )
                              <option value=" {{ $status->id }}"> {{ $status->status }}</option>
                        @endforeach
                        </select>
                    </td>
                </div>
            </tr>
                
                <tr>
               <td align="right"><button type="#" class="btn btn-success">Quay lại</button></td>
                <td><button type="submit" class="btn btn-success">Thay đổi </button></td>
         		</tr>
                
            {!! Form::close() !!}

            </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection

