@extends('admin.inc.layouts')
@section('content')

    <!-- Main content -->
    <section class="content">
    <?php $i = ($categoriess->currentpage()-1)* $categoriess->perpage() + 1;?>


    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục sản phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên</th>
                        <th class="text-center">Trạng Thái</th>
                        <th>Hàng Động</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($categoriess as $categories)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $categories->name}}</td>
                            <td>
                                @if ( $categories->status_id == 1 )
                                    Hiển Thị
                                @else
                                    Không Hiển Thị
                                @endif
                            </td>
                            <td>
                             {!! Form::open(array('route'=> ['categories_admin.destroy', $categories->id], 'method' => 'DELETE')) !!}
                                {{ link_to_route('categories_admin.edit', 'Edit', [$categories->id], ['class' => 'btn btn-primary btn-xs btn-block']) }}
                            
                                {!! Form::button('Delete', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                            {!! Form::close() !!}
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="text-center">{!! $categoriess->links() !!}</div>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection

