@extends('admin.inc.layouts')
@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục sản phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
            <form action="{{ route('categories_admin.store') }}" method="POST" >
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('name')) style="color:red" @endif >Tên danh mục</label></td>
                    <td>
                        <input @if ($errors->first('name')) style="border-color:red" @endif type="text" name="name" class="form-control" placeholder="Nhập danh mục..."  }}">
                        @if ($errors->first('name'))
                            <span style="color: red">{{ $errors->first('name') }}</span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <div class="form-group">
                        <td><label @if ($errors->first('status')) style="color:red" @endif >Trạng thái</label></td>
                        <td>
                           <select name="status_id" class="form-control">
	                        @foreach($status as $status )
	                              <option value="{!! $status->id !!}"> {{ $status->status }}</option>
	                        @endforeach
	                        </select>
                        </td>
                    </div>
                </tr>
               
                
                <tr>
                    <div class="form-group">
                        <td align="right"><a class="btn btn-link" href="{{ route('categories_admin.index') }}">Trở Lại</i></a></td>
                        <td><button type="submit" name="register" class="btn btn-success">Thêm</button></td>
                    </div>
                </tr>
            </form>

            </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection
