@extends('admin.inc.layouts')
@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục thành viên</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
           {!!  Form::model($categories,array('route' => ['categories_admin.update', $categories->id], 'method' => 'PUT'))  !!}
			<tr>
				<div class="form-group">
					<td>{!! Form::label('name','Tên danh mục')  !!}</td>
					<td>{!! Form::text('name', null, ['class' => 'form-control' ])  !!}</td>
				</div>
			</tr>
			<tr>
				<div class="form-group">
					<td>{!! Form::label('status_id','Trạng Thái')  !!}</td>
					<td>
						<select name="status_id" class="form-control">
	                        @foreach($status as $status )
	                              <option value="{!! $status->id !!}"> {{ $status->status }}</option>
	                        @endforeach
                        </select>
					</td>
				</div>
			</tr>	

			<tr>
				<div class="form-group">
					<td align="right"><a  class="btn btn-link" href="{{ route('categories_admin.index') }}">Back</i></a></td>
					<td>{!! Form::button('Update', ['type' => 'submit', 'class' => 'btn btn-success'])  !!}</td>
				</div>
			</tr>
			{!! Form::close() !!}


        </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection

