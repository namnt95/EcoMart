  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('dist/img/avatar.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Tìm Kiếm...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Trang chủ</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Danh mục thể loại</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href=" {{ route('categories_admin.index') }} "><i class="fa fa-circle-o"></i>Danh sách thể loại</a></li>
            <li><a href=" {{ route('categories_admin.create') }} "><i class="fa fa-circle-o"></i>Thêm danh mục thể loại</a></li>
          </ul>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>sản phẩm</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=" {{ route('product_admin.index') }} "><i class="fa fa-circle-o"></i>Danh sách sản phẩm</a></li>
            <!-- <li><a href=" {{ route('product.create') }} "><i class="fa fa-circle-o"></i>Thêm sản phẩm</a></li> -->
          </ul>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Hãng sx</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           <li><a href=" {{ route('brand_admin.index') }} "><i class="fa fa-circle-o"></i>Danh sách hãng</a></li>
            <li><a href=" {{ route('brand_admin.create') }} "><i class="fa fa-circle-o"></i>Thêm hãng</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-google-plus"></i> <span>Thành viên</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=" {{ route('user_admin.index') }} "><i class="fa fa-circle-o"></i>Quản lý thành viên</a></li>
            <!-- <li><a href=" {{ route('user.create') }} "><i class="fa fa-circle-o"></i>Thêm thành viên</a></li> -->
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-google-plus"></i> <span>Shop</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href=" {{ route('shop_admin.index') }} "><i class="fa fa-circle-o"></i>Quản lý Shop</a></li>
              <li><a href=" {{ route('bill_admin.index') }} "><i class="fa fa-circle-o"></i>Chi tiết đơn hàng</a></li>
              <li><a href=" {{ route('shop_admin_pay') }} "><i class="fa fa-circle-o"></i>Yêu cầu thanh toán</a></li>
              <li><a href=" {{ route('register_shop') }} "><i class="fa fa-circle-o"></i>Đăng ký cửa hàng</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i> <span>Thống Kê</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href=" {{ route('profit.index') }} "><i class="fa fa-circle-o"></i>Thống Kê</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-google-plus"></i> <span>Tin Tức</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=" {{ route('information.index') }} "><i class="fa fa-circle-o"></i>Quản lý Tin Tức</a></li>
            <li><a href=" {{ route('information.create') }} "><i class="fa fa-circle-o"></i>Thêm tin tức</a></li>
            <!-- <li><a href=" {{ route('user.create') }} "><i class="fa fa-circle-o"></i>Thêm thành viên</a></li> -->
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->