@extends('admin.inc.layouts')
@section('content')

    <!-- Main content -->
    <section class="content">
      <section class="content-header">
      <h1>
        Chào mừng bạn đến với trang quản trị website
      </h1>
    </section>
    <br>
      <!-- Info boxes -->
      <div class="row">
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-paw"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Thành viên</span>
              <span class="info-box-number">{!! $count_user !!}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-reorder"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Danh mục sản phẩm</span>
              <span class="info-box-number">{!! $count_categories !!}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sản phẩm</span>
              <span class="info-box-number">{!! $count_product !!}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Đơn hàng</span>
              <span class="info-box-number">{!! $count_customer !!}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Đơn hàng Chưa Xử Lý</span>
              <span class="info-box-number">{!! $customerInfosCXL !!}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-fw fa-ship"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Nhà sản xuất</span>
              <span class="info-box-number">{!! $count_brand !!}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-reorder"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Danh mục tin tức </span>
              <span class="info-box-number">4<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-fw fa-pencil-square-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Bài viết</span>
              <span class="info-box-number">{{ $count_information }}<small></small></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        
         
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

     
      </div>
      <!-- /.row -->

     
    

     
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection