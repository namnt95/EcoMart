@extends('admin.inc.layouts')
@section('content')

    <!-- Main content -->
    <section class="content">
    <?php $i = ($informations->currentpage()-1)* $informations->perpage() + 1;?>
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh mục hãng</h3>
                <div>
                    @include('flash::message')
                    <script>
                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                    </script>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tiêu Đề</th>
                        <th class="text-center">Nội Dung</th>
                        <th class="text-center">Loại Tin Tức</th>
                        <th class="text-center">Trạng Thái</th>
                        <th class="text-center">Chi Tiết</th>
                        <th class="text-center">Hành Động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($informations as $information)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $information->title}}</td>
                        <td>{{ $information->news}}</td>
                        <td class="text-center">
                        	@if ( $information->information_id  == '1')
                                Tin Tức 
                            @elseif ( $information->information_id  == '2') 
                                 Giới Thiệu
                            @elseif ( $information->information_id  == '3') 
                            	Liên Hệ
                            @else
                            	Tuyển Dụng
                            @endif
                        </td>
                        
                        <td class="text-center">
                                @if ( $information->status  == '1')
                                    Hiển Thị
                                @else
                                    Không hiển thị
                                @endif
                        </td>
                        <td  class="text-center">
                        	<a href="{{ route('information.show', $information->id ) }}">Chi Tiết</a>
                        </td>
                        <td>
                            {!! Form::open(array('route'=> ['information.destroy', $information->id], 'method' => 'DELETE')) !!}
                                {{ link_to_route('information.edit', 'Edit', [$information->id], ['class' => 'btn btn-primary btn-xs btn-block']) }}
                            
                                {!! Form::button('Delete', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="text-center">{!! $informations->links() !!}</div>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->

@endsection
