@extends('admin.inc.layouts')
<script type="text/javascript" src="{{asset('ckeditor/ckeditor.js')}}"></script>

@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục Tin Tức</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
            {!!  Form::model($information,array('route' => ['information.update', $information->id], 'method' => 'PUT'))  !!}
                <tr>
                    <div class="form-group">
                        <td>{!! Form::label('title','Tiêu đề')  !!}</td>
                        <td>{!! Form::text('title', null, ['class' => 'form-control' ])  !!}
                            @if ($errors->first('title'))
                                <span style="color: red">{{ $errors->first('title') }}</span>
                            @endif
                        </td>
                    </div>
                </tr>

                <tr>
                    <div class="form-group">
                    <td><label  @if ($errors->first('news')) style="color:red" @endif >Nội dung</label></td>
                    <td>
                        {!! Form::textarea('news', null, ['class' => 'form-control', 'class' => 'ckeditor' ])  !!}
                    </td>
                    </div>

                </tr>

                <tr>
                    <div class="form-group">
                        <td>{!! Form::label('information_id','Loại tin tức')  !!}</td>
                        <td>
                            <select name="information_id" class="form-control">
                                <option value="1"> Tin Tức</option>
                              <option value="2"> Giới Thiệu </option>
                              <option value="3"> Liên Hệ </option>
                              <option value="4"> Tuyển Dụng </option>
                            </select>
                        </td>
                    </div>
                </tr>
                <tr>
                    <div class="form-group">
                        <td>{!! Form::label('status','Trạng thái')  !!}</td>
                        <td>
                            <select name="status" class="form-control">
                            @foreach($status as $status )
                                  <option value=" {{ $status->id }}"> {{ $status->status }}</option>
                            @endforeach
                            </select>

                        </td>
                    </div>
                </tr>
               
                <tr>
                    <div class="form-group">
                        <td align="right"><a class="btn btn-link " href="{{ route('information.index') }}">Back</i></a></td>
                        <td>{!! Form::button('Sửa', ['type' => 'submit', 'class' => 'btn btn-success'])  !!}</td>
                    </div>
                </tr>
                {!! Form::close() !!}
            </form>

            </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection
