@extends('admin.inc.layouts')
@section('content')

    <style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    tr:hover {background-color:#f5f5f5;}
    </style>


    <!-- Main content -->
    <section class="content">

     <div class="row">
        <?php $i = ($products->currentpage()-1)* $products->perpage() + 1;?>

    
        <div class="col-sm-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sản Phẩm</h3>
              <br>
            <br>
            <div style="margin-left: 10px"> 
                
                <form action=" {{ route('product_search_admin', Auth::user()->id) }}  " class="form-inline typeahead">
                    <div class="form-group">
                        <input type="search" class="form-control search-input" id="name" name="search" autocomplete="off" placeholder="Nhập tên sản phẩm">
                    </div>
                    <button type="submit" class="btn btn-default">Tìm kiếm</button>
                </form>
                </div>
            <br>
            <div style="color: red; margin-left: 10px">
            @if ( empty($count_search) )
                 
            @else 
               Tìm thấy {{ $count_search }} sản phẩm
            @endif
            </div>
            <br>
            <div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">Tên</th>
                            <th class="text-center">Giá</th>
                            <th class="text-center">review</th>
                            <th class="text-center">img</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $product->name}}</td>
                            <td>{{ $product->price }} Đ</td>
                            <td>
                                <table>
                                    <tr>
                                        <td width="40%"> Màn Hình   </td>
                                        <td>{!! $product->display !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Hệ Điều Hành    </td>
                                        <td>{!! $product->os !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Bộ Nhớ  </td>
                                        <td>{!! $product->memory !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Camera  </td>
                                        <td>{!! $product->camera !!}</td>
                                    </tr>

                                    <tr>
                                        <td>CPU</td>
                                        <td>{!! $product->cpu !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Ram</td>
                                        <td>{!! $product->ram !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Sim</td>
                                        <td>{!! $product->sim !!}</td>
                                    </tr>

                                    <tr>
                                        <td>Pin</td>
                                        <td>{!! $product->pin !!}</td>
                                    </tr>
                                </table>
                            </td>

                            <td align="center" ><img src="{{ asset($product->img1) }}  "></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                <div class="center">{!! $products->links() !!}</div>
            <div class="clearfix visible-sm-block"></div>
        </div>
            <!-- /.row -->
    </section>
            <!-- /.content -->
    </div>


</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection

