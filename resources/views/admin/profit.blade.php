@extends('admin.inc.layouts')

@section('content')

<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>


<!-- Main content -->
<section class="content">

 <div class="row">


    <div class="col-sm-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Thống Kê</h3>
          <br>
        <br>
        <table>
            <tr>
                <td width="550px">
                    <div id="piechart"></div>
                </td>
                <td>
                    <div style="margin-top: -100px">
                        <h4>Số lượng sản phẩm: {{ $product  }}</h4>
                        <h4>Sản phẩm hết hàng: {{ $product_het  }}</h4>
                        <h4>Sản phẩm còn hàng: {{ $product_con  }}</h4>
                        <h4>Sản phẩm chất lượng kém: {{ $product_rating  }}</h4>
                        <h4>Sản phẩm ẩn hoạt động: {{ $product_status_id  }}</h4>
                    </div>
                </td>
            </tr>
        </table>
          


    <div id="chart_div"></div>
    <hr>
    <div>
            Tổng doanh thu: {{ number_format($total, 0, ',', '.') }} Đ
            <br>
            Doanh thu % sản phẩm: {{ number_format($total_percent, 0, ',', '.') }} Đ
            <br>
            Doanh thu vận chuyển: {{ number_format($count, 0, ',', '.') }} Đ
            <br>
            Thanh toán cửa hàng: {{ number_format($total_shop, 0, ',', '.') }} Đ
    </div>
    <br>
    <div class="row no-print">
                <div class="col-md-12">
                  <a href="{{ route('invoice') }}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
                 
                </div>
              </div>
        <!-- /.row -->
    </div>
      </div>
        <div class="clearfix visible-sm-block"></div>
    </div>
        <!-- /.row -->
</section>
        <!-- /.content -->
</div>


</section>
<script src="{{ asset('js/thongke.js') }}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
// Load google charts
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Task', 'Hours per Day'],
  ['Hết Hàng', 9],
  ['Kém', 42],
  ['Còn Hàng', 44],
  ['Ẩn', 0],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Sản Phẩm', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}
</script>
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'X');
      data.addColumn('number', 'Lượt xem');

      data.addRows([
        [0, 00],   [1, 100],  [2, 230],  [3, 170],  [4, 180],  [5, 90],
        [6, 110],  [7, 270],  [8, 330],  [9, 400],  [10, 320], [11, 350],
        [12, 300], [13, 400], [14, 420], [15, 470], [16, 440], [17, 480],
        [18, 520], [19, 540], [20, 420], [21, 550], [22, 560], [23, 570],
        [24, 600], [25, 500], [26, 520], [27, 510], [28, 490], [29, 530],
        [30, 550], [31, 600], [32, 610], [33, 590], [34, 620], [35, 650],
        [36, 620], [37, 580], [38, 550], [39, 610], [40, 640], [41, 650],
        [42, 630], [43, 660], [44, 670], [45, 690], [46, 690], [47, 700],
        [48, 720], [49, 680], [50, 660], [51, 650], [52, 670], [53, 700],
        [54, 710], [55, 720], [56, 730], [57, 750], [58, 700], [59, 680],
        [60, 640], [61, 600], [62, 650], [63, 670], [64, 680], [65, 690],
        [66, 700], [67, 720], [68, 750], [69, 800]
      ]);

      var options = {
        hAxis: {
          title: 'Ngày'
        },
        vAxis: {
          title: 'Lượt truy cập'
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
</script>
<script src="{{ asset('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js')}}"></script>
@endsection

