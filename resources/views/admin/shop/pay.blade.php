@extends('admin.inc.layouts')

@section('content')
    
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="card">
            <div class="card-header">
              <h3 class="card-title">Yêu cầu thanh toán</h3>
            </div>
            <div class="card-body">
              <div id="example2_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable" >
                <thead>
                <tr role="row">
                	<th>STT</th>
                	<th>Cửa Hàng</th>
                	<th>Địa Chỉ</th>
                	<th>Yêu cầu</th>
                	<th>Thanh Toán</th>
                </tr>
                </thead>
                <tbody>
                	<?php $i = 0;  ?>
                @foreach ( $listPays as $listPay )
                <tr role="row" class="odd">
                	<td>{{ $i++ }}</td>
                  	<td>{{ $listPay->name }}</td>
                  	<td>{{ $listPay->addressshop }}</td>
                  	<td>{{ $listPay->phoneshop }}</td>
                  	<td><button>Thanh Toán</button></td>
                </tr>
                @endforeach
                </tbody>
              </table>
          </div>
      </div>
      
  </div>
            </div>
          </div>
    <div class="clearfix visible-sm-block"></div>
</div>

</section>
</div>
@endsection
