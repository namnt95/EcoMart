@extends('admin.inc.layouts')

@section('content')
    
             <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục Đăng ký cửa hàng</h3>
            </div>
            <br>

            <div style="margin-left: 10px"> 
                

                </div>
            <br>

            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên Cửa Hàng</th>
                        <th class="text-center">Địa chỉ</th>
                        <th  class="text-center">Hàng Động</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach ($users as $user)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td align="center">{{ $user->shop }}</td>
                            <td align="center">{{ $user->addressshop}}</td>
                            <td align="center">
                            <a href="{{ route('register_shop_details', $user->id) }}">Chi Tiết</a>
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection
