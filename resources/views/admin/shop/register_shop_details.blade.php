@extends('admin.inc.layouts')

@section('content')
    
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                <div class="card">
                    <div class="card-header">
                    @foreach ( $users as $user )
                      <h3 class="card-title">Đăng ký cửa hàng - {{ $user->shop }}</h3>
                      <p>Hình đăng đăng tải thông tin cá nhân</p>
                    </div>
                    <div class="card-body">
                    <table>
                        <tr>
                            <td style="border:1px solid black"><img src="{{ asset($user->front_img) }}" width="400px" height="350px"></td>
                            <td><img src="{{ asset($user->backside_img) }}"  width="400px" height="350px"></td>
                            
                                <td>
                                    <a  class="btn btn-success" href="{{ route('register_shop_update', $user->id) }}" style="margin-left: 50px"> Đồng ý</a>
                                    <br><br><br>

                                    <a  class="btn btn-success" href="{{ route('register_shop_destroy', $user->id) }}" style="margin-left: 50px"> Hủy</a>
                                </td>
                            </form>
                        </tr>
                    </table>
                     @endforeach
                   <!--  @foreach ( $users as $user )
                        <div><h4>{{ $user->shop }}</h4></div>
                        <div>
                            <img src="{{ asset($user->front_img) }}">
                        </div>
                        <div>
                            <img src="{{ asset($user->backside_img) }}">
                        </div>
                    @endforeach -->
                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix visible-sm-block"></div>
</section>
</div>
@endsection
