@extends('admin.inc.layouts')

@section('content')
    
             <!-- Main content -->
    <section class="content">
    <?php $i = ($users->currentpage()-1)* $users->perpage() + 1;?>
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục thông tin cửa hàng</h3>
            </div>
            <br>

            <div style="margin-left: 10px"> 
                @if ( empty($count_search) )
                 
            @else 
               Tìm thấy {{ $count_search }} sản phẩm
            @endif
                <form action=" {{ route('shop_search_admin', Auth::user()->id) }}  " class="form-inline typeahead">
                    <div class="form-group">
                        <input type="name" class="form-control search-input" id="name" name="search" autocomplete="off" placeholder="Nhập tên cửa hàng">
                    </div>
                    <button type="submit" class="btn btn-default">Tìm kiếm</button>
                </form>

                </div>
            <br>
              
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Tên Cửa Hàng</th>
                        <th class="text-center">Trạng Thái</th>
                        <th  class="text-center">Hàng Động</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td align="center">{{ $i++ }}</td>
                            <td>{{ $user->shop}}</td>
                            <td align="center">
                                @if ( $user->status_id == 1 )
                                    Hiển Thị
                                @else
                                    Không Hiển Thị
                                @endif
                            </td>
                            <td align="center">
                            <a href="{{ route('shop_admin.show', $user->id) }}">Chi Tiết</a>
                                
                                <!-- @if ( $user->status_id == 1 )
                                   <a href="{{ route('userBlock_admin', [$user->id]) }}" class="btn btn-warning">Chặn</a>
                                @else
                                    <a href="{{ route('userBlock_admin', [$user->id]) }}" class="btn btn-warning">Bỏ Chặn</a>
                                @endif -->
                                    
                                             
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="text-center">{!! $users->links() !!}</div>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection
