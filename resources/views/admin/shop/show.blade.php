@extends('admin.inc.layouts')
@section('content')
    <section class="content-header">
        <h1>
            Chi tiết Shop {{ $users->shop }}
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-md-12">
                        <div class="container123  col-md-6"   style="">
                            <h4></h4>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="col-md-4">Thông tin Shop </th>
                                    <th class="col-md-6"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Tên Shop</td>
                                    <td>{{ $users->shop }}</td>
                                </tr>
                                <tr>
                                    <td>Số điện thoại</td>
                                    <td>{{ $users->phoneshop }}</td>
                                </tr>
                                <tr>
                                    <td>Địa chỉ</td>
                                    <td>{{ $users->addressshop }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{ $users->email }}</td>
                                </tr>
                                <tr>
                                    <td>Trạng Thái</td>
                                    <td>
                                        @if ( $users->status_id == 2 )
                                           Chặn
                                        @else
                                            Hoạt Động
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Số lượng đơn hàng</td>
                                    <td>Tất Cả: {!! $count !!}  |  Đã Xử Lý: {!! $countXL !!} | Chưa Xử Lý: {!! $countCXL !!}</td>
                                </tr>
                                <tr>
                                    <td>Tổng Doanh Thu</td>
                                    <td> {{ $TotalMoney }} VNĐ </td>
                                </tr>
                                <tr>
                                    <td>Đánh Giá Chất Lượng Sản Phẩm</td>
                                    <td>
                                        @if ( $products_rating <= 1 )
                                            Tồi
                                        @elseif ( $products_rating > 1 && $products_rating <= 2 )
                                            Kém
                                        @elseif ( $products_rating > 2 && $products_rating <= 3 )
                                            Trung Bình
                                        @elseif ( $products_rating > 3 && $products_rating <= 4 )
                                            Khá
                                        @else 
                                            Tốt
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>

                <table style="margin-left: 15px">
                    <tr>
                        <td width="70px"><a href=" {{route('shop_admin.index')  }} ">Quay lại</a></td>
                        <td width="70px">
                            @if ( $users->status_id == 1 )
                               <a href="{{ route('userBlock_admin', [$users->id]) }}" class="btn btn-warning">Chặn</a>
                            @else
                                <a href="{{ route('userBlock_admin', [$users->id]) }}" class="btn btn-warning">Bỏ Chặn</a>
                            @endif
                        </td>
                        <!-- <td>
                                @include('flash::message')
                                <script>
                                $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                                </script>
                        </td>
                    </tr> -->
                </table>
                
                
                
            </div>
            <br>
            <div style="margin-left: 10px">
                <h3>Số lượng sản phẩm: {{ $count }} sản phẩm.</h3> 
                <br>

            <div style="margin-left: 10px"> 
                <form class="form-inline typeahead">
                    <div class="form-group">
                        <input type="name" class="form-control search-input" id="name" autocomplete="off" placeholder="Nhập tên sản phẩm">
                    </div>
                    <button type="submit" class="btn btn-default">Tìm kiếm</button>
                </form>

                </div>
            <br>

            <div>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                                        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                                        <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
                                        <script>
                                            jQuery(document).ready(function($) {
                                                var engine = new Bloodhound({
                                                    remote: {
                                                        url: 'api/customer?q=%QUERY%',
                                                        wildcard: '%QUERY%'
                                                    },
                                                    datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                                                    queryTokenizer: Bloodhound.tokenizers.whitespace
                                                });

                                                $(".search-input").typeahead({
                                                    hint: true,
                                                    highlight: true,
                                                    minLength: 1
                                                }, {
                                                    source: engine.ttAdapter(),
                                                    name: 'usersList',
                                                    templates: {
                                                        empty: [
                                                            '<div class="list-group search-results-dropdown"><div class="list-group-item">Không có kết quả phù hợp.</div></div>'
                                                        ],
                                                        header: [
                                                            '<div class="list-group search-results-dropdown">'
                                                        ],
                                                        suggestion: function (data) {
                                                            return '<a href="customer/' + data.id + '" class="list-group-item">' + data.name + '</a>'
                                                        }
                                                    }
                                                });
                                            });
                                        </script>
            </div>
                <table id="example2" class="table table-bordered table-hover">
            <?php $i = ($products->currentpage()-1)* $products->perpage() + 1;?>

                <thead>
                <tr>
                    <th class="text-center">STT</th>
                    <th class="text-center">Tên</th>
                    <th class="text-center">Giá</th>
                    <!-- <th class="text-center">review</th> -->
                    <th class="text-center">Ảnh</th>
                    <th class="text-center">S.Lượng</th>
                    <th class="text-center">View</th>
                    <th class="text-center">Chất Lượng</th>
                    <th class="text-center">Hàng Động</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($products as $product)
                <tr>
                    <?php $i = 1;   ?>
                    <td>{{ $i++ }}</td>
                    <td>{{ $product->name}}</td>
                    <td>{{ number_format($product->price , 0, ',', '.') }}Đ</td>
                    <!-- <td width="300px" height="-100px">{!! $product->review !!}</td> -->
                    <td align="center"><img src="{{ asset($product->img1) }}"  width="150px" height="150px"></td>
                    <td align="center">{{ $product->quantity }}</td>
                    <td align="center">{{ $product->view }}</td>
                    <td align="center">
                        @if ( $product->rating <= 1 )
                            Tồi
                        @elseif ( $product->rating > 1 && $product->rating <= 2 )
                            Kém
                        @elseif ( $product->rating > 2 && $product->rating <= 3 )
                            Trung Bình
                        @elseif ( $product->rating > 3 && $product->rating <= 4 )
                            Khá
                        @else 
                            Tốt
                        @endif
                    </td>
                    <td align="center">
                                    @if ( $product->status_id == 1 ) 
                                        Hoạt Động
                                    @else
                                        Ẩn Hoạt Động
                                    @endif
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                <div class="center">{!! $products->links() !!}</div>
    </div>
    </section>
@endsection