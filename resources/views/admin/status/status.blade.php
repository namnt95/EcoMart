@extends('layouts.admin')
@section('content')

    <!-- Main content -->
    <section class="content">
    <?php $i = ($status->currentpage()-1)* $status->perpage() + 1;?>
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục sản phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">STT</th>
                        <th class="text-center">Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($statuss as $status)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $status->name}}</td>
                            <td>
                             {!! Form::open(array('route'=> ['status.destroy', $status->id], 'method' => 'DELETE')) !!}
                                {{ link_to_route('status.edit', 'Edit', [$status->id], ['class' => 'btn btn-primary btn-xs btn-block']) }}
                            
                                {!! Form::button('Delete', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                            {!! Form::close() !!}
                        </td>
                        </tr>
                    @endforeach
                    </tbody>
            </table>
    <div class="text-center">{!! $status->links() !!}</div>
    <div class="clearfix visible-sm-block"></div>
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@endsection

