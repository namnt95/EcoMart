@extends('layouts.admin')
@section('content')

    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Danh mục sản phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
            <form action="{{ route('status.store') }}" method="POST" >
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('status')) style="color:red" @endif >Tên</label></td>
                    <td>
                        <input @if ($errors->first('status')) style="border-color:red" @endif type="text" name="status" class="form-control" placeholder="Nhập thể loại..."  }}">
                        @if ($errors->first('status'))
                            <span style="color: red">{{ $errors->first('status') }}</span>
                        @endif
                    </td>
                </tr>
               
                
                <tr>
                    <div class="form-group">
                        <td align="right" ><a class="btn btn-link" href="{{ route('categories.index') }}">Back</a></td>
                        <td align="left"><button type="submit" name="register" class="btn btn-success">Create</button></td>
                    </div>
                </tr>
            </form>

            </table>
                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->
@endsection

