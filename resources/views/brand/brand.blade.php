@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
<div class="home">
        <?php $i = ($products->currentpage()-1)* $products->perpage() + 1;?>

    
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/shop_background.jpg"></div>
        <div class="home_overlay"></div>
        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Smartphones </h2>
        </div>
    </div>

    <!-- Shop -->

    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">

                    <!-- Shop Sidebar -->
                    <div class="shop_sidebar">


                    @if( empty($categories_id) )
                        @if ( empty($price1) )
                            <div class="sidebar_section">
                            <div class="sidebar_title">Danh Mục</div>
                            <ul class="sidebar_categories">
                                @foreach ( $categories as $categories )
                                    <li>
                                        <a href="{!! route('brand_categories', [$brand_id, $categories->id ]) !!}">{{ $categories->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            </div>
                        @else
                            <div class="sidebar_section">
                            <div class="sidebar_title">Danh Mục</div>
                            <ul class="sidebar_categories">
                                @foreach ( $categories as $categories )
                                    <li>
                                        <a href="{!! route('brand_price_categories', [$brand_id, $price1, $price2, $categories->id]) !!}">{{ $categories->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                            </div>
                        @endif

                        @if ( empty($price1) )
                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_subtitle">Giá</div>
                            <ul class="brands_list">
                                @foreach ($prices as $price)
                                    <li class="brand"><a href="{!! route('brand_price', [$brand_id, $price->price1, $price->price2 ]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                @endforeach
                            </ul>
                            </div>
                        @else
                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_subtitle">Giá</div>
                            <ul class="brands_list">
                                @foreach ($prices as $price)
                                    @if ( $price->price1 == $price1 )
                                        <li class="brand"><a href="{!! route('brand_price', [$brand_id, $price->price1, $price->price2 ]) !!}" style="color:blue" ><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif
                                    @if ( $price->price1 != $price1 )
                                        <li class="brand"><a href="{!! route('brand_price', [$brand_id, $price->price1, $price->price2 ]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif
                                @endforeach
                            </ul>
                            </div>
                        @endif


                    @else
                        @if (empty($price1))
                            <div class="sidebar_section">
                            <div class="sidebar_title">Danh Mục</div>
                            <ul class="sidebar_categories">
                                    @foreach ( $categories as $categories )
                                        @if ( $categories_id == $categories->id )
                                            <li>
                                                <a href="{!! route('brand_categories', [$brand_id, $categories->id ]) !!}" style="color: blue">{{ $categories->name }}</a>
                                            </li>
                                        @endif
                                        @if ( $categories_id != $categories->id )
                                            <li>
                                                <a href="{!! route('brand_categories', [$brand_id, $categories->id ]) !!}" >{{ $categories->name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                            </ul>
                            </div>

                        @else
                             <div class="sidebar_section">
                            <div class="sidebar_title">Danh Mục</div>
                            <ul class="sidebar_categories">
                                    @foreach ( $categories as $categories )
                                        @if ( $categories_id == $categories->id )
                                            <li>
                                                <a href="{!! route('brand_price_categories', [$brand_id, $price1, $price2, $categories->id ]) !!}" style="color: blue">{{ $categories->name }}</a>
                                            </li>
                                        @endif
                                        @if ( $categories_id != $categories->id )
                                            <li>
                                                <a href="{!! route('brand_price_categories', [$brand_id, $price1, $price2, $categories->id ]) !!}" >{{ $categories->name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                            </ul>
                            </div>
                        @endif

                        @if ( empty($price1) )
                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_subtitle">Giá</div>
                            <ul class="brands_list">
                                @foreach ($prices as $price)
                                    <li class="brand"><a href="{!! route('brand_price_categories', [$brand_id, $price->price1, $price->price2, $categories->id ]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                @endforeach
                            </ul>
                            </div>
                        @else
                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_subtitle">Giá</div>
                            <ul class="brands_list">
                                @foreach ($prices as $price)
                                    @if ( $price->price1 == $price1 )
                                        <li class="brand"><a href="{!! route('brand_price_categories', [$brand_id, $price->price1, $price->price2, $categories->id ]) !!}" style="color:blue" ><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif
                                    @if ( $price->price1 != $price1 )
                                        <li class="brand"><a href="{!! route('brand_price_categories', [$brand_id, $price->price1, $price->price2, $categories->id ]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif
                                @endforeach
                            </ul>
                            </div>
                        @endif
                    @endif

                        
                        <div class="sidebar_section">
                            <div class="sidebar_subtitle brands_subtitle">Hãng</div>
                            <ul class="brands_list">
                                
                            @foreach ( $brands as $brand )
                                @if ( $brand_id == $brand->id )
                                    <li>
                                        <a href="{!! route('brand.show', $brand->id ) !!}" style="color: blue">{{ $brand->name }}</a>
                                    </li>
                                @endif
                                @if ( $brand_id != $brand->id )
                                    <li>
                                        <a href="{!! route('brand.show', $brand->id ) !!}" style="color: rgba(0,0,0,0.5)">{{ $brand->name }}</a>
                                    </li>
                                @endif

                            @endforeach
                            

                                
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-lg-9">
                    
                    <!-- Shop Content -->

                    <div class="shop_content">
                        <div class="shop_bar clearfix">
                            <div class="shop_product_count">
                                <span> 
                                    {{ $count }}
                                </span> Sản phẩm tìm thấy </div>
                            <div class="shop_sorting">
                                <span>Sắp Xếp:</span>
                                <ul>
                                    <li>
                                        <span class="sorting_text">Giá từ cao đến thấp<i class="fas fa-chevron-down"></span></i>
                                        <ul>
                                            @if( empty($categories_id)  )
                                                @if ( empty($price1) )
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('brand_ratehigh', $brand_id) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('brand_ratelow', $brand_id) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('brand_ratename', $brand_id) !!}">Tên</a>
                                                @else
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('brand_price_ratehigh', [$brand_id, $price1, $price2] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('brand_price_ratelow', [$brand_id, $price1, $price2]) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('brand_price_ratename', [$brand_id, $price1, $price2]) !!}">Tên</a>
                                                @endif

                                            @else
                                                @if ( empty($price1) )
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('brand_categories_ratehigh', [$brand_id, $categories_id] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('brand_categories_ratelow', [$brand_id, $categories_id] ) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('brand_categories_ratename', [$brand_id, $categories_id] ) !!}">Tên</a>
                                                @else
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('brand_price_categories_ratehigh', [$brand_id, $price1, $price2, $categories_id] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('brand_price_categories_ratelow', [$brand_id, $price1, $price2, $categories_id] ) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('brand_price_categories_ratename', [$brand_id, $price1, $price2, $categories_id] ) !!}">Tên</a>
                                                @endif
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product_grid">
                            <div class="product_grid_border"></div>

                            @foreach ($products as $product)

                            <!-- Product Item -->
                            <a href="{{ route('product.show',$product->id) }}">
                            <div class="product_item is_new">
                                <div class="product_border"></div>
                                <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset($product->img1)}}" alt="" width="150" height="150"></div>
                                <div class="product_content">
                                    <div class="product_price"><?php echo number_format($product->price , 0, ',', '.'); ?> 
                                    <img src="{{ asset('images\vnd.png') }}" width='19px' style="margin-left: -5px">
                                    </div>
                                    <div class="product_name"><div><a href="#" tabindex="0">{{ $product->name }}</a></div></div>
                                    <div class="product_name"><div><a href="{{ route('product_shop', [$product->shopper] ) }}" tabindex="0">Cửa hàng {{ $product->shop }}</a></div></div>
                                </div>
                                <div class="product_fav"><i class="fas fa-heart"></i></div>
                                <ul class="product_marks">
                                    <li class="product_mark product_discount">-25%</li>
                                    <li class="product_mark product_new">new</li>
                                </ul>
                                <div><a href="{!! route('index', [$product->id]) !!}">Thêm Giỏ Hàng</a></div>
                            </div>
                            </a>
                            @endforeach

                        </div>

                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            <div class="center">{!! $products->links() !!}</div>
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    @endsection