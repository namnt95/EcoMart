@extends('inc-index/layout')
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 10px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

tr:hover {background-color:#f5f5f5;}
</style>
@section('content')
    <!-- Characteristics -->
    
    <div class="single_product">
        <div class="container">
            <div class="row">
                @include('flash::message')
            <div class="col-sm-12">
            
            <!-- /.box-header -->
            <div class="box-body">
                    <div class="cart_container">
                        @if ( Cart::count() == 0 )
                        <div class="notification" style="text-align: center; height: 250px;">
                            <br/> <br/>
                            <h4>Hiện bạn chưa có sản phẩm trong giỏ hàng, vui lòng trở lại trang chủ để tiếp tục mua sắm.</h4>
                                <a href="{{ route('home.index')}}" style="font-size: 25px; color: red;">Trang chủ</a>
                            <br/>
                            <h4>Xin chân thành cảm ơn</h4>
                            <i class="fa fa-thumbs-o-up" style="font-size: 75px;"></i>

                        </div>
                        @else
                        <div class="cart_title">Giỏ Hàng</div>
                        <br>
                        <div class="card">
                        <div class="card-body p-0">
                        <table class="table table-striped">
                          <tbody><tr>
                            <th style="width: 10px">Ảnh</th>
                            <th>Sản Phẩm</th>
                            <th>Số Lượng</th>
                            <th>Giá</th>
                            <th>Tổng </th>
                            <th>Cập Nhật</th>
                            <th style="width: 40px"></th>
                          </tr>
                          <?php  $coupon_code = 0; ?>
                          @foreach($content as $item)
                          <form method="POST" action="{!! route('update_cart',['id'=>$item->rowId]) !!}">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                          <tr>
                            <td><img src=" {{ $item->options->img  }} " alt="" width="150" height="150"></td>
                            <td style="text-align: justify;padding: 70px"><a href="{{ route('product.show',$item->id) }}">{!! $item->name !!}</a></td>
                            <td style="text-align: justify"><input type="number" min="1" name="qty" value="{!! $item->qty !!}" style="margin-top: 59px;width: 3em;"></td>
                            <td style="text-align: justify;"><div style="margin-top: 59px">{!! number_format($item->price, 0, ',', '.') !!} Đ</div></td>
                            <td style="text-align: justify;"><div style="margin-top: 59px">{!! number_format($item->price*$item->qty, 0, ',', '.') !!} Đ
                                                    <?php  $coupon_code = $item->price*$item->qty + $coupon_code;?></div></td>
                            <td style="text-align: justify;">
                                <div style="margin-top: 50px"><button type="submit" class="btn btn-success">Cập nhập</button></div>
                            </td>
                            <td style="text-align: justify;"><div style="margin-top: 50px"><a href="{{ route('delete_cart', ['id' => $item->rowId]) }}" ><img class="tooltip-test" data-original-title="delete"  src="{{ asset('images/remove-icon.png') }}" alt="sad" width="55px" ></a></div></td>
                          </tr>
                          </form>
                          @endforeach
                        </tbody></table>
                        </div>
                        </div>
                        
                        <br>
                        <div style="text-align: right;">
                        <a href="{{ route('deleteall_cart') }}" class="btn btn-primary">Xoá Tất Cả</a>
                        <a href="{{ route('checkout', $item->rowId) }}" class="btn btn-primary"> Đặt Hàng</a>
                        </div>
                        <br>
                        <div align="right">
                           <div class="col-md-4" align="right">
                            <table >
                                <thead>
                                <tr>
                                    <td>Thông tin đơn hàng</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Phí giao hàng</td>
                                    <td>15.000Đ</td>
                                </tr>
                                @if ( Auth::user() ) 
                                           <tr>
                                    {!!  Form::open(array('route' => ['code_input', Auth::id()], 'method' => 'post' )) !!}
                                    <td>
                                        {!! Form::text('code', null, ['class' => 'form-control', 'size'=> "width='20px'", 'placeholder' => 'Nhập mã giảm giá' ])  !!}
                                    </td>
                                    <td>
                                        {!! Form::button('Áp dụng', ['type' => 'submit', 'class' => 'btn btn-success'])  !!}
                                    </td>
                                    {!! Form::close() !!}
                                </tr>
                                @endif
                                    <td >
                                        Giảm giá:
                                    </td>
                                    <td>
                                        @if ( session()->has('key') ) 
                                           10%
                                        @else 
                                            0%
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        Tổng Tiền:
                                    </td>
                                    <td>
                                        @if ( session()->has('key') ) 
                                            <?php  echo number_format($coupon_code*0.9, 0, ',', '.'); ?>Đ
                                        @else
                                            {!! $total !!}Đ
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="width: 400px" align="center">
                    @include('flash::message')        
                </div>
                
                <script>
                $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                </script>
                {!!  Form::open(array('route' => ['code_input', Auth::id()], 'method' => 'post' )) !!}
                        </div>
                        <br>
                <div class="col-md-10">
                Bạn có {{ $count }} mã giảm giá
                    @foreach ( $codes as $code )
                        : {{ $code->code }}
                    @endforeach
                </div>
                @endif
               <br>     
            <div class="clearfix visible-sm-block"></div>
        </div>
            <!-- /.row -->
    </section>
            <!-- /.content -->
    </div>
    </div>
</div>
</div>

    @endsection
    