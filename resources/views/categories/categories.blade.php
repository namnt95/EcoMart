@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')

<div class="home">
        <?php $i = ($products->currentpage()-1)* $products->perpage() + 1;?>

        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/shop_background.jpg"></div>
        <div class="home_overlay"></div>
        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Smartphones</h2>
        </div>
    </div>

    <!-- Shop -->

    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">

                    <!-- Shop Sidebar -->
                    <div class="shop_sidebar">
                        <div class="sidebar_section">
                            <div class="sidebar_title">Danh Mục</div>
                            <ul class="sidebar_categories">

                        <!-- Màu Categories   --> 
                                @foreach ( $categories as $categories )
                                    @if ( $categories_id == $categories->id )
                                        <li>
                                            <a href="{!! route('categories.show', $categories->id ) !!}" style="color: blue">{{ $categories->name }}</a>
                                        </li>
                                    @endif
                                    @if ( $categories_id != $categories->id )
                                        <li>
                                            <a href="{!! route('categories.show', $categories->id ) !!}" >{{ $categories->name }}</a>
                                        </li>
                                    @endif

                                @endforeach

                            </ul>
                        </div>

        

                        <!-- price rỗng -->

                        @if ( empty($price1) )

                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_title">Lọc</div>
                            <div class="sidebar_subtitle">Giá</div>
                            <ul class="brands_list">
                                <!-- Brand_id, Price rỗng -->
                                @if ( empty($brand_id) )
                                    @foreach ( $prices as $price )
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id, $price->price1, $price->price2]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endforeach
                                <!-- Brand_id có, Price rỗng -->
                                @else
                                    @foreach ( $prices as $price )
                                        <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id, $price->price1, $price->price2, $brand_id ]) !!}"><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>
                        <div class="sidebar_section">
                            <div class="sidebar_subtitle brands_subtitle">Hãng</div>
                            <ul class="brands_list">
                               <!--  Price, Brand_Id rỗng -->
                                @if ( empty($brand_id) )
                                    @foreach ($brands as $brands)
                                        <li class="brand"><a href="{!! route('categories_brand', [$categories_id, $brands->id ]) !!}"> {{ $brands->name }} </a></li>
                                    @endforeach
                                <!-- Price rỗng, Brand_id có -->
                                @else
                                    @foreach ($brands as $brands)
                                        @if ( $brand_id == $brands->id )
                                            <li class="brand"><a href="{!! route('categories_brand', [$categories_id, $brands->id ]) !!}" style="color: blue"> {{ $brands->name }} </a></li>
                                        @endif
                                        @if ( $brand_id != $brands->id )
                                            <li class="brand"><a href="{!! route('categories_brand', [$categories_id, $brands->id ]) !!}"> {{ $brands->name }} </a></li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                            </div>

                        <!-- có price -->
                        @else

                            <div class="sidebar_section filter_by_section">
                            <div class="sidebar_title">Lọc</div>
                            <div class="sidebar_subtitle">Giá</div>
                                    <ul class="brands_list">
                                <!-- Price có, Brand_id rỗng -->
                                @if ( empty($brand_id) )
                                    @if ( $price1 == 0)
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id, 1, 250000]) !!}" style="color: blue">1 - 250.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,1, 250000]) !!}" >1 - 250.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '250000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,250000, 500000]) !!}" style="color: blue">250.000 - 500.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,250000, 500000]) !!}">250.000 - 500.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '500000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,500000, 1000000]) !!}" style="color: blue">500.000 - 1.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li> 
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,500000, 1000000]) !!}" >500.000 - 1.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '1000000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,1000000, 2000000]) !!}"  style="color: blue">1.000.000 - 2.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,1000000, 2000000]) !!}">1.000.000 - 2.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '2000000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,2000000, 5000000]) !!}" style="color: blue">2.000.000 - 5.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,2000000, 5000000]) !!}">2.000.000 - 5.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '5000000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,5000000, 10000000]) !!}"  style="color: blue">5.000.000 - 10.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,5000000, 10000000]) !!}">5.000.000 - 10.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '10000000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,10000000, 15000000]) !!}"  style="color: blue">10.000.000 - 15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @else
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id,10000000, 15000000]) !!}">10.000.000 - 15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                    @endif

                                    @if ( $price1 == '15000000')
                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id, 15000000,1000000000]) !!}"  style="color: blue">15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > Trở Lên</a></li>
                                    @else

                                        <li class="brand"><a href="{!! route('categories_price',[$categories_id, 15000000,1000000000]) !!}">15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > Trở Lên</a></li>
                                    @endif

                                @else
                                <!-- Price có, Brand_id có -->
                                    @if ( $price1 )
                                        @if ( $price1 == '0')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id, 1, 250000, $brand_id]) !!}" style="color: blue">1 - 250.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,1, 250000, $brand_id]) !!}">1 - 250.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '250000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,250000, 500000, $brand_id]) !!}" style="color: blue">250.000 - 500.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,250000, 500000, $brand_id]) !!}">250.000 - 500.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '500000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,500000, 1000000, $brand_id]) !!}" style="color: blue">500.000 - 1.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,500000, 1000000, $brand_id]) !!}">500.000 - 1.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '1000000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,1000000, 2000000, $brand_id]) !!}" style="color: blue">1.000.000 - 2.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,1000000, 2000000, $brand_id]) !!}">1.000.000 - 2.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '2000000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,2000000, 5000000, $brand_id]) !!}"  style="color: blue">2.000.000 - 5.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,2000000, 5000000, $brand_id]) !!}">2.000.000 - 5.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '5000000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,5000000, 10000000, $brand_id]) !!}" style="color: blue">5.000.000 - 10.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,5000000, 10000000, $brand_id]) !!}">5.000.000 - 10.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '10000000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,10000000, 15000000, $brand_id]) !!}" style="color: blue">10.000.000 - 15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id,10000000, 15000000, $brand_id]) !!}">10.000.000 - 15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > </a></li>
                                        @endif

                                        @if ( $price1 == '15000000')
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id, 15000000,1000000000, $brand_id]) !!}">15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > Trở Lên</a></li>
                                        @else
                                            <li class="brand"><a href="{!! route('categories_price_brand',[$categories_id, 15000000,1000000000, $brand_id]) !!}">15.000.000<img src="{{ asset('images\vnd.png') }}" width='15px' > Trở Lên</a></li>
                                        @endif
                                    @endif

                                @endif
                            </ul>
                        </div>
                        <div class="sidebar_section">
                            <div class="sidebar_subtitle brands_subtitle">Hãng</div>
                            <ul class="brands_list">
                                <!-- Price có, Brand_id rỗng -->
                            @if ( empty($brand_id) )
                                @foreach ($brands as $brands)
                                    <li class="brand"><a href="{!! route('categories_price_brand', [$categories_id, $price1, $price2,  $brands->id ]) !!}"> {{ $brands->name }} </a></li>
                                @endforeach
                            @else
                            <!-- Price có, Brand_id có -->
                                @foreach ($brands as $brands)
                                    @if ( $brand_id == $brands->id  )
                                        <li class="brand"><a href="{!! route('categories_price_brand', [$categories_id, $price1, $price2,  $brands->id ]) !!}" style="color:blue"> {{ $brands->name }} </a></li>
                                    @endif
                                    @if ( $brand_id != $brands->id  )
                                        <li class="brand"><a href="{!! route('categories_price_brand', [$categories_id, $price1, $price2,  $brands->id ]) !!}" > {{ $brands->name }} </a></li>
                                    @endif
                                @endforeach
                            @endif
                            </ul>
                        </div>

                        @endif

                    </div>

                </div>

                <div class="col-lg-9">
                    
                    <!-- Shop Content -->

                    <div class="shop_content">
                        <div class="shop_bar clearfix">
                            <div class="shop_product_count">
                                <span> 
                                    {{ $count }}
                                </span> Sản phẩm tìm thấy </div>
                            <div class="shop_sorting">
                                <span>Sắp Xếp:</span>
                                <ul>
                                    <li>
                                        <span class="sorting_text">Giá từ cao đến thấp<i class="fas fa-chevron-down"></span></i>
                                        <ul>
                                            @if( empty($price1)  )
                                                @if ( empty($brand_id) )
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('categories_ratehigh', $categories_id) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('categories_ratelow', $categories_id) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('categories_ratename', $categories_id) !!}">Tên</a>
                                                @else
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('categories_brand_ratehigh', [$categories_id, $brand_id] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('categories_brand_ratelow', [$categories_id, $brand_id] ) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('categories_brand_ratename', [$categories_id, $brand_id] ) !!}">Tên</a>
                                                @endif

                                            @else
                                                @if ( empty($brand_id) )
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('categories_price_ratehigh', [$categories_id, $price1, $price2] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('categories_price_ratelow', [$categories_id, $price1, $price2]) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('categories_price_ratename', [$categories_id, $price1, $price2]) !!}">Tên</a>
                                                @else
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'><a href="{!! route('categories_price_brand_ratehigh', [$categories_id, $price1, $price2, $brand_id] ) !!}">Giá từ cao đến thấp</a></li>
                                                    <li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'><a href="{!! route('categories_price_brand_ratelow', [$categories_id, $price1, $price2, $brand_id] ) !!}">Giá từ thấp đến cao</a></li>
                                                    <li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'><a href="{!! route('categories_price_brand_ratename', [$categories_id, $price1, $price2, $brand_id] ) !!}">Tên</a>
                                                @endif
                                            @endif
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="product_grid">
                            <div class="product_grid_border"></div>

                            @foreach ($products as $product)

                            <!-- Product Item -->
                            <a href="{{ route('product.show',$product->id) }}">
                            <div class="product_item is_new">
                                <div class="product_border"></div>
                                <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset($product->img1)}}" alt=""></div>
                                <div class="product_content">
                                    <div class="product_price"><?php echo number_format($product->price , 0, ',', '.'); ?> 
                                    <img src="{{ asset('images\vnd.png') }}" width='19px' style="margin-left: -5px">
                                    </div>
                                    <div class="product_name"><div><a href="#" tabindex="0">{{ $product->name }}</a></div></div>
                                </div>
                                <div class="product_fav"><i class="fas fa-heart"></i></div>
                                <ul class="product_marks">
                                    <li class="product_mark product_discount">-25%</li>
                                    <li class="product_mark product_new">new</li>
                                </ul>
                            </div>
                            </a>
                            @endforeach

                        </div>

                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            <div class="center">{!! $products->links() !!}</div>
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    @endsection