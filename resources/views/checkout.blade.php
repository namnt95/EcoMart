@extends('inc-index/layout')
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

@section('content')

<style type="text/css">
    .borderless td, .borderless th {
    border: none;
}
</style>
    <div>
        <div style="background-color: #EFF6FA">
        <br>
            
        <div class="container" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="row">

                <div class="table-responsive">
                    <table class="table" style="border: 1px solid white;background-color: white">
                        <thead>
                        <tr class="table-success">
                            <th class="text-center" width="10px">STT</th>
                            <th class="text-center">Tên</th>
                            <th class="text-center" width="100px">Số lượng</th>
                            <th class="text-center" width="10px">Giá</th>
                            <th class="text-center" width="25px">Tổng</th>
                        </tr>
                        </thead>
                        <?php  $coupon_code = 0; ?>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($content as $item)
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <tr>
                                <td class="text-center"> {!! $i++ !!}</td>
                                <td>
                                    <a href="{{ route('product.show',$item->id) }}">
                                        {!! $item->name !!}
                                    </a>
                                </td>
                                <td class="text-center"> 
                                    {!! $item->qty !!}
                                </td>
                                <td class="text-center"> {!! number_format($item->price, 0, ',', '.') !!} Đ</td>
                                <td class="text-center">
                                    {!! number_format($item->price*$item->qty, 0, ',', '.') !!} Đ
                                    <?php  $coupon_code = $item->price*$item->qty + $coupon_code;?>
                                </td>
                            </tr>
                        @endforeach
                                <tr>
                                    <td colspan="4" class="text-center"> Tổng Tiền</td>
                                    <td align="center">
                                        <b>
                                        @if ( session()->has('key') ) 
                                            <?php echo number_format($coupon_code*0.9, 0, ',', '.'); 
                                                    $number = $coupon_code*0.9;

                                            ?> Đ
                                        @else
                                            {{ Cart::subtotal('0', ',' , '.') }} Đ
                                        @endif
                                        </b>
                                        @if ( session()->has('key') ) 
                                            ( coupon 10% )
                                        @endif
                                    </td>
                                </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="col-sm-6" style="border: 1px solid white;padding: 10px;background-color: white">
                
                @guest
                <form action="{{ route('post_checkout') }}" method="post">
                    {{ csrf_field() }}
                    <table class="table borderless" width="100%">
                        <tr>
                            <td>
                                {!! Form::label('name','Tên Khách Hàng*')  !!}
                            </td>
                            <td>
                                <input type="text" name="fullName" size="50px" placeholder=" Họ và Tên " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" >
                            </td>
                        </tr>   
                        <tr>
                            <td>
                                {!! Form::label('name','Địa Chỉ Email*')  !!}
                            </td>
                            <td>
                                <input type="gmail" name="email"  size="50px" placeholder=" Email " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {!! Form::label('name','Địa Chỉ* ')  !!}
                            </td>
                            <td>
                                <input type="text" name="address"  size="50px" placeholder=" Địa Chỉ " style="border-left:none;border-right:none; border-top :none; line-height: 2em;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                {!! Form::label('name','Số Điện Thoại* ')  !!}
                            </td>
                            <td>
                                <input type="text" name="phoneNumber"  size="50px" placeholder=" Số điện thoại " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" >
                            </td>
                        </tr>   
                        <tr>
                            <td>
                                {!! Form::label('name','Ngày Nhận Hàng*')  !!}
                            </td>
                            <td>
                                <input type="date" name="senddate"  size="50px" placeholder=" Số điện thoại "  >
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><h4 style="color: red">Yêu cầu nhập đầy đủ các thông tin </p></h4>
                            
                        </tr>
                    </table>

                </div>
                <div class="col-sm-6"> 
                    <textarea class="form-control" rows="7" id="comment" name="note" placeholder="Ghi chú*"></textarea>
                    <!--  <p class="lead">Thanh toán bằng:</p>
                  <img src="{{ asset('dist/img/credit/visa.png') }}" alt="Visa">
                  <img src="{{ asset('dist/img/credit/mastercard.png') }}" alt="Mastercard">
                  <img src="{{ asset('dist/img/credit/american-express.png') }}" alt="American Express">
                  <img src="{{ asset('dist/img/credit/paypal2.png') }}" alt="Paypal">

                  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                    plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                  </p> -->
                    <br><br>
                    <div align="center">
                        <table>
                            <tr>
                                <td style="padding:10px">
                                    <a href="{{ route('cart') }}" >Quay Lại</a>
                                </td>
                                <td style="padding:10px"> 
                                    <a href="{{ route('deleteall_cart') }}" class="btn btn-primary">Hủy đơn hàng</a>
                                </td>
                                <td style="padding:10px">
                                    <button type="submit" class="btn btn-success check_out"  >Gửi đơn hàng</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                </form>
                @else
                    @if ( session()->has('key') ) 
                        <form action="{{ route('post_checkout_code_id', [Auth::user()->id, $number]) }}" method="post">
                        {{ csrf_field() }}
                        <table class="table borderless" width="100%">
                            <tr>
                                <td>
                                    {!! Form::label('name','Tên Khách Hàng*')  !!}
                                </td>
                                <td>
                                    <input type="text" name="fullName" size="50px" placeholder=" Họ và Tên " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->name !!}">
                                </td>
                            </tr>   
                            <tr>
                                <td>
                                    {!! Form::label('name','Địa Chỉ Email*')  !!}
                                </td>
                                <td>
                                    <input type="gmail" name="email"  size="50px" placeholder=" Email " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->email !!}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('name','Địa Chỉ* ')  !!}
                                </td>
                                <td>
                                    <input type="text" name="address"  size="50px" placeholder=" Địa Chỉ " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->address !!}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('name','Số Điện Thoại* ')  !!}
                                </td>
                                <td>
                                    <input type="text" name="phoneNumber"  size="50px" placeholder=" Số điện thoại " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->phone !!}">
                                </td>
                            </tr>   
                            <tr>
                                <td>
                                    {!! Form::label('name','Ngày Nhận Hàng*')  !!}
                                </td>
                                <td>
                                    <input type="date" name="senddate"  size="50px" placeholder=" Số điện thoại "  >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><h4 style="color: red">Yêu cầu nhập đầy đủ các thông tin </p></h4>
                                
                            </tr>
                        </table>

                    </div>
                    <div class="col-sm-6"> 
                        <textarea class="form-control" rows="7" id="comment" name="note" placeholder="Ghi chú*"></textarea>
                        <br><br>
                        <div align="center">
                            <table>
                            <tr>
                                <td style="padding:10px">
                                    <a href="{{ route('cart') }}" >Quay Lại</a>
                                </td>
                                <td style="padding:10px"> 
                                    <a href="{{ route('deleteall_cart') }}" class="btn btn-primary">Hủy đơn hàng</a>
                                </td>
                                <td style="padding:10px">
                                    <button type="submit" class="btn btn-success check_out"  >Gửi đơn hàng</button>
                                </td>
                            </tr>
                            </table>
                        </div>
                    </div>
                    </form>         

                    @else
                        <form action="{{ route('post_checkout_id', Auth::user()->id) }}" method="post">
                        {{ csrf_field() }}
                        <table class="table borderless" width="100%">
                            <tr>
                                <td>
                                    {!! Form::label('name','Tên Khách Hàng*')  !!}
                                </td>
                                <td>
                                    <input type="text" name="fullName" size="50px" placeholder=" Họ và Tên " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->name !!}">
                                </td>
                            </tr>   
                            <tr>
                                <td>
                                    {!! Form::label('name','Địa Chỉ Email*')  !!}
                                </td>
                                <td>
                                    <input type="gmail" name="email"  size="50px" placeholder=" Email " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->email !!}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('name','Địa Chỉ* ')  !!}
                                </td>
                                <td>
                                    <input type="text" name="address"  size="50px" placeholder=" Địa Chỉ " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->address !!}">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {!! Form::label('name','Số Điện Thoại* ')  !!}
                                </td>
                                <td>
                                    <input type="text" name="phoneNumber"  size="50px" placeholder=" Số điện thoại " style="border-left:none;border-right:none; border-top :none; line-height: 2em;" value="{!! Auth::user()->phone !!}">
                                </td>
                            </tr>   
                            <tr>
                                <td>
                                    {!! Form::label('name','Ngày Nhận Hàng*')  !!}
                                </td>
                                <td>
                                    <input type="date" name="senddate"  size="50px" placeholder=" Số điện thoại "  >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><h4 style="color: red">Yêu cầu nhập đầy đủ các thông tin </p></h4>
                                
                            </tr>
                        </table>

                    </div>
                    <div class="col-sm-6"> 
                        <textarea class="form-control" rows="7" id="comment" name="note" placeholder="Ghi chú*"></textarea>

                 
                        <br><br>
                        <div align="center">
                            <table>
                            <tr>
                                <td style="padding:10px">
                                    <a href="{{ route('cart') }}" >Quay Lại</a>
                                </td>
                                <td style="padding:10px"> 
                                    <a href="{{ route('deleteall_cart') }}" class="btn btn-primary">Hủy đơn hàng</a>
                                </td>
                                <td style="padding:10px">
                                    <button type="submit" class="btn btn-success check_out"  >Gửi đơn hàng</button>
                                </td>
                            </tr>
                            </table>
                        </div>
                    </div>
                    </form>     

                    @endif
                @endguest
        </div>
    </div>
    <br> <br> <br> <br> <br> <br>
@endsection