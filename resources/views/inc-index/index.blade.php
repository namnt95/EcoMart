@extends('inc-index/layout')
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/slick-1.8.0/slick.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>

@section('content')
<div class="home">
    
	
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="images/shop_background.jpg"></div>
		<div class="home_overlay"></div>
		<div class="home_content d-flex flex-column align-items-center justify-content-center">
			<h2 class="home_title">Smartphones 	</h2>
		</div>
	</div>

	<!-- Shop -->

	<div class="shop">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">

					<!-- Shop Sidebar -->
					<div class="shop_sidebar">
						<div class="sidebar_section">
							<div class="sidebar_title">Danh Mục</div>
							<ul class="sidebar_categories">
								@foreach ( $categories as $categories )
									<li>
										<a href="{!! route('categories.show', $categories->id  ) !!}">{{ $categories->name }}</a>
									</li>
								@endforeach
							</ul>
						</div>
						
						<div class="sidebar_section filter_by_section">
                            <div class="sidebar_subtitle">Giá</div>
							<ul class="sidebar_categories">
								@foreach ( $prices as $price )
                                    <a href="{{ route('show_price', [$price->price1, $price->price2]) }}"><li><?php echo number_format($price->price1 , 0, ',', '.'); ?>  - <?php echo number_format($price->price2 , 0, ',', '.'); ?> <img src="{{ asset('images\vnd.png') }}" width='15px' > </a>
                                    </li>
                                    </a>
                                @endforeach
							</ul>
						</div>
						

						<div class="sidebar_section">
							<div class="sidebar_subtitle brands_subtitle">Hãng</div>
							<ul class="brands_list">
								@foreach ($brands as $brands)
									<li class="brand"><a href="{!! route('brand.show', $brands->id  ) !!}""> {{ $brands->name }} </a></li>
								@endforeach
							</ul>
						</div>
					</div>

				</div>

				<div class="col-lg-9">
					
					<!-- Shop Content -->

					<div class="shop_content">
						<div class="shop_bar clearfix">
							<div class="shop_product_count"><span> 
                                    {{ $count }}
                                </span> Sản phẩm tìm thấy </div>
							<div class="shop_sorting">
								<span>Sắp Xếp:</span>
								<ul>
									<li>
										<span class="sorting_text">Giá từ cao đến thấp<i class="fas fa-chevron-down"></span></i>
										<ul>
											<a href="{{ route('rate_high') }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Giá từ cao đến thấp</li></a>
											<a href="{{ route('rate_low') }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Giá từ thấp đến cao</li></a>
											<a href="{{ route('rate_name') }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Tên</li></a>
										</ul>
									</li>
								</ul>
							</div>
						</div>
        <?php $i = ($products->currentpage()-1)* $products->perpage() + 1;?>

						<div class="product_grid">
							<div class="product_grid_border"></div>

							@foreach ($products as $product)

							<!-- Product Item -->
							<a href="{{ route('product.show',$product->id) }}">
							<div class="product_item is_new">
								<div class="product_border"></div>
								<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ asset($product->img1) }}" alt="" width="150" height="150"></div>
								<div class="product_content">
									<div class="product_price" style="color:#D2691E"><?php echo number_format($product->price , 0, ',', '.'); ?> 
									<img src="{{ asset('images\vnd.png') }}" width='13px' style="margin-left: -5px">
									</div>
									<div class="product_name"><div><a href="#" tabindex="0">{{ $product->name }}</a></div></div>
									<div class="product_name"><div><a href="{{ route('product_shop', [$product->shopper] ) }}" tabindex="0">Cửa hàng {{ $product->shop }}</a></div></div>
								</div>
								<div class="product_fav"><i class="fas fa-heart"></i></div>
								<ul class="product_marks">
									<li class="product_mark product_discount">-25%</li>
									<li class="product_mark product_new">new</li>
								</ul>
								<div><a href="{!! route('index', [$product->id]) !!}">Thêm Giỏ Hàng</a></div>
							</div>
							</a>
							@endforeach

						</div>

						<!-- Shop Page Navigation -->

						<div class="shop_page_nav d-flex flex-row">
							
                			<div class="center">{!! $products->links() !!}</div>
							
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	@endsection