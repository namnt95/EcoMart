<!DOCTYPE html>
<html>
<head>
	<title>Welcome</title>
</head>
<body style="text-align: center;">
	<h1>Xin chào {{ $name }}</h1>
	Đơn hàng của bạn là .
	<table>
        <tr>
            <th>Thông tin khách hàng</th>
            <th ></th>
        </tr>
        <tr>
            <td>Thông tin người đặt hàng</td>
            <td>{{ $customerInfo->name }}</td>
        </tr>
        <tr>
            <td>Ngày đặt hàng</td>
            <td>{{ $customerInfo->created_at }}</td>
        </tr>
        <tr>
            <td>Số điện thoại</td>
            <td>{{ $customerInfo->phone_number }}</td>
        </tr>
        <tr>
            <td>Địa chỉ</td>
            <td>{{ $customerInfo->address }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{ $customerInfo->email }}</td>
        </tr>
        <tr>
            <td>Ghi chú</td>
            <td>{{ $customerInfo->bill_note }}</td>
        </tr>
    </table>
</body>
</html>
