@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('styles/regular_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('styles/regular_responsive.css')}}">
<style type="text/css">
	ul {
	    list-style-type: number;
	}
</style>

@section('content')
   
<!-- Single Blog Post -->

	<div class="single_post">
		<div class="container">
			<div class="row">
				@if( empty($news_details) )
								@if( $information_id == '{"information_id":1}' )
									<H3>TIN TỨC</H3>
								@elseif ( $information_id == '{"information_id":2}' )
									<H3>GIỚI THIỆU</H3>
								@elseif ( $information_id == '{"information_id":3}' )
									<H3>LIÊN HỆ</H3>
								@else
									<H3>THÔNG TIN TUYỂN DỤNG</H3>
								@endif
					<div class="col-lg-12" style="border: 3px ; border-style: ridge;padding: 20px" >
						<ol style="padding-left: 30px">
							@foreach ( $news as $new )
							  <li style="padding-left: 30px"><h4><a href="{{ route('news_details', $new->id) }}">{!!  $new->title !!}</a></h4></li>
							@endforeach
						</ol>
						@else 
						<div class="col-lg-12" style="border: 3px ; border-style: ridge;padding: 20px">
							@foreach ( $news_details as $new )
							<div class="single_post_title">{!!  $new->title !!}</div>
							<div class="single_post_text">
								{!! $new->news !!}
							</div>
							@endforeach
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
<br>
<br>
<br>
<br>
<br>
<br>
	@endsection