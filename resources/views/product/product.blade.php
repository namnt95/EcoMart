@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                   
                    </div>
                </div>
                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px;padding-left: 25px">
                        <div class="box">
                            <div class="box-header" style="margin-top: 10px">
                                <h3>Sản Phẩm</h3>
                                <div>
                                    @include('flash::message')
                                    <script>
                                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                                    </script>
                                </div>
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Tất Cả ({{ $count }}) </a>
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Hết Hàng ({{ $count_quantity }}) </a>
                                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Chất Lượng Kém ({{ $count_rating }}) </a>
                                        <a class="nav-item nav-link" id="nav-status-tab" data-toggle="tab" href="#nav-status" role="tab" aria-controls="nav-status" aria-selected="false">Không Hoạt Động ({{ $count_status_id }}) </a>
                                        <a class="nav-item nav-link" id="nav-status-tab" data-toggle="tab" href="#nav-status" role="tab" aria-controls="nav-status" aria-selected="false">Bán chạy ({{ $count_status_id }}) </a>
                                        </div>
                                    </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                
                                                <br>
                                                <form action=" {{ route('product_search', Auth::user()->id) }}  " class="form-inline typeahead">
                                                    <div class="form-group">
                                                        <input type="search" class="form-control search-input" id="name" name="search" autocomplete="off" placeholder="Nhập tên sản phẩm">
                                                    </div>
                                                    <button type="submit" class="btn btn-default">Tìm kiếm</button>
                                                </form>
                                                <br>
                                                @if ( empty($count_search) )
                                                @else 
                                                    <div style="color: red">
                                                    Tìm thấy {{ $count_search }} sản phẩm.
                                                </div>
                                                @endif
                                                <br>
                                                <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $i = 1;   ?>

                                                @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{ route('product.show',$product->id) }}">
                                                            {{ $product->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product->price , 0, ',', '.') }}Đ</td>
                                                    <!-- <td width="300px" height="-100px">{!! $product->review !!}</td> -->
                                                    <td align="center"><img src="{{ asset($product->img1) }}" width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product->quantity }}</td>
                                                    <td align="center">{{ $product->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products->links() !!}</div>

                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <br>
                                            <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;   ?>
                                                @foreach ($products_quantity as $product_quantity)
                                                <tr>
                                                    
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{ route('product.show',$product_quantity->id) }}">
                                                            {{ $product_quantity->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product_quantity->price , 0, ',', '.') }}Đ</td>
                                                    <!-- <td width="300px" height="-100px">{!! $product->review !!}</td> -->
                                                    <td align="center"><img src="{{ asset($product_quantity->img1) }}  " width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product_quantity->quantity }}</td>
                                                    <td align="center">{{ $product_quantity->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product_quantity->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product_quantity->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product_quantity->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product_quantity->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product_quantity->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product_quantity->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products_quantity->links() !!}</div>

                                        </div>
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                            <br>
                                                <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;   ?>
                                                @foreach ($products_rating as $product_rating)
                                                <tr>
                                                    
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{ route('product.show',$product_rating->id) }}">
                                                            {{ $product_rating->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product_rating->price , 0, ',', '.') }}Đ</td>
                                                    <td align="center"><img src="{{ asset($product_rating->img1) }}  " width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product_rating->quantity }}</td>
                                                    <td align="center">{{ $product_rating->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product_rating->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product_rating->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product_rating->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product_rating->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product_rating->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product_rating->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products_rating->links() !!}</div>
                                            
                                        </div>
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                            <br>
                                                <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;   ?>
                                                @foreach ($products_rating as $product_rating)
                                                <tr>
                                                    
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{ route('product.show',$product_rating->id) }}">
                                                            {{ $product_rating->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product_rating->price , 0, ',', '.') }}Đ</td>
                                                    <td align="center"><img src="{{ asset($product_rating->img1) }}  " width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product_rating->quantity }}</td>
                                                    <td align="center">{{ $product_rating->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product_rating->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product_rating->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product_rating->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product_rating->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product_rating->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product_rating->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products_rating->links() !!}</div>
                                            
                                        </div>
                                        <div class="tab-pane fade" id="nav-status" role="tabpanel" aria-labelledby="nav-status-tab">
                                            <br>
                                                <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;   ?>
                                                @foreach ($products_status_id as $product_status_id)
                                                <tr>
                                                    
                                                    <td>{{ $i++ }}</td>

                                                    <td>
                                                        <a href="{{ route('product.show',$product_status_id->id) }}">
                                                            {{ $product_status_id->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product_status_id->price , 0, ',', '.') }}Đ</td>
                                                    <!-- <td width="300px" height="-100px">{!! $product->review !!}</td> -->
                                                    <td align="center"><img src="{{ asset($product_status_id->img1) }}  " width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product_status_id->quantity }}</td>
                                                    <td align="center">{{ $product_status_id->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product_status_id->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product_status_id->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product_status_id->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product_status_id->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product_status_id->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product_status_id->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products_status_id->links() !!}</div>
                                            
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <br>
                                            <table id="example2" class="table table-bordered table-hover">
                                                <thead>
                                                <tr class="table-active">
                                                    <th class="text-center">STT</th>
                                                    <th class="text-center">Tên</th>
                                                    <th class="text-center">Giá</th>
                                                    <!-- <th class="text-center">review</th> -->
                                                    <th class="text-center">Ảnh</th>
                                                    <th class="text-center">S.Lượng</th>
                                                    <th class="text-center">view</th>
                                                    <th>Hàng Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i = 1;   ?>
                                                @foreach ($products_quantity as $product_quantity)
                                                <tr>
                                                    
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        <a href="{{ route('product.show',$product_quantity->id) }}">
                                                            {{ $product_quantity->name}}
                                                        </a>
                                                    </td>
                                                    <td>{{ number_format($product_quantity->price , 0, ',', '.') }}Đ</td>
                                                    <!-- <td width="300px" height="-100px">{!! $product->review !!}</td> -->
                                                    <td align="center"><img src="{{ asset($product_quantity->img1) }}  " width="150px" height="150px" ></td>
                                                    <td align="center">{{ $product_quantity->quantity }}</td>
                                                    <td align="center">{{ $product_quantity->view }}</td>
                                                    <td>
                                                        <div class="shop_sorting">
                                                        <ul>
                                                            <li>
                                                                <span class="sorting_text"> 
                                                                    @if ( $product_quantity->status_id == 1 ) 
                                                                        Hoạt Động
                                                                    @else
                                                                        Ẩn Hoạt Động
                                                                    @endif
                                                                    <i class="fas fa-chevron-down"></span></i>
                                                                <ul>
                                                                    @if ( $product_quantity->status_id == 1 )
                                                                        <?php $status_id = 2; ?>
                                                                        <a href="{!! route('product_status', [$product_quantity->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Ẩn Hoạt Động</li></a>
                                                                    @else
                                                                        <?php $status_id = 1; ?>
                                                                        <a href="{!! route('product_status', [$product_quantity->id, $status_id] ) !!}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>Hoạt Động</li></a>
                                                                    @endif
                                                                    
                                                                    <a href="{{ route('product.edit', $product_quantity->id) }}"><li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>Sửa chi tiết</li></a>
                                                                    <a href="{{ route('product_delete', $product_quantity->id) }}"><li class="shop_sorting_button"data-isotope-option='{ "sortBy": "price" }'>Xóa sản phẩm</li></a>

                                                                </ul>
                                                            </li>
                                                        </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                                </table>
                                                <div class="center">{!! $products_quantity->links() !!}</div>

                                        </div>
                                        
                                        

                            </div>
                            <!-- /.box-header -->
                            
                        <div class="clearfix visible-sm-block"></div>
        </div>
        </div>
            <!-- Shop Page Navigation -->

            <div class="shop_page_nav d-flex flex-row">
                
                
            </div>

</div>

</div>
</div>
</div>
</div>

@endsection
