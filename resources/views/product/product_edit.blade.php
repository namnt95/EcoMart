@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                  
                <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px;padding-left: 25px">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title" style="margin-top: 10px">Sửa sản phẩm</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">

                  {!!  Form::model($product,array('route' => ['product.update', $product->id], 'method' => 'PUT', 'files' => true, 'enctype' =>"multipart/form-data" ))  !!}
                      {{ csrf_field() }}

                  <input name="_method" type="hidden" value="PATCH">

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('name','Tên sản phẩm')  !!}</td>
                          <td>{!! Form::text('name', null, ['class' => 'form-control' ])  !!}
                              @if ($errors->first('name'))
                                  <span style="color: red">{{ $errors->first('name') }}</span>
                              @endif
                          </td>
                      </div>
                  </tr>


                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('quantity','Số Lượng')  !!}</td>
                          <td>{!! Form::text('quantity', null, ['class' => 'form-control' ])  !!}
                          @if ($errors->first('quantity'))
                              <span style="color: red">{{ $errors->first('quantity') }}</span>
                          @endif</td>
                      </div>
                  </tr>


                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('price','Giá')  !!}</td>
                          <td>{!! Form::text('price', null, ['class' => 'form-control' ])  !!}
                          @if ($errors->first('price'))
                              <span style="color: red">{{ $errors->first('price') }}</span>
                          @endif</td>
                      </div>
                  </tr>


                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('Specifications','Thông Số Kỹ Thuật')  !!}</td>
                          <td>
                              <table width="100%">
                                  <tr>
                                      <td>{!! Form::label('Specifications','Màn Hình')  !!}</td>
                                      <td>{!! Form::text('display', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('display'))
                                              <span style="color: red">{{ $errors->first('display') }}</span>
                                          @endif
                                      </td>
                                  </tr>   

                                  <tr>
                                      <td>{!! Form::label('Specifications','Hệ Điều Hành')  !!}</td>
                                      <td>{!! Form::text('os', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('os'))
                                              <span style="color: red">{{ $errors->first('os') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','Bộ Nhớ')  !!}</td>
                                      <td>{!! Form::text('memory', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('memory'))
                                              <span style="color: red">{{ $errors->first('memory') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','Camera')  !!}</td>
                                      <td>{!! Form::text('camera', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('camera'))
                                              <span style="color: red">{{ $errors->first('camera') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','CPU')  !!}</td>
                                      <td>{!! Form::text('cpu', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('cpu'))
                                              <span style="color: red">{{ $errors->first('cpu') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','Ram')  !!}</td>
                                      <td>{!! Form::text('ram', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('ram'))
                                              <span style="color: red">{{ $errors->first('ram') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','Sim')  !!}</td>
                                      <td>{!! Form::text('sim', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('sim'))
                                              <span style="color: red">{{ $errors->first('sim') }}</span>
                                          @endif
                                      </td>
                                  </tr>

                                  <tr>
                                      <td>{!! Form::label('Specifications','Pin')  !!}</td>
                                      <td>{!! Form::text('pin', null, ['class' => 'form-control' ])  !!}
                                          @if ($errors->first('pin'))
                                              <span style="color: red">{{ $errors->first('pin') }}</span>
                                          @endif
                                      </td>
                                  </tr>
                              </table>
                          </td>
                          
                      </div>
                  </tr>

                  <tr>
                      <td>{!! Form::label('promotion','Khuyến Mãi')  !!}</td>
                      <td>{!! Form::text('promotion', null, ['class' => 'form-control' ])  !!}
                          @if ($errors->first('promotion'))
                              <span style="color: red">{{ $errors->first('promotion') }}</span>
                          @endif
                      </td>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('review','Review')  !!}</td>
                          <td>{!! Form::textarea('review', null, ['class' => 'form-control', 'class' => 'ckeditor' ])  !!}</td>
                          
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('img','IMG 1')  !!}</td>
                          <td>{!! Form::file('img1', null, ['class' => 'form-control' ])  !!}
                              @if ($errors->first('img1'))
                                  <span style="color: red">{{ $errors->first('img1') }}</span>
                              @endif
                          </td>
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('img','IMG 2')  !!}</td>
                          <td>{!! Form::file('img2', null, ['class' => 'form-control' ])  !!}
                              @if ($errors->first('img2'))
                                  <span style="color: red">{{ $errors->first('img2') }}</span>
                              @endif
                          </td>
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('img','IMG 3')  !!}</td>
                          <td>{!! Form::file('img3', null, ['class' => 'form-control' ])  !!}
                              @if ($errors->first('img3'))
                                  <span style="color: red">{{ $errors->first('img3') }}</span>
                              @endif
                          </td>
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('categories_id','Danh mục')  !!}</td>
                          <td>
                                <select name="categories_id" class="form-control">
                                  @foreach( $categoriess as $categories )
                                    @if ( $categories->id == $product->categories_id )
                                        <option value="{!! $categories->id !!}" selected=""> {{ $categories->name }}</option>
                                    @endif
                                    @if ( $categories->id != $product->categories_id )
                                        <option value="{!! $categories->id !!}" > {{ $categories->name }}</option>
                                    @endif 
                                  @endforeach
                                </select>
                          </td>
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td>{!! Form::label('brand_id','Hãng')  !!}</td>
                          <td>
                            <select name="brand_id" class="form-control">
                              @foreach( $brands as $brand )
                                @if ( $brand->id == $product->brand_id )
                                    <option value="{!! $brand->id !!}" selected=""> {{ $brand->name }}</option>
                                @endif
                                @if ( $brand->id != $product->brand_id )
                                    <option value="{!! $brand->id !!}" > {{ $brand->name }}</option>
                                @endif 
                              @endforeach 
                            </select>
                          </td>
                      </div>
                  </tr>

                  <tr>
                      <div class="form-group">
                          <td align="right"><a class="btn btn-link " href="{{ route('product.index') }}">Back</i></a></td>
                          <td>{!! Form::button('Sửa', ['type' => 'submit', 'class' => 'btn btn-success'])  !!}</td>
                      </div>
                  </tr>
                  {!! Form::close() !!}

                  </table>

                      <div class="clearfix visible-sm-block"></div>
                  </div>
                  <!-- /.row -->

                      

                            </div>
                    </div>
               </div>

                             
                      </div>
                  </div>
              </div>
                    </div>
                    <br>
                  
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
