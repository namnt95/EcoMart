@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">
                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px;padding-left: 25px">
                        <div class="box">
                            <div class="box-header" style="margin-top: 10px">
                            <h3> Tổng Quan Giao Dịch</h3>
                            <h6>Chất lượng sản phẩm: 
                                @if ( $products_ratings <= 1 )
                                    Tồi
                                @elseif ( $products_ratings > 1 && $products_ratings <= 2 )
                                    Kém
                                @elseif ( $products_ratings > 2 && $products_ratings <= 3 )
                                    Trung Bình
                                @elseif ( $products_ratings > 3 && $products_ratings <= 4 )
                                    Khá
                                @else 
                                    Tốt
                                @endif
                            </h6>
                            <h4>Lượt đăng: {{ $post }}</h4>
                            @if ( $pay == 1 ) 
                                <h4>
                                    <a href="{{ route('pay', $user->id ) }}">Yêu cầu thanh toán</a>
                                </h4>
                            @else 
                                <h4>
                                    <a href="{{ route('pay', $user->id ) }}">Hủy yêu cầu thanh toán</a>
                                </h4>
                            @endif
                            </h4>
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Tất Cả ( {!! $count !!} ) </a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Đơn Hàng Đã Xử lý ( {!! $countXL !!} )</a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Đơn Hàng Chưa Xử lý ( {!! $countCXL !!} )</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                     
                                    <br>
                                    <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="table-active">
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Tên Sản Phẩm</th>
                                        <th class="text-center">Số Lượng</th>
                                        <th class="text-center">Ngày Đặt Hàng</th>
                                        <th class="text-center">Tổng Tiền</th>
                                        <th class="text-center">Trạng Thái</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;   ?>

                                    @foreach ($customerInfos as $customerInfo)
                                    <tr>
                                        <td  class="text-center">{{ $i++ }}</td>
                                        <td>{{ $customerInfo->name }}</td>
                                        <td  class="text-center">{{ $customerInfo->quantity }}</td>
                                        <td  class="text-center">{{ $customerInfo->created }}</td>
                                        <td  class="text-center">{{ number_format($customerInfo->bill_total) }} VNĐ</td>
                                        <td  class="text-center">
                                            @if ( $customerInfo->bill_status == 1 || $customerInfo->bill_status == 2 || $customerInfo->bill_status == 3)
                                                @if ( $customerInfo->bill_status == 1 )
                                                    Chưa giao
                                                @endif 
                                                @if ( $customerInfo->bill_status == 2 )
                                                    Đang giao
                                                @endif 
                                                @if ( $customerInfo->bill_status == 3 )
                                                    Đã giao
                                                @endif 
                                            @else
                                                {{ $customerInfo->bill_status }}
                                            @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>

                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                    <br>
                                    <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="table-active">
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Tên Sản Phẩm</th>
                                        <th class="text-center">Số Lượng</th>
                                        <th class="text-center">Ngày Đặt Hàng</th>
                                        <th class="text-center">Tổng Tiền</th>
                                        <th class="text-center">Trạng Thái</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;   ?>

                                    @foreach ($customerInfosXL as $customerInfoXL)
                                    <tr>
                                        <td  class="text-center">{{ $i++ }}</td>
                                        <td>{{ $customerInfoXL->name }}</td>
                                        <td  class="text-center">{{ $customerInfoXL->quantity }}</td>
                                        <td  class="text-center">{{ $customerInfoXL->created }}</td>
                                        <td  class="text-center">{{ number_format($customerInfoXL->bill_total) }} VNĐ</td>
                                        <td  class="text-center">
                                            @if ( $customerInfoXL->bill_status == 1 || $customerInfoXL->bill_status == 2 || $customerInfoXL->bill_status == 3)
                                                @if ( $customerInfoXL->bill_status == 1 )
                                                    Chưa giao
                                                @endif 
                                                @if ( $customerInfoXL->bill_status == 2 )
                                                    Đang giao
                                                @endif 
                                                @if ( $customerInfoXL->bill_status == 3 )
                                                    Đã giao
                                                @endif 
                                            @else
                                                {{ $customerInfoXL->bill_status }}
                                            @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>


                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <br>
                                    <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr class="table-active">
                                        <th class="text-center">STT</th>
                                        <th class="text-center">Tên Sản Phẩm</th>
                                        <th class="text-center">Số Lượng</th>
                                        <th class="text-center">Ngày Đặt Hàng</th>
                                        <th class="text-center">Tổng Tiền</th>
                                        <th class="text-center">Trạng Thái</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1;   ?>

                                    @foreach ($customerInfosCXL as $customerInfoCXL)
                                    <tr>
                                        <td  class="text-center">{{ $i++ }}</td>
                                        <td>{{ $customerInfoCXL->name }}</td>
                                        <td  class="text-center">{{ $customerInfoCXL->quantity }}</td>
                                        <td  class="text-center">{{ $customerInfoCXL->created }}</td>
                                        <td  class="text-center">{{ number_format($customerInfoCXL->bill_total) }} VNĐ</td>
                                        <td  class="text-center">
                                            @if ( $customerInfoCXL->bill_status == 1 || $customerInfoCXL->bill_status == 2 || $customerInfoCXL->bill_status == 3)
                                                @if ( $customerInfoCXL->bill_status == 1 )
                                                    Chưa giao
                                                @endif 
                                                @if ( $customerInfoCXL->bill_status == 2 )
                                                    Đang giao
                                                @endif 
                                                @if ( $customerInfoCXL->bill_status == 3 )
                                                    Đã giao
                                                @endif 
                                            @else
                                                {{ $customerInfoCXL->bill_status }}
                                            @endif
                                        </td>

                                    </tr>
                                    @endforeach
                                    </tbody>
                                    </table>


                                </div>
                            </div>       
                                        
                                        
                                        

                            </div>
                            <!-- /.box-header -->
                            
                        <div class="clearfix visible-sm-block"></div>
        </div>
        </div>
            <!-- Shop Page Navigation -->

            <div class="shop_page_nav d-flex flex-row">
                
                
            </div>

</div>

</div>
</div>
</div>
</div>

@endsection
