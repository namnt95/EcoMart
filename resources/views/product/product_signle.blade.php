@extends('inc-index/layout')
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/css_product_signle.css') }}">
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
 -->
<script type="text/javascript">
	$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    
    
  });
  
  
});


function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
}
</script>
@section('content')
	<!-- Single Product -->
	<div class="single_product" style="margin-top: -90px" >
		<div class="container">
			
			
			<hr>
			<br>
			<div class="row">
				<!-- Images -->

				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						<li data-image="{{ asset($product->img1) }}"><img src="{{ asset($product->img1) }}" alt=""></li>
						<li data-image="{{ asset($product->img2) }}"><img src="{{ asset($product->img2) }}" alt=""></li>
						<li data-image="{{ asset($product->img3) }}"><img src="{{ asset($product->img3) }}" alt=""></li>
					</ul>

				</div>

				<!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 ">
					<div class="image_selected"><img src="{{ asset($product->img1) }}" alt=""></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">

					<div class="product_description" >
						<div class="product_name" style="font-family:serif">{{ $product->name }}</div>
						<div class='rating-widget' style="margin-left: -230px">
  
						  <!-- Rating Stars Box -->
						  <div class='rating-stars text-center'>
						    <ul id='stars'>
						    @if ( $user )
						    	@if ( number_format($star) == 1 )
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('star', [1, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2'>
								        <a href=" {{ route('star', [2, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								        <a href=" {{ route('star', [3, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
								        <a href=" {{ route('star', [4, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
								        <a href=" {{ route('star', [5, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 2 )
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('star', [1, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
								        <a href=" {{ route('star', [2, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								        <a href=" {{ route('star', [3, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
								        <a href=" {{ route('star', [4, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
								        <a href=" {{ route('star', [5, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 3 )
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('star', [1, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
								        <a href=" {{ route('star', [2, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								    	<a href=" {{ route('star', [3, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4' >
								        <a href=" {{ route('star', [4, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc" ></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
								        <a href=" {{ route('star', [5, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 4 )
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('star', [1, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
								        <a href=" {{ route('star', [2, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								        <a href=" {{ route('star', [3, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
								       	<a href=" {{ route('star', [4, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
								        <a href=" {{ route('star', [5, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc;"></i>
								        </a>
								    </li>
						    	@else
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('star', [1, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    <li class='star' title='Kém' data-value='2' >
								        <a href=" {{ route('star', [2, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								        <a href=" {{ route('star', [3, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
								        <a href=" {{ route('star', [4, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5' >
								        <a href=" {{ route('star', [5, $product->id]) }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
						    	@endif
						      	có {{ $count_star }} đánh giá
						    @else
						    	@if ( number_format($star) == 1 )
						    		<li class='star' title='Tồi' data-value='1' >
								        <a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2'>
								        <a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
								        <a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
								        <a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
								        <a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 2 )
						    		<li class='star' title='Tồi' data-value='1' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 3 )
						    		<li class='star' title='Tồi' data-value='1' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc" ></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc"></i>
								        </a>
								    </li>
						    	@elseif ( number_format($star) == 4 )
						    		<li class='star' title='Tồi' data-value='1' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Kém' data-value='2' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#ccc;"></i>
								        </a>
								    </li>
						    	@else
						    		<li class='star' title='Tồi' data-value='1' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    <li class='star' title='Kém' data-value='2' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Trung Bình' data-value='3' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Khá' data-value='4'>
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
								    <li class='star' title='Rất tốt!!!' data-value='5' >
						    			<a href=" {{ route('login') }}">
								        <i class='fa fa-star fa-fw' style="color:#FF912C;"></i>
								        </a>
								    </li>
						    	@endif
						      	có {{ $count_star }} đánh giá
						    @endif

						    </ul> 

						  </div>
						  
						  <div class='success-box'>
						    <div class='clearfix'></div>
						    <!-- <img alt='tick image' width='32' src='https://i.imgur.com/3C3apOp.png'/> -->
						    <div class='text-message' style="margin-left: 150px"></div>
						    <div class='clearfix'></div>
						  </div>
						  
						  
						</div>
						<div class="product_price" style="font-family:serif;color:red"><?php echo number_format($product->price , 0, ',', '.'); ?> 
						<img src="{{ asset('images\vnd.png') }}" width='19px' style="margin-left: -5px">
					</div><br><br>
						@foreach ( $shops as $shop )
						<a href="{{ route('product_shop', [$shop->id] ) }}">
						<h3 class="viewed_title" style="font-family:serif"> Cửa Hàng
							{{ $shop->shop }}
						@endforeach
						</h3>
						</a>
						<hr>
						<h3 class="viewed_title" style="font-family:serif">THÔNG SỐ KĨ THUẬT</h3>


						<style>
						table {
						    border-collapse: collapse;
						    width: 100%;
						}

						th, td {
						    padding: 8px;
						    text-align: left;
						    border-bottom: 1px solid #ddd;
						}

						tr:hover {background-color:#f5f5f5;}
						</style>

						
						<table>
							<tr>
								<td width="40%"> Màn Hình	</td>
								<td>{!! $product->display !!}</td>
							</tr>

							<tr>
								<td>Hệ Điều Hành	</td>
								<td>{!! $product->os !!}</td>
							</tr>

							<tr>
								<td>Bộ Nhớ	</td>
								<td>{!! $product->memory !!}</td>
							</tr>

							<tr>
								<td>Camera	</td>
								<td>{!! $product->camera !!}</td>
							</tr>

							<tr>
								<td>CPU</td>
								<td>{!! $product->cpu !!}</td>
							</tr>

							<tr>
								<td>Ram</td>
								<td>{!! $product->ram !!}</td>
							</tr>

							<tr>
								<td>Sim</td>
								<td>{!! $product->sim !!}</td>
							</tr>

							<tr>
								<td>Pin</td>
								<td>{!! $product->pin !!}</td>
							</tr>
							
						</table>
						<br>
						@if ( $product->quantity > 0  )
							<h4 style="color: red">Số Lượng: {!! $product->quantity !!} </h4>
						@else 
							<h4 style="color: red">Hết Hàng</h4>
						@endif
					</div>
				</div>
				</div>
<br>
    <div class="clearfix visible-sm-block"></div>
				<div class="row">
				<div class="col-lg-7" style="background-color: #fafafa;border-radius: 5px">	
					<div class="product_text" >
								<h4  style="color: red">{!! $product->promotion !!}</h4>
					</div>
								<h4  style="color: blue">Địa chỉ cửa hàng: {!! $shop->addressshop !!}</h4>
								<h4  style="color: blue">SĐT: {!! $shop->phoneshop !!}</h4>
								<h4  style="color: blue">Web: {!! $shop->linkshop !!}</h4>

				</div>	
				<div class="col-lg-5 order-lg-2 ">	

						<div class="order_info d-flex flex-row  ">
							<div class="product_description">
								<div class="clearfix" style="z-index: 1000;">

									<!-- Product Quantity -->
									<div class="product_quantity clearfix">
									<form method="get" action="{!! route('create_cart', [$product->id]) !!}">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
										<span>Số Lượng: </span>
										<input id="quantity_input" type="text" pattern="[0-9]*" value="1" name="qty">
										<div class="quantity_buttons">
											<div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
											<div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
										</div>

									</div>
									<button type="submit" class="button cart_button">Thêm Vào Giỏ</button>
									</form>
									<div class="product_fav"><i class="fas fa-heart"></i></div>
									</div>
								</div>
							
						</div>
					</div>
			</div>
		</div>
	</div>
	
	<!-- COMMENT -->
	<div class="row" style="margin-left: -400px;margin-top: -150px">
	<!-- Contenedor Principal -->
    <div class="comments-container">
		<h3>Nhận xét về sản phẩm </h3>
        
		<ul id="comments-list" class="comments-list">
     	@guest
			@foreach ( $cmts as $cmt )
     			<li>
					<div class="comment-main-level">
						<!-- Avatar -->
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name"><a  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>"href="http://creaticode.com/blog">{{ $cmt->name }}</a></h6>
								<span  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>">{{ $cmt->created_at }}</span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								{{ $cmt->comments_1 }}
							</div>
						</div>
					</div>
					<ul class="comments-list reply-list">
						@foreach ( $cmts_2 as $cmt_2 )
						<li>
							<!-- Avatar -->
							<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
							<!-- Contenedor del Comentario -->
							<div class="comment-box">
								<div class="comment-head">
									<h6 class="comment-name"><a  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>"href="http://creaticode.com/blog">{{ $cmt_2->name }}</a></h6>
									<span  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>">{{ $cmt_2->created_at }}</span>
									<i class="fa fa-reply"></i>
									<i class="fa fa-heart"></i>
								</div>
								<div class="comment-content">
									{{ $cmt_2->comments_2 }}
								</div>
							</div>
						</li>
						@endforeach
					</ul>
				</li>
				@endforeach
			</div>
	    @else
			@foreach ( $cmts as $cmt )
			<li>
				<div class="comment-main-level">
					<!-- Avatar -->
					<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
					<!-- Contenedor del Comentario -->
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name"><a  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>"href="http://creaticode.com/blog">{{ $cmt->name }}</a></h6>
							<span  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>">{{ $cmt->created_at }}</span>
							<i class="fa fa-reply"></i>
							<i class="fa fa-heart"></i>
						</div>
						<div class="comment-content">
							{{ $cmt->comments_1 }}
						</div>
					</div>
				</div>
				<ul class="comments-list reply-list">
					@foreach ( $cmts_2 as $cmt_2 )
					<li>
						<!-- Avatar -->
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name"><a  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>"href="http://creaticode.com/blog">{{ $cmt_2->name }}</a></h6>
								<span  style="color: <?php $rand = str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);  echo '#'.$rand; ?>">{{ $cmt_2->created_at }}</span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								{{ $cmt_2->comments_2 }}
							</div>
						</div>
					</li>
					@endforeach

					<li>
						<!-- Avatar -->
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name"><a href="http://creaticode.com/blog">{{ Auth::user()->name }}</a></h6>
							</div>
							<div class="comment-content">
								{!!  Form::open(array('route' => ['update_2', $cmt->id, $product->id], 'method' => 'POST')) !!}
		                       		{!! Form::textarea('comments_2', null, ['class' => 'form-control', 'placeholder' => 'Nhận xét...', 'rows' => '2', 'style' => 'font-size: 100%;' ])  !!}
									<div align="right">
										{!! Form::button('Gửi', ['type' => 'submit', 'class' => 'btn btn-info', 'style' => 'font-size: 100%;'])  !!}
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</li>
				</ul>
			</li>
			@endforeach
			<li>
			<div class="comment-main-level">
					<!-- Avatar -->
					<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
					<!-- Contenedor del Comentario -->
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name "><a href="http://creaticode.com/blog">{{ Auth::user()->name }}</a></h6>
							
						</div>
						<div class="comment-content">
						{!!  Form::open(array('route' => ['comments.update', $product->id], 'method' => 'PUT')) !!}
                       		{!! Form::textarea('comments_1', null, ['class' => 'form-control', 'placeholder' => 'Nhận xét...', 'rows' => '2', 'style' => 'font-size: 100%;' ])  !!}
							<div align="right">
								{!! Form::button('Gửi', ['type' => 'submit', 'class' => 'btn btn-info', 'style' => 'font-size: 100%;'])  !!}
							</div>
						{!! Form::close() !!}
						</div>
					</div>
			</li>
		</div>
		 @endguest
		</ul>

	</div>
         <div style="margin-left: 740px">{!! $cmts->links() !!}</div>

	</div>




	<!-- Recently Viewed -->
						<h3 class="viewed_title" style="margin-left: 100px">Sản Phẩm Tương Tự</h3>
	<div >
		<div class="container">
			<div class="row">
					@foreach ($products as $product)

						<a href="{{ route('product.show',$product->id) }}">
							<div class="product_item is_new">
								<div class="product_border"></div>
								<div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{ asset($product->img1) }}" alt="" width="150px" height="150px"></div>
								<div class="product_content">
									<div class="product_price"><?php echo number_format($product->price , 0, ',', '.'); ?> 
									<img src="{{ asset('images\vnd.png') }}" width='19px' style="margin-left: -5px">
									</div>
									<div class="product_name"><div><a href="#" tabindex="0">{{ $product->name }}</a></div></div>
								</div>
								<div class="product_fav"><i class="fas fa-heart"></i></div>
								<ul class="product_marks">
									<li class="product_mark product_discount">-25%</li>
									<li class="product_mark product_new">new</li>
								</ul>
								<div><a href="{!! route('index', [$product->id]) !!}">Thêm Giỏ Hàng</a></div>
							</div>
							</a>
					@endforeach

			</div>
		</div>
	</div>

							


	<!-- Brands -->

	<div class="brands">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="brands_slider_container">
						
						<!-- Brands Slider -->

						<div class="owl-carousel owl-theme brands_slider">
							@foreach ( $brands as $brand )
							<div class="owl-item"><div class="brands_item d-flex flex-column justify-content-center"><img src="{{ asset($brand->img) }}" alt=""></div></div>
							@endforeach
						</div>
						
						<!-- Brands Slider Navigation -->
						<div class="brands_nav brands_prev"><i class="fas fa-chevron-left"></i></div>
						<div class="brands_nav brands_next"><i class="fas fa-chevron-right"></i></div>

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
