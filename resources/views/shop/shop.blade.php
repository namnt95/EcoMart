@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                  
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                    TỔNG QUAN
                    </a>
                    <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                    <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                    <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('shop.index') }}"">Danh Sách Sản Phẩm</a>
                    <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('product.create') }}"">Đăng Sản Phẩm</a>
                    <!--  -->
                 
                    </div>
                </div>
                <div class="col-lg-9">
                        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sản Phẩm</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th class="text-center">STT</th>
                            <th class="text-center">name</th>
                            <th class="text-center">price</th>
                            <th class="text-center">review</th>
                            <th class="text-center">img</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1 ?>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $product->name}}</td>
                            <td>{{ $product->price }} Đ</td>
                            <td width="300px" height="100px">{!! $product->review !!}</td>
                            <td><img src="{{ $product->img1 }}  "></td>
                            <td>
                                {!! Form::open(array('route'=> ['product.destroy', $product->id], 'method' => 'DELETE')) !!}
                                    {{ link_to_route('product.edit', 'Sửa', [$product->id], ['class' => 'btn btn-primary btn-xs btn-block']) }}
                                
                                    {!! Form::button('Xóa', ['class' => 'btn btn-danger btn-xs btn-block', 'type' => 'submit']) !!}                    
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                            <div class="center">{!! $products->links() !!}</div>
                        <div class="clearfix visible-sm-block"></div>
                     </div>
            </div>
        </div>
    </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
