@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 750px;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->shop || Auth::user()->addressshop || Auth::user()->phoneshop || Auth::user()->linkshop  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                            <div class="shop_content">
                                <div class=" clearfix" style="margin-top: 10px">
                                    <div class="shop_product_count"><h3>Mua Lượt</h3> </div>
                                </div>
                                <div>@include('flash::message')
                                   <script>
                                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                                    </script>
                                </div>
                                <p>Số dư tài khoản: {{ number_format($money, 0, ',', '.') }} đ</p>
                                    
                                <form action="{{ route('buy') }}" method="get">
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control" name="post">
                                        
                                        <span class="input-group-append">
                                        <input type="submit" name="submit" class="btn btn-info btn-flat" value="Mua">
                                        </span>
                                    </div>
                                </form>

                                      <p>Ví dụ: <code>10.000đ = lượt, 20.000đ = 2lượt</code></p>



                        </div>
                    </div>
                    </div>
              
                

                    
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
