@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 750px;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                            <div class="shop_content">
                                <div class=" clearfix" style="margin-top: 10px">
                                    <div class="shop_product_count">
                                        <h3>Thông Tin Tài Khoản</h3>
                                        <p>
                                        @if ( $users->status_shop == '1' )
                                            Tài khoản đã xác minh
                                        @elseif ( $users->status_shop == '2' )
                                            Đang xác minh cửa hàng
                                        @elseif ( $users->status_shop == '3' ) 
                                            Yêu cầu xác minh cửa hàng bị hủy
                                        @else 
                                            <a href="">Xác minh tài khoản <a href=""></a>
                                        @endif
                                        </p>
                                    </div>
                                    
                                </div>
                                <div>@include('flash::message')
                                   <script>
                                    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
                                    </script>
                                </div>
                                <div class="product_grid">
                                   {!!  Form::model($user,array('route' => ['user.update', $user->id], 'method' => 'PUT'))  !!}
                                    <input name="_method" type="hidden" value="PATCH">
                                    <table id="example2"  class="table-nam" style="margin-left: -10px">
                                    <tr>
                                        <div class="form-group">
                                            <td rowspan="3">
                                                <div  align="center"  style="margin-top: -10px">
                                                    <img src="{{ asset($user->img) }}" width="150px" height="150px">
                                                </div>
                                            </td>
                                            <td >{!! Form::label('email','Email')  !!}</td>
                                            <td>{!! Form::email('email', null, ['class' => 'form-control',  'readonly' => 'true' ])  !!}</td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="form-group">
                                            <td>{!! Form::label('name','Họ và tên')  !!}</td>
                                            <td>{!! Form::text('name', null, ['class' => 'form-control'])  !!}</td>
                                        </div>
                                    </tr>
                                        
                                    <tr>
                                        <div class="form-group">
                                            <td>{!! Form::label('address','Địa chỉ')  !!}</td>
                                            <td>{!! Form::text('address', null, ['class' => 'form-control' ])  !!}</td>
                                        </div>
                                    </tr>

                                    <tr>
                                        <div class="form-group">
                                            <td></td>
                                            <td>{!! Form::label('phone','Số điện thoại')  !!}</td>
                                            <td>{!! Form::text('phone', null, ['class' => 'form-control' ])  !!}</td>
                                        </div>
                                    </tr>
                                    {!! Form::close() !!}

                                    <tr>
                                        <div class="form-group">
                                            <td></td>
                                            <td></td>
                                            <td align="left"><a href="{{ route('user_personalinformation', $user ) }}" class="btn btn-primary">Thay Đổi</a></td>
                                        </div>
                                    </tr>
                                    
                                </table>
                                </div>
                        </div>
                    </div>
                    </div>
              
                

                    
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
