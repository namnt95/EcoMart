@extends('inc-index/layout')
@section('content')


    <!-- Characteristics -->

    <div class="single_product">
        <div class="container">
            <div class="row">

    
        <div class="col-sm-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ĐĂNG KÍ SHOP BÁN HÀNG</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
           
            <form action="{{ route('user.store') }}" method="POST" >
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('email')) style="color:red" @endif >Email</label></td>
                    <td>
                        <input @if ($errors->first('email')) style="border-color:red" @endif type="text" name="email" class="form-control" placeholder="Nhập Email.."  }}">
                        @if ($errors->first('email'))
                            <span style="color: red">{{ $errors->first('email') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('password')) style="color:red" @endif >Mật Khẩu</label></td>
                    <td>
                        <input @if ($errors->first('password')) style="border-color:red" @endif type="text" name="password" class="form-control" placeholder="Nhập Mật Khẩu..."  }}">
                        @if ($errors->first('password'))
                            <span style="color: red">{{ $errors->first('password') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('name')) style="color:red" @endif >Tên </label></td>
                    <td>
                        <input @if ($errors->first('name')) style="border-color:red" @endif type="text" name="name" class="form-control" placeholder="Nhập Tên..."  }}">
                        @if ($errors->first('name'))
                            <span style="color: red">{{ $errors->first('name') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('address')) style="color:red" @endif >Địa Chỉ</label></td>
                    <td>
                        <input @if ($errors->first('address')) style="border-color:red" @endif type="text" name="address" class="form-control" placeholder="Nhập Địa Chỉ..."  }}">
                        @if ($errors->first('address'))
                            <span style="color: red">{{ $errors->first('address') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('phone')) style="color:red" @endif >Số Điện Thoại</label></td>
                    <td>
                        <input @if ($errors->first('phone')) style="border-color:red" @endif type="text" name="phone" class="form-control" placeholder="Nhập SĐT..."  }}">
                        @if ($errors->first('phone'))
                            <span style="color: red">{{ $errors->first('phone') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('shop')) style="color:red" @endif >Shop</label></td>
                    <td>
                        <input @if ($errors->first('shop')) style="border-color:red" @endif type="text" name="shop" class="form-control" placeholder="Nhập Tên Shop..."  }}">
                        @if ($errors->first('shop'))
                            <span style="color: red">{{ $errors->first('shop') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('addressshop')) style="color:red" @endif >Địa Chỉ Shop</label></td>
                    <td>
                        <input @if ($errors->first('addressshop')) style="border-color:red" @endif type="text" name="addressshop" class="form-control" placeholder="Nhập Địa Chỉ Shop..."  }}">
                        @if ($errors->first('addressshop'))
                            <span style="color: red">{{ $errors->first('addressshop') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('phoneshop')) style="color:red" @endif >SĐT Shop</label></td>
                    <td>
                        <input @if ($errors->first('phoneshop')) style="border-color:red" @endif type="phoneshop" name="phoneshop" class="form-control" placeholder="Nhập SĐT Shop..."  }}">
                        @if ($errors->first('phoneshop'))
                            <span style="color: red">{{ $errors->first('phoneshop') }}</span>
                        @endif
                    </td>
                </tr>

                <tr>
                    <div class="form-group" >
                    <td><label  @if ($errors->first('linkshop')) style="color:red" @endif >Link</label></td>
                    <td>
                        <input @if ($errors->first('linkshop')) style="border-color:red" @endif type="text" name="linkshop" class="form-control" placeholder="Nhập Link Shop..."  }}">
                        @if ($errors->first('linkshop'))
                            <span style="color: red">{{ $errors->first('linkshop') }}</span>
                        @endif
                    </td>
                </tr>
                
               
                
                <tr>
                    <div class="form-group">
                        <td align="right"><a class="btn btn-link" href="{{ route('user.index') }}">Trở Lại</i></a></td>
                        <td><button type="submit" name="register" class="btn btn-success">Thêm</button></td>
                    </div>
                </tr>
            </form>

             </table>

                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->

                    </div>
                </div>
            </div>

@endsection
