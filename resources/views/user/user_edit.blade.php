@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')


	<!-- Characteristics -->

	<div class="single_product">
		<div class="container">
			<div class="row">

	
        <div class="col-sm-8" style="border: 1px solid rgba(0,0,0,.125); border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding:50px;margin-left: 200px">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ĐĂNG KÍ SHOP BÁN HÀNG</h3>
              <p style="color: red">* Vui lòng điền đầy đủ thông tin</p>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <table id="example2" class="table table-bordered table-hover" width="80%">
            {!!  Form::model($user,array('route' => ['user.update', $user->id], 'method' => 'put', 'files' => true, 'enctype' =>"multipart/form-data" ))  !!}
			<input name="_method" type="hidden" value="PATCH">
			<tr>
				<div class="form-group">
					<td>
						{!! Form::label('email ','Email')  !!}*
					</td>
					<td>
						{!! Form::email('email', null, ['class' => 'form-control', 'readonly' => 'true' ]) !!}
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('name','Họ và tên')  !!}*</td>
					<td>
						{!! Form::text('name', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('name'))
                            <span style="color: red">{{ $errors->first('name') }}</span>
                        @endif
					</td>
				</div>
			</tr>
				
			<tr>
				<div class="form-group">
					<td>{!! Form::label('address','Địa chỉ')  !!}*</td>
					<td>
						{!! Form::text('address', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('address'))
                            <span style="color: red">{{ $errors->first('address') }}</span>
                        @endif
					</td>
				</div>
			</tr>


			<tr>
				<div class="form-group">
					<td>{!! Form::label('password','Mật khẩu')  !!}*</td>
					<td>
						{!! Form::text('password', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('password'))
                            <span style="color: red">{{ $errors->first('password') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('phone','Số điện thoại')  !!}*</td>
					<td>
						{!! Form::text('phone', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('phone'))
                            <span style="color: red">{{ $errors->first('phone') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('shop','Tên Shop')  !!}*</td>
					<td>
						{!! Form::text('shop', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('shop'))
                            <span style="color: red">{{ $errors->first('shop') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('addressshop','Địa Chỉ Shop')  !!}*</td>
					<td>
						{!! Form::text('addressshop', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('addressshop'))
                            <span style="color: red">{{ $errors->first('addressshop') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('phoneshop','Số điện thoại Shop')  !!}*</td>
					<td>
						{!! Form::text('phoneshop', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('phoneshop'))
                            <span style="color: red">{{ $errors->first('phoneshop') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('linkshop','Link Shop')  !!}*</td>
					<td>
						{!! Form::text('linkshop', null, ['class' => 'form-control' ])  !!}
						@if ($errors->first('linkshop'))
                            <span style="color: red">{{ $errors->first('linkshop') }}</span>
                        @endif
					</td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('front_img','Ảnh trước CMT/ Passport/ Bằng Lái Xe')  !!}*</td>
                    <td>{!! Form::file('front_img', null, ['class' => 'form-control' ])  !!}
                        @if ($errors->first('front_img'))
                            <span style="color: red">{{ $errors->first('front_img') }}</span>
                        @endif
                    </td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td>{!! Form::label('backside_img','Ảnh sau CMT/ Passport/ Bằng Lái Xe')  !!}*</td>
                    <td>{!! Form::file('backside_img', null, ['class' => 'form-control' ])  !!}
                        @if ($errors->first('backside_img'))
                            <span style="color: red">{{ $errors->first('backside_img') }}</span>
                        @endif
                    </td>
				</div>
			</tr>

			<tr>
				<div class="form-group">
					<td align="right"><a class="btn btn-success " href="{{ route('user.index') }}">Trở lại</i></a></td>
					<td>{!! Form::button('Đăng Ký ', ['type' => 'submit', 'class' => 'btn btn-primary'])  !!}</td>
				</div>
			</tr>
			{!! Form::close() !!}

       		 </table>

                <div class="clearfix visible-sm-block"></div>
            </div>
            <!-- /.row -->

            </section>
            <!-- /.content -->

            		</div>
				</div>
			</div>

@endsection
