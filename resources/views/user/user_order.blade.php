@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px">
                    
                    <!-- Shop Content -->

                     <div class="box">
                            <div class="box-header clearfix" style="margin-top: 10px">
                            <div class="shop_product_count"><h3>Thông Tin Đơn Hàng</h3> </div>
                        </div>

                       <div class="box-header with-border">
                <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover ">
                    <thead>
                    <tr class="table-active">
                        <th >ID</th>
                        <th s>Tên người order</th>
                        <th >Ngày đặt hàng</th>
                        <th>Ngày nhận hàng</th>
                        <th>Trạng thái</th>
                        <th>Hành Động</th>
                       
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($customers as $customer)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $customer->name }}</td>
                                <td>{{ $customer->created_at }}</td>
                                <td>{{ $customer->senddate }}</td>
                                        <td>
                                            @if ( $customer->bill_status == 1 || $customer->bill_status == 2 || $customer->bill_status == 3)
                                                @if ( $customer->bill_status == 1 )
                                                    Chưa giao
                                                @endif 
                                                @if ( $customer->bill_status == 2 )
                                                    Đang giao
                                                @endif 
                                                @if ( $customer->bill_status == 3 )
                                                    Đã giao
                                                @endif 
                                            @else
                                                {{ $customer->bill_status }}
                                            @endif
                                        </td>
                                        <td><a href="{{ route('order_details', $customer->id)  }}">Chi Tiết</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
