@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">
                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>
                

                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px">
                    
                    <!-- Shop Content -->
                     <div class="box">
                            <div class="clearfix" style="margin-top: 10px">
                            <div class="shop_product_count"><h3>Chi Tiết Đơn Hàng</h3> </div>
                        </div>
                        <div class="box-header with-border" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class=" col-md-12"  >
                                        <h4></h4>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th class="table-active">Thông tin khách hàng</th>
                                                <th class="table-active"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>Thông tin người đặt hàng</td>
                                                <td>{{ $customerInfo->name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Ngày đặt hàng</td>
                                                <td>{{ $customerInfo->created_at }}</td>
                                            </tr>
                                            <tr>
                                                <td>Số điện thoại</td>
                                                <td>{{ $customerInfo->phone_number }}</td>
                                            </tr>
                                            <tr>
                                                <td>Địa chỉ</td>
                                                <td>{{ $customerInfo->address }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td>{{ $customerInfo->email }}</td>
                                            </tr>
                                            <tr>
                                                <td>Ghi chú</td>
                                                <td>{{ $customerInfo->bill_note }}</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    <table  class="table table-bordered table-hover " role="grid" aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row" class="table-active">
                                            <th>STT</th>
                                            <th class="sorting_asc col-md-4">Tên sản phẩm</th>
                                            <th class="sorting col-md-2">Số lượng</th>
                                            <th class="sorting col-md-2">Giá tiền</th>
                                        </thead>
                                        <tbody>
                                        @foreach($billInfo as $key => $bill)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $bill->product_name }}</td>
                                                <td>{{ $bill->quantity }}</td>
                                                <td>{{ number_format($bill->price) }} VNĐ</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="3"><b>Tổng tiền</b></td>
                                            <td colspan="1">
                                                <b class="text-red">
                                                        {{ number_format($customerInfo->bill_total, 0, ',', '.') }} VNĐ
                                                </b>
                                        </td>
                                        </tr>
                                     
                                        </tbody>
                                    </table>
                                        <div align="right"><a class="btn btn-link " href="{{ route('order') }}">Trở lại</i></a></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
