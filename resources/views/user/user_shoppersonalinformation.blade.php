@extends('inc-index/layout')
<script src="{{ asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('js/custom.js ')}}"></script>
@section('content')
   
   <style type="text/css">
       .table-nam {
                width: 100%;
                margin-left: 200px; 
                max-width: 60%;
                margin-bottom: 1rem;
                background-color: transparent;
       }

       .table-nam td {
            padding: .75rem;
       }
   </style>
    <div class="shop">
        <div class="container">
            <div class="row">

                <div class="col-lg-3"  style="border-right: 1px solid #eee">

                    <!-- Shop Sidebar -->
                    <div class="" ss="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                        TỔNG QUAN
                        </a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('user.index') }}">Thông Tin Tài Khoản</a>
                        <a class="sidebar_title list-group-item list-group-item-action" href=" {{ route('order') }}">Đơn Hàng</a>
                        @if( Auth::user()->status_shop == 1  )
                            <a class="nav-link  dropdown-toggle sidebar_title list-group-item list-group-item-action" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Quản Lý Shop
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item sidebar_title " href="{{ route('shop', Auth::user()->id )  }}">Thông tin Shop</a> 
                              <a class="dropdown-item sidebar_title " href="{{ route('product.index') }}">Danh Sách Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product.create') }}">Đăng Sản Phẩm</a>
                              <a class="dropdown-item sidebar_title " href="{{ route('product_exchange') }}">Tổng Quan Giao Dịch</a>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-lg-9" style="border: 1px solid rgba(0,0,0,.125);border-top-left-radius: .25rem;
             border-top-right-radius: .25rem; padding-bottom: 200px;padding-left: 25px">
                    
                    <!-- Shop Content -->

                    <div class="shop_content">
                        <div class="clearfix">
                            <div class="shop_product_count" style="margin-top: 10px"><h3>Sửa Thông Tin Tài Khoản</h3> </div>
                        </div>

                        <div class="product_grid">
                        
                        <table id="example2"  class="table-nam" style="margin-left: 30px">
                        {!!  Form::model($user,array('route' => ['shop_update', $user->id], 'method' => 'get'))  !!}
                            <input name="_method" type="hidden" value="PATCH">
                            <tr>
                                <div class="form-group">
                                    <td>
                                        {!! Form::label('shop ','Tên Cửa Hàng')  !!}*
                                    </td>
                                    <td>
                                        {!! Form::text('shop', null, ['class' => 'form-control']) !!}
                                    </td>
                                </div>
                            </tr>

                            <tr>
                                <div class="form-group">
                                    <td>{!! Form::label('addressshop','Địa Chỉ')  !!}*</td>
                                    <td>
                                        {!! Form::text('addressshop', null, ['class' => 'form-control' ])  !!}
                                        @if ($errors->first('addressshop'))
                                            <span style="color: red">{{ $errors->first('addressshop') }}</span>
                                        @endif
                                    </td>
                                </div>
                            </tr>
                                
                            <tr>
                                <div class="form-group">
                                    <td>{!! Form::label('phoneshop','Địa chỉ')  !!}*</td>
                                    <td>
                                        {!! Form::text('phoneshop', null, ['class' => 'form-control' ])  !!}
                                        @if ($errors->first('phoneshop'))
                                            <span style="color: red">{{ $errors->first('phoneshop') }}</span>
                                        @endif
                                    </td>
                                </div>
                            </tr>


                            <tr>
                                <div class="form-group">
                                    <td>{!! Form::label('linkshop','Web')  !!}*</td>
                                    <td>
                                        {!! Form::text('linkshop', null, ['class' => 'form-control' ])  !!}
                                        @if ($errors->first('linkshop'))
                                            <span style="color: red">{{ $errors->first('linkshop') }}</span>
                                        @endif
                                    </td>
                                </div>
                            </tr>
                            <tr>
                                <div class="form-group">
                                    <td align="right"><a class="btn btn-success " href="{{ route('shop', $user->id) }}">Trở lại</i></a></td>
                                    <td>{!! Form::button('Thay Đổi ', ['type' => 'submit', 'class' => 'btn btn-primary'])  !!}</td>
                                </div>
                            </tr>
                            {!! Form::close() !!}

                         </table>
               
                        
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                        <!-- Shop Page Navigation -->

                        <div class="shop_page_nav d-flex flex-row">
                            
                            
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
