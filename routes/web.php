<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	
// Route::get('find', 'SearchController@find');
// Route::get('view', 'SearchController@view');
// Route::get('customer/{id}', function($id) {
//    $customer = App\Models\Admin\Customers::find($id);
//    return $customer->name . '@' . $customer->phone . '-' . $customer->address;
// });

// Route::get('autocomplete', 'SearchController@autocomplete');

Route::get('customer/{id}', function() {
   $customer = Customer::find($id);
   return $customer->name . '@' . $customer->phone . '-' . $customer->address;
});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/', function () {
//     return view('layouts.index');
// });

	
// Admin
	Route::resource('categories_admin', 'Admin\Categories_AdminController');
	Route::resource('brand_admin', 'Admin\Brand_AdminController');
	Route::resource('status_admin', 'Admin\Status_AdminController');
	Route::resource('bill_admin', 'Admin\AdminBillController');
	Route::resource('user_admin', 'Admin\AdminUserController');
	Route::get('user_search_admin', 'Admin\AdminUserController@search')->name('user_search_admin');
	Route::get('userBlock_admin/{product_id}', 'Admin\AdminUserController@block')->name('userBlock_admin');
	Route::resource('profit', 'Admin\ProfitController');
	Route::get('invoice', 'Admin\ProfitController@invoice')->name('invoice');

	
	Route::resource('admin', 'Admin\DefaultController');
	Route::resource('status', 'StatusController');


	Route::resource('product_admin', 'Admin\Admin_ProductControlelr');
	Route::get('product_search_admin', 'Admin\Admin_ProductControlelr@search')->name('product_search_admin');
	Route::resource('shop_admin', 'Admin\AdminShopController');
	Route::get('shop_search_admin', 'Admin\AdminShopController@search')->name('shop_search_admin');
	Route::get('shop_admin_pay', 'Admin\AdminShopController@pay')->name('shop_admin_pay');
	Route::get('register_shop', 'Admin\AdminShopController@registerShop')->name('register_shop');
	Route::get('register_shop_details/{id}', 'Admin\AdminShopController@register_shop_details')->name('register_shop_details');
	Route::get('register_shop_update/{id}', 'Admin\AdminShopController@register_shop_update')->name('register_shop_update');
	Route::get('register_shop_destroy/{id}', 'Admin\AdminShopController@register_shop_destroy')->name('register_shop_destroy');


/*USER*/
	// USER
	Route::resource('user', 'UserController');
	Route::get('click/{email}', 'UserController@click')->name('click');
	Route::get('user_personalinformation/{id}', 'UserController@user_personalinformation')->name('user_personalinformation');
	Route::get('user_edit/{id}', 'UserController@user_personalinformation_edit')->name('user_edit');
	Route::PATCH('user_update/{id}', 'UserController@user_update')->name('user_update');

	Route::get('order', 'UserController@order')->name('order');
	Route::get('order_details/{id}', 'UserController@details')->name('order_details');

	Route::get('pay/{id}', 'UserController@pay')->name('pay');

	// SHOP
	Route::get('shop/{id}', 'UserController@shop')->name('shop');
	Route::get('shop_edit/{id}', 'UserController@shop_edit')->name('shop_edit');
	Route::get('shop_update/{id}', 'UserController@shop_update')->name('shop_update');
	Route::get('buy_post', 'UserController@buypost')->name('buy_post');
	Route::get('buy', 'UserController@buy')->name('buy');

	Route::resource('shop', 'ShopController');
	// PRODUCT
	Route::resource('product', 'ProductController');
	Route::get('star/{star}/{id}', 'ProductController@star')->name('star');
	Route::get('product_delete/{product_id}', 'ProductController@delete')->name('product_delete');
	Route::get('product_status/{product_id}/{status_id}', 'ProductController@status')->name('product_status');
	Route::get('product_search/{id}', 'ProductController@search')->name('product_search');
	//EXCHANGE
	Route::get('product_exchange', 'ProductController@exchange')->name('product_exchange');
	Route::get('month', 'ProductController@month')->name('month');

	Route::resource('comments', 'CommentController');
	Route::post('update_2/{cmt_id}/{product_id}', 'CommentController@update_2')->name('update_2');

	
/*RATE*/
	Route::resource('home', 'HomeController');
	Route::get('product_shop/{shop_id}', 'HomeController@product_shop')->name('product_shop');

	Route::get('search', 'HomeController@search')->name('search');

	Route::get('rate_high', 'HomeController@getRate_High')->name('rate_high');
	Route::get('rate_low', 'HomeController@getRate_Low')->name('rate_low');
	Route::get('rate_name', 'HomeController@getRate_Name')->name('rate_name');

/*CART*/
	Route::get('create_cart/{product_id}', 'CartController@create')->name('create_cart');
	Route::get('index/{product_id}', 'CartController@createIndex')->name('index');
	Route::get('cart', 'CartController@cart')->name('cart');
	Route::get('delete_cart/{id}', 'CartController@delete')->name('delete_cart');
	Route::get('deleteall_cart', 'CartController@deleteall')->name('deleteall_cart');
	// Route::post('update_cart/{id}/{increment}', 'CartController@update')->name('update_cart');
	Route::post('update_cart/{id}', 'CartController@update')->name('update_cart');
	Route::post('code_input/{id}', 'CartController@code')->name('code_input');

/*CHECKOUT*/
	Route::get('checkout/{id}', 'CartController@getCheckOut')->name('checkout');
	Route::post('post_checkout', 'CartController@postCheckOut')->name('post_checkout');
	Route::post('post_checkout_id/{id}', 'CartController@postCheckOut_id')->name('post_checkout_id');
	Route::post('post_checkout_code_id/{id}/{number}', 'CartController@postCheckOut_code_id')->name('post_checkout_code_id');


/*CATEGORIES*/
	Route::resource('categories', 'CategoriesController');

	/*ASIDE*/
	Route::get('categories_price/{categories_id}/{price1}/{price2}', 'CategoriesController@getCategories_Price')->name('categories_price');
	Route::get('categories_brand/{categories_id}/{brand_id}', 'CategoriesController@getCategorie_Brand')->name('categories_brand');
	Route::get('categories_price_brand/{categories_id}/{price1}/{price2}/{brand_id}', 'CategoriesController@getCategories_Price_Brand')->name('categories_price_brand');

	/*RATE: general*/
	Route::get('categories_rate_high/{categories_id}', 'CategoriesController@getCategories_RateHigh')->name('categories_ratehigh');
	Route::get('categories_rate_low/{categories_id}', 'CategoriesController@getCategories_RateLow')->name('categories_ratelow');
	Route::get('categories_rate_name/{categories_id}', 'CategoriesController@getCategories_RateName')->name('categories_ratename');

	/*RATE: price. no brand_id*/
	Route::get('categories_price_rate_high/{categories_id}/{price1}/{price2}', 'CategoriesController@getCategories_PriceRateHigh')->name('categories_price_ratehigh');
	Route::get('categories_price_rate_low/{categories_id}/{price1}/{price2}', 'CategoriesController@getCategories_PriceRateLow')->name('categories_price_ratelow');
	Route::get('categories_price_rate_name/{categories_id}/{price1}/{price2}', 'CategoriesController@getCategories_PriceRateName')->name('categories_price_ratename');

	/*RATE: no price. brand_id*/
	Route::get('categories_brand_rate_high/{categories_id}/{brand_id}', 'CategoriesController@getCategories_BrandRateHigh')->name('categories_brand_ratehigh');
	Route::get('categories_brand_rate_low/{categories_id}/{brand_id}', 'CategoriesController@getCategories_BrandRateLow')->name('categories_brand_ratelow');
	Route::get('categories_brand_rate_name/{categories_id}/{brand_id}', 'CategoriesController@getCategories_BrandRateName')->name('categories_brand_ratename');

	/*RATE: price. brand_id*/
	Route::get('categories_price_brand_rate_high/{categories_id}/{price1}/{price2}/{brand_id}', 'CategoriesController@getCategories_Price_BrandRateHigh')->name('categories_price_brand_ratehigh');
	Route::get('categories_price_brand_rate_low/{categories_id}/{price1}/{price2}/{brand_id}', 'CategoriesController@getCategorie_Price_BrandRateLow')->name('categories_price_brand_ratelow');
	Route::get('categories_price_brand_rate_name/{categories_id}/{price1}/{price2}/{brand_id}', 'CategoriesController@getCategories_Price_BrandRateName')->name('categories_price_brand_ratename');



/*BRAND*/
	Route::resource('brand', 'BrandController');

	/*ASIDE*/
	Route::get('brand_price/{brand_id}/{price1}/{price2}', 'BrandController@getBrand_Price')->name('brand_price');
	Route::get('brand_categories/{brand_id}/{categories_id}', 'BrandController@getBrand_Categories')->name('brand_categories');
	Route::get('brand_price_categories/{brand_id}/{price1}/{price2}/{categories_id}', 'BrandController@getBrand_Price_Categories')->name('brand_price_categories');

	/*RATE: general*/
	Route::get('brand_rate_high/{brand_id}', 'BrandController@getBrand_RateHigh')->name('brand_ratehigh');
	Route::get('brand_rate_low/{brand_id}', 'BrandController@getBrand_RateLow')->name('brand_ratelow');
	Route::get('brand_rate_name/{brand_id}', 'BrandController@getBrand_RateName')->name('brand_ratename');

	/*RATE: price. no categories*/
	Route::get('brand_price_rate_high/{brand_id}/{price1}/{price2}', 'BrandController@getBrand_PriceRateHigh')->name('brand_price_ratehigh');
	Route::get('brand_price_rate_low/{brand_id}/{price1}/{price2}', 'BrandController@getBrand_PriceRateLow')->name('brand_price_ratelow');
	Route::get('brand_price_rate_name/{brand_id}/{price1}/{price2}', 'BrandController@getBrand_PriceRateName')->name('brand_price_ratename');

	/*RATE: no price. categories_id*/
	Route::get('brand_categories_rate_high/{brand_id}/{categoriesd_id}', 'BrandController@getBrand_CategoriesRateHigh')->name('brand_categories_ratehigh');
	Route::get('brand_categories_rate_low/{brand_id}/{categories_id}', 'BrandController@getBrand_CategoriesRateLow')->name('brand_categories_ratelow');
	Route::get('brand_categories_rate_name/{categories_id}/{brand_id}', 'BrandController@getBrand_CategoriesRateName')->name('brand_categories_ratename');

	/*RATE: price. categories_id*/
	Route::get('brand_price_categories_rate_high/{brand_id}/{price1}/{price2}/{categories_id}', 'BrandController@getBrand_Price_CategoriesRateHigh')->name('brand_price_categories_ratehigh');
	Route::get('brand_price_categories_rate_low/{brand_id}/{price1}/{price2}/{categories_id}', 'BrandController@getBrand_Price_CategoriesRateLow')->name('brand_price_categories_ratelow');
	Route::get('brand_price_categories_rate_name/{brand_id}/{price1}/{price2}/{categories_id}', 'BrandController@getBrand_Price_CategoriesRateName')->name('brand_price_categories_ratename');


/*PRICE*/
	Route::resource('price', 'PriceController');
	Route::get('show_price/{price1}/{price2}', 'PriceController@showprice')->name('show_price');
	Route::get('price_brand/{price1}/{price2}/{brand_id}', 'PriceController@price_brand')->name('price_brand');

	Route::get('price_rate_high/{price1}/{price2}', 'PriceController@price_rate_high')->name('price_rate_high');
	Route::get('price_rate_low/{price1}/{price2}', 'PriceController@price_rate_low')->name('price_rate_low');
	Route::get('price_rate_name/{price1}/{price2}', 'PriceController@price_rate_name')->name('price_rate_name');

	Route::get('price_brand_rate_high/{price1}/{price2}/{brand_id}', 'PriceController@price_brand_rate_high')->name('price_brand_rate_high');
	Route::get('price_brand_rate_low/{price1}/{price2}/{brand_id}', 'PriceController@price_brand_rate_low')->name('price_brand_rate_low');
	Route::get('price_brand_rate_name/{price1}/{price2}/{brand_id}', 'PriceController@price_brand_rate_name')->name('price_brand_rate_name');

	Route::resource('information', 'Admin\InformationController');
	Route::get('news/{id}', 'Admin\InformationController@showNews')->name('news');
	Route::get('news_details/{id}', 'Admin\InformationController@showNewsDetails')->name('news_details');
